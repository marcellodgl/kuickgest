Kuickgest
==========
Data managment application interfaced through REST API.

# Author: 
Marcello Di Guglielmo - marcellodgl AT aruba.it

# License
Kuickgest is free software and licensed under GNU Geneal Public License v3

# Dependencies
Qt5 (modules: Core Quick Test Gui QuickControls2 Widgets)
KF5 (modules: Kirigami2 I18n CoreAddons)

# Third libraries:

# Build
```
mkdir build && cd build
cmake ..
cmake --build .
```
# Features
* Kirigami interface
* REST api access to database
* Cards grid of table access and info
* Table page showing Records list organized in swiping items
* Sample page for items modify
* Order dialog that set items sort of data
* Search functionality
* Network settings for connection to resource

# Instructions
Manual is available in following document [manual](src/doc/manual.md)

# Gallery
<img title="Global drawer (mobile)" src="src/doc/images/kuickgest-screenshot.png" width="200px">
<img title="Table page (mobile)" src="src/doc/images/kuickgest-screenshot1.png" width="200px">
<img title="Sample page (mobile)" src="src/doc/images/kuickgest-screenshot2.png" width="200px">
<img title="Wide interface (desktop)" src="src/doc/images/kuickgest-screenshot3.png" width="500px">
<img title="Search (mobile)" src="src/doc/images/kuickgest-screenshot4.png" width="200px">
<img title="Browse cards (mobile)" src="src/doc/images/kuickgest-screenshot5.png" width="500px">
