#ifndef MAPPER_H
#define MAPPER_H
#include <QAbstractItemModel>
#include <QDate>
#include <QDebug>
#include <QObject>
#include <QSignalMapper>
class Mapper : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QAbstractItemModel *model READ model WRITE setModel NOTIFY modelChanged)

    Q_PROPERTY(int currentIndex READ currentIndex WRITE setCurrentIndex NOTIFY currentIndexChanged)
    // Q_PROPERTY(Type typeName)

public:
    explicit Mapper(QObject *parent = nullptr);

    enum Type { None, String, Script, Date, Time, DateTime, Bool, Int, Number, Enum, List };
    // Q_ENUM replace deprecated Q_ENUMS, positioned after enum declaration
    Q_ENUM(Type)
    Q_INVOKABLE void addMapping(QObject *widget, int section, Type type);
    QAbstractItemModel *model() const;
    int currentIndex() const;

Q_SIGNALS:

    void modelChanged(QAbstractItemModel *model);

    void currentIndexChanged();

public Q_SLOTS:
    void setModel(QAbstractItemModel *model);
    void setCurrentIndex(int currentIndex);
    bool submit();
    void revert();
    void clear();
    QVariantMap itemsValueMap();
    QVariantMap secondaryItemsValueMap();
    QVariantMap itemsOperatorMap();
    QVariant itemDisplay(QString name);
    QVariant secondaryItemDisplay(QString name);

private:
    QAbstractItemModel *m_model;
    QMap<int, QObject *> widgetMap;
    QMap<int, Type> widgetTypeMap;
    QMap<int, QVariant> widgetValueMap;
    QMap<int, QVariant> widgetSecondaryValueMap;
    int m_currentIndex;
    QSignalMapper *signalMapper;
    QSignalMapper *signalMapper1;
    void setWidgetValue(int section, QVariant value);
private Q_SLOTS:
    void valueChanged(int section);
    void displayChanged(int section);
    void deleteItem(QObject *widget);
};

#endif // MAPPER_H
