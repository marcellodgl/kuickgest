#ifndef INTERFACEDATA_H
#define INTERFACEDATA_H

#include "support.h"
#include <QCoreApplication>
#include <QDebug>
#include <QFile>
#include <QFileInfo>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QRegularExpression>
#include <QSettings>
#include <QStandardItemModel>
#include <QUrl>
#include <QUrlQuery>
#include <QVariant>

class InterfaceData : public QStandardItemModel
{
    Q_OBJECT

    Q_PROPERTY(QByteArray content READ content WRITE setContent NOTIFY contentChanged)
    Q_PROPERTY(float progressValue READ progressValue WRITE setProgressValue NOTIFY progressValueChanged)
    Q_PROPERTY(QString section READ section WRITE setSection NOTIFY sectionChanged)
    Q_PROPERTY(QString contentPath READ contentPath WRITE setContentPath NOTIFY contentPathChanged)
    Q_PROPERTY(QString contentQuery READ contentQuery WRITE setContentQuery NOTIFY contentQueryChanged)
    Q_PROPERTY(QString contentOrder READ contentOrder WRITE setContentOrder NOTIFY contentOrderChanged)
    Q_PROPERTY(QString contentRange READ contentRange WRITE setContentRange NOTIFY contentRangeChanged)
    Q_PROPERTY(bool runningState READ runningState WRITE setRunningState NOTIFY runningStateChanged)
    Q_PROPERTY(QString schemaKey READ schemaKey WRITE setSchemaKey NOTIFY schemaKeyChanged)
    Q_PROPERTY(QString schemaPath READ schemaPath WRITE setSchemaPath NOTIFY schemaPathChanged)
    Q_PROPERTY(QString contentKey READ contentKey WRITE setContentKey NOTIFY contentKeyChanged)
    Q_PROPERTY(QString indexesKey READ indexesKey WRITE setIndexesKey NOTIFY indexesKeyChanged)
    Q_PROPERTY(QString indexesPath READ indexesPath WRITE setIndexesPath NOTIFY indexesPathChanged)
    Q_PROPERTY(int numScreenItems READ numScreenItems WRITE setNumScreenItems NOTIFY numScreenItemsChanged)
    Q_PROPERTY(QString filter READ filter WRITE setFilter NOTIFY filterChanged)
    Q_PROPERTY(int filterColumn READ filterColumn WRITE setFilterColumn NOTIFY filterColumnChanged)
    Q_PROPERTY(QString filterRole READ filterRole WRITE setFilterRole NOTIFY filterRoleChanged)
    Q_PROPERTY(QString orderRole READ orderRole WRITE setOrderRole NOTIFY orderRoleChanged)
public:
    explicit InterfaceData(QObject *parent = nullptr);
    bool setData(const QModelIndex &index, const QVariant &value, int role);

    //    Q_INVOKABLE void updateData();
    bool runningState() const;
    Q_INVOKABLE void loadContentData();
    Q_INVOKABLE void loadContents();
    QString schemaKey() const;
    Q_INVOKABLE void initRoles();
    Q_INVOKABLE QList<int> itemsRoleList();
    Q_INVOKABLE QString itemName(int role);
    Q_INVOKABLE QString itemType(int role);
    Q_INVOKABLE QString itemFormat(int role);
    Q_INVOKABLE int itemLength(int role);
    Q_INVOKABLE QString itemDescription(int role);
    Q_INVOKABLE QString itemRelationTable(int role);
    Q_INVOKABLE QString itemRelationIndex(int role);
    Q_INVOKABLE QString itemRelationDisplay(int role);
    Q_INVOKABLE QVariantMap itemConstraint(int role);
    Q_INVOKABLE bool itemEnabled(int role);
    Q_INVOKABLE QString itemKey(int role);
    Q_INVOKABLE QVariantList itemEnum(int role);
    Q_INVOKABLE void add();
    Q_INVOKABLE void remove(int index);
    Q_INVOKABLE QString primaryKey();
    Q_INVOKABLE QVariantMap catchValueMap(QVariant primaryValue);
    Q_INVOKABLE QVariant dataRole(QModelIndex index, int role);
    Q_INVOKABLE bool matchContainsFilter(int index);
    Q_INVOKABLE void sortItems(int column, Qt::SortOrder order = Qt::AscendingOrder);
    QString contentKey() const;
    QString indexesKey() const;

    QString contentPath() const;

    QString section() const;

    QString contentQuery() const;

    QByteArray content() const;

    QString schemaPath() const;

    QString indexesPath() const;

    int numScreenItems() const;

    QString contentOrder() const;

    float progressValue() const;

    QString contentRange() const;
    QString filter() const;
    int filterColumn() const;
    QString filterRole() const;

    QString orderRole() const;

public Q_SLOTS:
    void setSchemaKey(QString schemaKey);
    void setContentKey(QString contentKey);
    bool submitRow(int row);
    void setIndexesKey(QString indexesKey);

    void setContentPath(QString contentPath);

    void setSection(QString section);

    void setContentQuery(QString contentQuery);

    void setContent(QByteArray content);

    void setSchemaPath(QString schemaPath);

    void setIndexesPath(QString indexesPath);
    bool goNext();
    void goLast();
    void setNumScreenItems(int numScreenItems);
    void searchItems(QVariantMap valueMap, QVariantMap secondaryValueMap, QVariantMap operatorMap);
    void setContentOrder(QString contentOrder);

    QVariantMap indexValues(const QModelIndex &index) const;
    void revert();
    void setRunningState(bool runningState);

    void setProgressValue(float progressValue);

    void setContentRange(QString contentRange);
    void abortContent();
    void clean();
    void setFilter(QString filter);
    void setFilterColumn(int filterColumn);
    void setFilterRole(QString filterRole);

    void setOrderRole(QString orderRole);

Q_SIGNALS:
    void errorHappened(QString text);
    void runningStateChanged(bool runningState);
    void schemaKeyChanged(QString schemaKey);
    void contentKeyChanged(QString contentKey);
    void loadCompleted();
    void postCompleted();
    void patchCompleted();
    void indexesKeyChanged(QString indexesKey);

    void contentPathChanged(QString contentPath);

    void sectionChanged(QString section);

    void contentQueryChanged(QString contentQuery);

    void contentChanged(QByteArray content);

    void schemaPathChanged(QString schemaPath);

    void indexesPathChanged(QString indexesPath);
    void relationRequest(QString tableName);
    void foreignKeyRequest(QString foreignKey,QString referenceTable,QString referenceIndex);
    void subTableRequest(QString subTableName, QString subTableFKey, QString subReferenceName);
    void numScreenItemsChanged(int numScreenItems);

    void contentOrderChanged(QString contentOrder);

    void progressValueChanged(float progressValue);

    void contentRangeChanged(QString contentRange);

    void filterChanged(QString filter);

    void filterColumnChanged(int filterColumn);

    void filterHashChanged(QString filterHash);

    void filterRoleChanged(QString filterRole);

    void orderRoleChanged(QString orderRole);

private:
    QUrl contentUrl;
    Support *support;
    QNetworkAccessManager *netAccessManager;
    QNetworkReply *replyContent;
    QNetworkReply *replyPatch;
    QNetworkReply *replyPost;
    QNetworkReply *replyDelete;
    bool m_runningState;
    void elaborateSchema(QByteArray data);
    QJsonValue parseJsonObject(QJsonObject jsonObj, QString key);
    QJsonDocument jsonSchemaDoc;
    QSettings *settingsConfiguration;

    QHash<int, QByteArray> baseRoleNames;
    QString m_schemaKey;
    QHash<int, QString> roleTypes;
    QHash<int, QString> roleFormats;
    QHash<int, int> roleLengths;
    QHash<int, QString> roleDescriptions;
    QHash<int, QString> roleRelationTables;
    QHash<int, QString> roleRelationIndexes;
    QHash<int, QString> roleRelationDisplays;
    QHash<int, QVariantMap> roleConstraints;
    QHash<int, bool> roleEnabled;
    QHash<int, QVariantList> roleEnums;
    QHash<int, QString> roleKeys;

    int m_itemsCount;
    QString m_contentKey;

    QString m_indexesKey;
    void saveIndexes();
    QString m_contentPath;

    QString m_section;

    QString m_contentQuery;

    QByteArray m_content;

    QString m_schemaPath;

    QString m_indexesPath;
    int m_rolePrimaryKey;
    int rowInserting;
    int rowDeleting;
    int rowChanged;
    int offsetFactor;
    int m_numScreenItems;
    int numItems;
    int minRange;
    int maxRange;
    int currentRow;
    QString m_contentOrder;

    QList<QMap<int, QVariant>> savedRowData;
    float m_progressValue;

    QString m_contentRange;
    QMap<int, bool> m_matchContainsMap;

    QString m_filter;
    int m_filterColumn;
    QString m_filterRole;

    QString m_orderRole;
    QString getSubTableName(QJsonValue subTableValue);
    QString getSubTableFKey(QJsonValue subTableValue);
private Q_SLOTS:
    void readyContent();
    void readyPatch();
    void readyPost();
    void readyDelete();
    void progressUpdate(qint64 value, qint64 maximum);
    void onRowsInserted(const QModelIndex &parent, int first, int last);
    void onDataChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight, const QVector<int> &roles);
};

#endif // INTERFACEDATA_H
