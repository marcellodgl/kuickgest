#ifndef SUPPORT_H
#define SUPPORT_H
#include <QDebug>
#include <QJsonDocument>
#include <QJsonObject>
#include <QString>
#include <QStringList>
class Support
{
public:
    Support();
    QJsonValue parseJsonObject(QJsonObject jsonObj, QString key);
    QStringList keysJsonList(QByteArray dataObj);

private:
};

#endif // SUPPORT_H
