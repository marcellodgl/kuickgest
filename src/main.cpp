/*
Kuickgest: Resource managment system interfaced through REST API.

Copyright (c) 2022 Marcello Di Guglielmo

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#define APP_VERSION "0.3.0"
#ifdef QT_QML_DEBUG
#include <QtQuick>
#endif
#include "config-kuickgest.h"
#include "interfacesettings.h"
#include <QGuiApplication>
#include <QQmlEngine>
#include <QQuickView>
#include <QScopedPointer>
#include <QtQml>
#ifndef Q_OS_ANDROID
#include <QApplication>
#else
#include <QGuiApplication>
#endif
#include "about.h"
#include "interfacedata.h"
#include "mapper.h"
#include "service.h"
#include <KAboutData>
#include <KLocalizedContext>
#include <KLocalizedString>
#include <QDebug>
#include <QQmlApplicationEngine>
#include <QUrl>
Q_DECL_EXPORT int main(int argc, char *argv[])
{
    qmlRegisterType<InterfaceSettings>("plasma.kuickgest.InterfaceSettings", 1, 0, "InterfaceSettings");
    qmlRegisterType<Service>("plasma.kuickgest.Service", 1, 0, "Service");
    qmlRegisterType<InterfaceData>("plasma.kuickgest.InterfaceData", 1, 0, "InterfaceData");
    qmlRegisterType<Mapper>("plasma.kuickgest.Mapper", 1, 0, "Mapper");
    //importante mettere i setSource dopo la configurazione degli engine
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif
#ifndef Q_OS_ANDROID
    QApplication app(argc, argv);
#else
    QGuiApplication app(argc, argv);
#endif
    QIcon windowIcon(QStringLiteral(":/kuickgest/icons/kuickgest.svg"));
    app.setWindowIcon(windowIcon);
    QCoreApplication::setOrganizationName(QStringLiteral("DGL"));
    QCoreApplication::setOrganizationDomain(QStringLiteral("DGL"));
    QCoreApplication::setApplicationName(QStringLiteral("Kuickgest"));
    app.setApplicationDisplayName(QStringLiteral("Kuickgest"));
    QCoreApplication::setApplicationVersion(QStringLiteral(APP_VERSION));
    KLocalizedString::setApplicationDomain("kuickgest");
    qDebug()<<"available domain translations "<<KLocalizedString::availableDomainTranslations("kuickgest");
    qDebug() << "italian " << KLocalizedString::isApplicationTranslatedInto(QStringLiteral("it"));

    qDebug()<<"application domain"<<KLocalizedString::applicationDomain();
    QQmlApplicationEngine engine;


    KLocalizedContext *context = new KLocalizedContext(&engine);
    context->setTranslationDomain(QStringLiteral("kuickgest"));
    engine.rootContext()->setContextObject(context);


    qDebug()<<KLocalizedString::languages();

    KAboutData aboutData(
        // The program name used internally.
        QStringLiteral("kuickgest"),
        // A displayable program name string.
        i18nc("@title", "Kuickgest"),
        // The program version string.
        QStringLiteral(KUICKGEST_VERSION_STRING),
        // Short description of what the app does.
        i18n("Data resource application interfaced through REST API"),
        // The license this code is released under.
        KAboutLicense::GPL,
        // Copyright Statement.
        i18n("(c) 2022"));
    aboutData.addAuthor(i18nc("@info:credit", "Marcello Di Guglielmo"),
                        i18nc("@info:credit", "Author Role"),
                        QStringLiteral("marcellodgl@aruba.it"),
                        QLatin1String(""));
    qmlRegisterSingletonType<AboutType>("plasma.kuickgest.About", 1, 0, "AboutType", [](QQmlEngine *engine, QJSEngine *scriptEngine) -> QObject * {
        Q_UNUSED(engine)
        Q_UNUSED(scriptEngine)

        return new AboutType();
    });
    KAboutData::setApplicationData(aboutData);



    engine.load(QUrl(QStringLiteral("qrc:/qml/kuickgest.qml")));

    if (engine.rootObjects().isEmpty()) {
        qDebug() << "Failed to load QML";
        return -1;
    }





    return app.exec();
}

