/*
Kuickgest: Resource managment system interfaced through REST API

Copyright (c) 2022 Marcello Di Guglielmo

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

import QtQuick 2.10
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.15
import "pages"
import QtQml.Models 2.15
import org.kde.kirigami as Kirigami
import plasma.kuickgest.About 1.0
import plasma.kuickgest.Service 1.0
import plasma.kuickgest.Mapper 1.0
import plasma.kuickgest.InterfaceData 1.0
Kirigami.ApplicationWindow {
    // ID provides unique identifier to reference this element

    id: root

    property string previewSource
    property alias model: schemaShow
    property alias mapper: mapper
    property alias relMapper: relMapper
    property alias actionsInstantiator: instantiatorActions
    property alias pagePool: mainPagePool
    property var tableActionGroup: tableActionGroup
    // Window title
    // i18nc is useful for adding context for translators, also lets strings be changed for different languages
    title: i18nc("@title:window", "Kuickgest")
/*
    contextDrawer: Kirigami.ContextDrawer{
        id: mainContextDrawer
    }
*/

    Kirigami.PagePool{
        id: mainPagePool
        onObjectNameChanged: console.log("objectName "+objectName)
        onLastLoadedUrlChanged: console.log("loaded item "+lastLoadedUrl)

    }

/*    onPageStackChanged: console.log("contentItem "+contentItem)
    pageStack.onPageRemoved: console.log("page removed")
    pageStack.onCurrentItemChanged: {
        console.log("current stack item changed "+pageStack.currentItem+" lastLoadedItem "+mainPagePool.lastLoadedItem)
        console.log(" samplePage "+actionSample.pageItem())
        if(pageStack.currentItem===actionSample.pageItem()){
            if(mapper.currentIndex===-1){
                pageStack.push(actionTable.pageItem())
            }
        }

    }
*/
/*    Component{
        id: aboutPage
        Kirigami.AboutPage {
            aboutData: AboutType.aboutData
        }
    }
*/
    // Initial page to be loaded on app load

    Service{
        id:serviceTests
        //Si prende il path dello schema json api per aggiungerlo all'host ed effettuare la query completa
        loginTokenPath: "/rpc/login"
        //        descriptionKey: "definitions"
        section: "definitions"

        //una volta caricati dal web tramite l'url testsPath e acquisiti nei settings vengono passati nella grafica

        //        tokenAcquired è un segnale che viene fuori dopo updateData ready lanciato su serviceTests dal bottone "Acquire"
        onTokenAcquired: {

            schemaShow.loadContents()
        }
        onParametersAcquired: {
            schemaShow.loadContents()
        }

        onProgressValueChanged: {
            console.log("progressValue"+progressValue)
            if(progressValue===0){
//                progressBusyIndicator.running=true
//                progressTimer.stop()
            }
            if(progressValue===1){
//                progressBusyIndicator.running=false
//                progressTimer.restart()
            }
        }
        onErrorHappened: showPassiveNotification(text,"azione")
        onRunningStateChanged: runningState => console.log("Application: Service running state "+runningState)

    }


    Mapper{
        id: mapper
        enum NameType{
            None, String, Script, Date, Time, DateTime, Bool, Int, Number, Enum, List
        }
        property int typeNone: Mapper.None
        property int typeString: Mapper.String
        property int typeScript: Mapper.Script
        property int typeDate: Mapper.Date
        property int typeTime: Mapper.Time
        property int typeDateTime: Mapper.DateTime
        property int typeBool: Mapper.Bool
        property int typeInt: Mapper.Int
        property int typeNumber: Mapper.Number
        property int typeEnum: Mapper.Enum
        property int typeList: Mapper.List



    }

    Mapper{
        id: relMapper
        enum NameType{
            None, String, Script, Date, Time, DateTime, Bool, Int, Number, Enum, List
        }
        property int typeNone: Mapper.None
        property int typeString: Mapper.String
        property int typeScript: Mapper.Script
        property int typeDate: Mapper.Date
        property int typeTime: Mapper.Time
        property int typeDateTime: Mapper.DateTime
        property int typeBool: Mapper.Bool
        property int typeInt: Mapper.Int
        property int typeNumber: Mapper.Number
        property int typeEnum: Mapper.Enum
        property int typeList: Mapper.List



    }

    globalDrawer: Kirigami.GlobalDrawer {
        id: actionsGlobalDrawer
        title:i18n("Kuickgest")

        titleIcon: ":/kuickgest/icons/kuickgest.svg"
        // Makes drawer a small menu rather than sliding pane
        //        isMenu: true
        actions: [
            Kirigami.Action {
                text: i18n("Connect")
                icon.name: "network-connect"
                onTriggered: {

                    if(actionsGlobalDrawer.actions.length>drawerActionGroup.actions.length){
                        console.log("connecting ")
                        actionsGlobalDrawer.actions=[]
                        for(var i=0;i<drawerActionGroup.actions.length;i++){
                            actionsGlobalDrawer.actions.push(drawerActionGroup.actions[drawerActionGroup.actions.length-1-i])
                        }
                    }
//                    pageStack.layers.push(mainPagePool.pageForUrl("pages/MainPage.qml"))
                    actionMain.trigger()
                    console.log("Mapper String "+root.mapper.typeString)
                    serviceTests.updateData()
                }
                ActionGroup.group: drawerActionGroup

            },
            Kirigami.PagePoolAction {
                text: i18n("Settings")
                icon.name: "settings-configure"
                pagePool: mainPagePool
                page: "pages/SettingsPage.qml"
                useLayers: true
                ActionGroup.group: drawerActionGroup

            },
            Kirigami.PagePoolAction {
                text: i18n("Help")
                icon.name: "help-browser"
                pagePool: mainPagePool
                page: "pages/HelpPage.qml"
                useLayers: true
                ActionGroup.group: drawerActionGroup

            },
            Kirigami.PagePoolAction {
                text: i18n("About")
                icon.name: "help-about"
                pagePool: mainPagePool
                page: "pages/AboutPage.qml"
                useLayers: true
                //                onTriggered: pageStack.layers.push(aboutPage)
                ActionGroup.group: drawerActionGroup
            },
            Kirigami.Action {
                id: actionQuit
                text: i18n("Quit")
                icon.name: "application-exit"
                shortcut: StandardKey.Quit
                onTriggered: Qt.quit()
                ActionGroup.group: drawerActionGroup
            },
            Kirigami.Action {
                id: actionSeparator
                separator: true
                ActionGroup.group: drawerActionGroup
            },
            Kirigami.PagePoolAction {
                id: actionMain
                text: i18n("Main")
                icon.name: "home"
                shortcut: StandardKey.Back
                pagePool: mainPagePool
                page:"pages/MainPage.qml"
                Component.onCompleted: {
                    console.log("main "+mainPagePool.pageForUrl("pages/MainPage.qml"))
                }
                //                onTriggered: tableActionGroup.checkedAction=null
                ActionGroup.group: drawerActionGroup


                //                onTriggered: mainPage.connectScreen(true)
                //                enabled: mainPage.status===MainPage.Status.Disconnected
            },
            Kirigami.PagePoolAction {
                id: actionTable
                text: i18n("Table")
                pagePool: mainPagePool
                page:"pages/TablePage.qml"
                visible: false
//                useLayers: true
                ActionGroup.group: drawerActionGroup
            },
            Kirigami.PagePoolAction {
                id: actionSample
                text: i18n("Sample")
                visible: false
                pagePool: mainPagePool
                page:"pages/SamplePage.qml"
//                visible: false
                useLayers: true
                onTriggered: console.log("position y"+pageStack.globalToolBar.height)
                ActionGroup.group: drawerActionGroup
            },
            Kirigami.Action{
                text:"prova"
                visible: false
                onTriggered: {
                    console.log(pagePool.items)
                    console.log(pagePool.loadPage("pages/SettingsPage.qml"))
                }
                ActionGroup.group: drawerActionGroup
            }

        ]
    }
    pageStack{
        initialPage: mainPagePool.loadPage("pages/MainPage.qml")
        defaultColumnWidth: root.width>=Kirigami.Units.gridUnit*20*2 ? root.width/2 : Kirigami.Units.gridUnit * 20
    }

    InterfaceData{
        //                Modello che serve a caricare la lista delle tabelle con relative informazioni
        id:schemaShow

        //la chiamata per il metodo postgrest per gli schema è ""
        contentPath: ""
        //              Schema for drawer is stored on file in qrc resource
        //              schemaKey and schemaPath have to be instantiated in sequence a solution could be after object is created so schemaKey could be instantiated before schemaPath
        schemaPath: ":/qml/attached/ListSchema.json"
        schemaKey:"Drawer"

        Component.onCompleted: {

            initRoles()
        }
        //                schemaKey: "ListSchema"
        //La section serve a individuare una stottoparte del documento json che rappresenti in questo caso la lista degli oggetti
        //tabella con annesse chiavi che rappresentano le proprietà della tabella.
        //Si avrà quindi un modello che ha i nomi di tutte le tabelle.
        section: "definitions"
        //Al caricamento delle voci del drawer laterale passa all'instantietor le tabelle
        //si troverà anche lo schema disponibile per tutte le tabelle
        //loadCompleted is signal that come from InterfaceData when finish readyContent (after acquire)
        onLoadCompleted:{
            console.log("schemaShow load Completed")

            //            pageTest.setModel(0)
            instantiatorTables.clear()
            instantiatorRelations.clear()
            instantiatorActions.clear()
            //listed tableName of drawer are used to create an instance of table
            for(var i=0;i<schemaShow.rowCount();i++){

                var roleList=schemaShow.itemsRoleList();
                var tableName=schemaShow.data(schemaShow.index(i,0),roleList[0]);
                var indexModel=schemaShow.index(i,0);
                console.log("schemaShow - addTable "+schemaShow.dataRole(indexModel,0)+" schemaShow.index "+schemaShow.index(i,0)+" roleList "+roleList)

                instantiatorTables.addTable(tableName)
            }
            instantiatorTables.active=true
            //solutions becouse in asynchronous mode of instantiator element are disposed in reverse order
            var reverseTableList=[]
            for(var j=0;j<schemaShow.rowCount();j++){

                var roleList1=schemaShow.itemsRoleList();
                var tableName1=schemaShow.data(schemaShow.index(schemaShow.rowCount()-j-1,0),roleList1[0]);
                reverseTableList.push(tableName1)
                console.log("instantiator action "+tableName1+" model "+instantiatorActions.model)
            }
            instantiatorActions.model=reverseTableList
            instantiatorActions.active=true
            console.log("schemaShow - load completed schema show rows "+schemaShow.rowCount()+" instantiator action model "+instantiatorActions.model[0])
        }
        //                Quando questo modello carica il contenuto totalmente come dati viene passato al serviceTests che ne fa elaborazione.
        //                contents represents such things read from readyContents

        onContentChanged: content => serviceTests.acquireSchema(content)
        property int numLoadedTables : 0
        onProgressValueChanged: {
//            progressBusyIndicator.running=true
//            if(progressValue==1) progressBusyIndicator.running=false
        }
//        Filling subTableRequest in table models during schemaShow scan in instantiatorTables map
        onSubTableRequest: function(subTableName,subTableFKey){
            instantiatorSubTables.addSubTable(subTableName,subTableFKey)
//            if(instantiatorTables.subTablesMap[subReferenceName]===undefined){
//                instantiatorTables.subTablesMap[subReferenceName]=[]
//            }
//            instantiatorTables.subTablesMap[subReferenceName].push(subTableName)

//            if(instantiatorTables.subForeignKeysMap[subReferenceName]===undefined){
//                instantiatorTables.subForeignKeysMap[subReferenceName]=[]
//            }
//            instantiatorTables.subForeignKeysMap[subReferenceName].push(subTableFKey)
//            console.log("subReferenceName "+subReferenceName+": "+instantiatorTables.subTablesMap[subReferenceName]+" "+instantiatorSubTables[subReferenceName])
        }
    }


    Instantiator{
        //si usa l'instantiator al posto della dichiarazione di Interno di un delegate (come era fatto prima)
        //perchè accadrebbe che le tabelle vengono instanziate ogni qualvolta l'elemento riappare, anche se
        //precedentemente era stato già caricato
        id:instantiatorTables
        property var listTables : []
        property int numLoadedTables: 0
        property var subTablesMap: ({})
        property var subForeignKeysMap: ({})
        property bool runningState: false
        onRunningStateChanged: console.log("Running state changed "+runningState)
        active: false
        onActiveChanged: console.log("Table active changed "+active+"\n-----Starting loading Tables-----")
        model:0
        function addTable(tableName){
            instantiatorTables.runningState=true
            console.log("Application: Table add "+tableName )
            //listTables: list of names of tables
            listTables.push(tableName)
            //model++ is an action that add an element to the model, in that case through delegate is created a new InterfaceData
            model++

        }

        function evaluateRunning(){
            var evalState=false;
            for(var i=0;i<model;i++){
                var tableObject=instantiatorTables.objectAt(i)
                if(tableObject) evalState=(evalState || tableObject.runningState)

            }
            //                console.log("evaluate running "+runningState)
            return evalState
        }
        function clear(){
            active=false
            listTables=[]
            numLoadedTables=0
            model=0

        }

        //this virtual model is used to allocate InterfaceDataModel characterized by parameters obtained by instantiator listTable element
        delegate: InterfaceData{
            //numero di elementi per schermata
            property var subTablesList: []
            property var subForeignKeysList: []
            property var subReferenceIndexesList: []
            numScreenItems: 5
            onErrorHappened: {
                errorDialog.subtitle=text
                errorDialog.open()
            }
            //tramite la chiave index si riesce a individuare il corrente elemento del delegate e pertanto
            //individuare nella lista delle tabelle il relativo nome
            contentPath: "/"+instantiatorTables.listTables[model.index]

            //                contentOrder:"id.desc"
            indexesPath: "keys"
            //importante mettere schemaKey dopo indexesKey perchè quest'ultimo viene utilizzato
            //all'interno di setSchemaKey
            //InstantiatorTables has names set by addTable(tableName)
            schemaPath: "schemes"
            schemaKey: instantiatorTables.listTables[model.index]
//            subTablesList: instantiatorTables.subTablesMap[instantiatorTables.listTables[model.index]]

//            subForeignKeysList: instantiatorTables.subForeignKeysListMap[instantiatorTables.listTables[model.index]]
            //After loaded the instance of the InterfaceData model it will load first data screen with loadContents
            //the order of screen is based on settings contentOrder of the InterfaceData model
            Component.onCompleted:{
                initRoles()
                //                    console.log("added index"+model.index+"table"+instantiatorTables.listTables[model.index])
                //                Carica La tabella dal DB con i valori impostati
                var primaryKeyName=primaryKey()
                if(primaryKeyName.length){
                    contentOrder=primaryKeyName+".desc"
                }

                console.log("added index"+model.index+"table"+instantiatorTables.listTables[model.index]+"primaryKey"+primaryKey())

                loadContents()
            }
            onRelationRequest: tableName => instantiatorRelations.addRelation(tableName)

            onLoadCompleted: {
                instantiatorTables.numLoadedTables++
                console.log("instantiatorTables loadCompleted: "+instantiatorTables.numLoadedTables+" on "+instantiatorTables.model)
                console.log("instantiatorTables loadCompleted: relations "+instantiatorRelations.listRelations)

                //through activate on instantiatorRelations the InterfaceData model of relations are allocated after all loadContens of tables are completed
                if(instantiatorTables.numLoadedTables===instantiatorTables.model) {
                    instantiatorRelations.active=true
                    instantiatorSubTables.active=true
                }

                //
            }
            onContentQueryChanged: {
                var mainPage=mainPagePool.pageForUrl("pages/MainPage.qml")
                if(mainPage) mainPage.setTableContentQuery(instantiatorTables.listTables[model.index],contentQuery)
                console.log("contentQuet changed "+contentQuery)
            }
            onPostCompleted: {
                console.log("post Completed on "+instantiatorTables.listTables[model.index])
                //               Comando che serve a far comparire la primary key inserita che è presente ormai nel modello tramite post
                //                ma deve essere resa disponibile tramite il mapper
                // ***VERIFICARE***
                var samplePage=mainPagePool.pageForUrl("pages/SamplePage.qml")
                root.mapper.revert()
            }
            onRunningStateChanged: instantiatorTables.runningState=instantiatorTables.evaluateRunning()

            onProgressValueChanged: {
            }
            onDataChanged: {
                var tablePage=mainPagePool.pageForUrl("pages/TablePage.qml")
                tablePage.updateDelegate(topLeft,bottomRight)
            }
            onContentRangeChanged: function(contentRange){
                var tablePage=mainPagePool.pageForUrl("pages/TablePage.qml")
                if(tablePage) tablePage.updateContentRange(contentRange)
                var mainPage=mainPagePool.pageForUrl("pages/MainPage.qml")
                if(mainPage) mainPage.setTableContentRange(instantiatorTables.listTables[model.index],contentRange)
            }
            onContentOrderChanged: function(contentOrder){
                var mainPage=mainPagePool.pageForUrl("pages/MainPage.qml")
                if(mainPage) mainPage.setTableContentOrder(instantiatorTables.listTables[model.index],contentOrder)
            }

        }

    }


    Instantiator{
        property var listRelations : []
        id:instantiatorRelations
        property bool runningState: false
        property int numLoadedTables: 0
        //        asynchronous: true

        active: false
        onActiveChanged: console.log("Relation active changed "+active+"\n-----Starting loading Relations-----")
        //        active: instantiatorTables.loaded
        onRunningStateChanged: console.log("Running relation state changed "+runningState)
        model:0
        function evaluateRunning(){
            var evalState=false;
            for(var i=0;i<model;i++){
                var tableObject=instantiatorRelations.objectAt(i)
                if(tableObject) evalState=(evalState || tableObject.runningState)

            }
            console.log("evaluate relation running "+evalState)
            return evalState
        }

        function addRelation(tableName){
            //                Istruzione levata altrimenti risulta che la relation table sta caricando anche se non fa nulla
            //                instantiatorRelations.runningState=true
            console.log("Relation request "+tableName )
            //there is only one instance of relation table for each tableName, in that creation occurrence the instantiator fill the
            //model with a new number that corrispond through delegate to an InterfaceData model
            if(listRelations.indexOf(tableName)===-1){
                listRelations.push(tableName)
                console.log("Relations "+listRelations)
                model++

            }

        }


        function objectItem(tableName){
            return instantiatorRelations.objectAt(listRelations.indexOf(tableName))
        }

        function clear(){
            active=false
            listRelations=[]
            numLoadedTables=0
            model=0

        }

        delegate: InterfaceData{

            contentPath: "/"+instantiatorRelations.listRelations[model.index]
            onErrorHappened: {
                errorDialog.subtitle=text
                errorDialog.open()
            }
            //                contentOrder:"id.asc"
            indexesPath: "keys"
            //importante mettere schemaKey dopo indexesKey perchè quest'ultimo viene utilizzato
            //all'interno di setSchemaKey
            schemaPath: "schemes"
            schemaKey: instantiatorRelations.listRelations[model.index]
            Component.onCompleted:{
//                start loading items with initRoles
                initRoles()
                //                console.log("added index"+model.index+"table"+instantiatorTables.listTables[model.index])
                console.log("added relation"+model.index+instantiatorRelations.listRelations[model.index]+" rowCount "+rowCount())
                var primaryKeyName=primaryKey()
                if(primaryKeyName.length){
                    contentOrder=primaryKeyName+".desc"
                }
                //the loadContents is not considered already because it has to know how partially load record.

                //##                    loadContents()
            }
            onRunningStateChanged: instantiatorRelations.runningState=instantiatorRelations.evaluateRunning()
            onLoadCompleted: {
                instantiatorRelations.numLoadedTables++
                console.log("relation table num"+instantiatorRelations.numLoadedTables+" name "+instantiatorRelations.listRelations[model.index]+" "+instantiatorRelations.model+" rowCount "+rowCount())
//                if(instantiatorRelations.numLoadedTables===instantiatorRelations.model) instantiatorSubTables.active=true
            }


        }


    }

    Instantiator{
        property var listSubTables : []
        id:instantiatorSubTables
        property bool runningState: false
        property int numLoadedTables: 0
        property var subTableForeignKeysMap: ({})
        //        asynchronous: true

        active: false
        onActiveChanged: console.log("Subtables active changed "+active+"\n-----Starting loading Subtables-----")
        //        active: instantiatorTables.loaded
        onRunningStateChanged: console.log("Running subtables state changed "+runningState)
        model:0
        function evaluateRunning(){
            var evalState=false;
            for(var i=0;i<model;i++){
                var tableObject=instantiatorSubTables.objectAt(i)
                if(tableObject) evalState=(evalState || tableObject.runningState)

            }
            console.log("evaluate relation running "+evalState)
            return evalState
        }

        function addSubTable(subTableName,subForeignKey){
            //                Istruzione levata altrimenti risulta che la relation table sta caricando anche se non fa nulla
            //                instantiatorRelations.runningState=true
            console.log("SubTable request "+subTableName )


            if(subTableForeignKeysMap[subTableName]===undefined){
                subTableForeignKeysMap[subTableName]=[]
            }
            subTableForeignKeysMap[subTableName].push(subForeignKey)
            console.log("subTableForeignKeys on "+subTableName+" "+subTableForeignKeysMap[subTableName])


            //there is only one instance of relation table for each tableName, in that creation occurrence the instantiator fill the
            //model with a new number that corrispond through delegate to an InterfaceData model
            if(listSubTables.indexOf(subTableName)===-1){
                listSubTables.push(subTableName)
                console.log("SubTables "+listSubTables)
                model++

            }

        }

        function addForeignKey(referenceTable,referenceIndex,foreignKey){

        }


        function objectItem(tableName){
            return instantiatorSubTables.objectAt(listSubTables.indexOf(tableName))
        }

        function clear(){
            active=false
            listSubTables=[]
            numLoadedTables=0
            model=0

        }

        delegate: InterfaceData{

            contentPath: "/"+instantiatorSubTables.listSubTables[model.index]
            onErrorHappened: {
                errorDialog.subtitle=text
                errorDialog.open()
            }
            //                contentOrder:"id.asc"
            indexesPath: "keys"
            //importante mettere schemaKey dopo indexesKey perchè quest'ultimo viene utilizzato
            //all'interno di setSchemaKey
            schemaPath: "schemes"
            schemaKey: instantiatorSubTables.listSubTables[model.index]
            Component.onCompleted:{
//                start loading items with initRoles
                initRoles()
                //                console.log("added index"+model.index+"table"+instantiatorTables.listTables[model.index])
                console.log("added subTable"+model.index+instantiatorSubTables.listSubTables[model.index]+" rowCount "+rowCount())
                var primaryKeyName=primaryKey()
                if(primaryKeyName.length){
                    contentOrder=primaryKeyName+".desc"
                }
                //the loadContents is not considered already because it has to know how partially load record.

                //##                    loadContents()
            }
            onForeignKeyRequest: function(foreignKey,referenceTable,referenceIndex){
                console.log("kuickgest: foreignKeyRequest on",schemaKey,foreignKey,referenceTable,referenceIndex)
                var subForeignKeys=instantiatorSubTables.subTableForeignKeysMap[schemaKey]
                if(subForeignKeys!==undefined){
                    if(subForeignKeys.indexOf(foreignKey)!==-1){
                        console.log("add sub foreign key for schemaKey "+schemaKey+" foreignKey "+foreignKey+" to referenceTable "+referenceTable+" with referenceIndex "+referenceIndex)
                        var referenceTableIndex=instantiatorTables.listTables.indexOf(referenceTable)
                        var referenceTableObject=instantiatorTables.objectAt(referenceTableIndex)
                        referenceTableObject.subTablesList.push(schemaKey)
                        referenceTableObject.subForeignKeysList.push(foreignKey)
                        referenceTableObject.subReferenceIndexesList.push(referenceIndex)
                    }
                }

            }
            onRunningStateChanged: instantiatorSubTables.runningState=instantiatorSubTables.evaluateRunning()
            onLoadCompleted: {

                instantiatorSubTables.numLoadedTables++
                console.log("subTable num"+instantiatorSubTables.numLoadedTables+" name "+instantiatorSubTables.listSubTables[model.index]+" "+instantiatorSubTables.model+" rowCount "+rowCount())
                //                if(instantiatorRelations.numLoadedTables===instantiatorRelations.model) instantiatorRelations.loaded=true
            }


        }


    }

    Instantiator{
        id:instantiatorActions
        //solutions becouse in asynchronous mode of instantiator element are disposed in reverse order
        active: false
        //solutions becouse in asynchronous mode of instantiator element are disposed in reverse order
        asynchronous: true
        delegate: Kirigami.Action{
            text: instantiatorActions.model[index]
            icon.name: "table"
            //            pagePool: mainPagePool
            //            page: "pages/TablePage.qml"
            //            useLayers: true
            checkable: true
            ActionGroup.group: tableActionGroup
            onTriggered: {
                if(pageStack.layers.currentItem!==mainPagePool.pageForUrl("pages/TablePage.qml")) actionTable.trigger()

                var tablePage=actionTable.pageItem()
                console.log("Application: InstantiatorActions - tablePage "+tablePage)
                //solutions becouse in asynchronous mode of instantiator element are disposed in reverse order
                tablePage.setModel(instantiatorTables.objectAt(instantiatorActions.count-index-1),instantiatorActions.model[index])
            }

        }
        onObjectAdded: function(index, object){
            console.log("Action objext added")
            actionsGlobalDrawer.actions.push(object)
        }
//        onObjectRemoved: {
//            tableActionGroup.removeAction(object)
//            console.log("object removed "+object+" on lenght "+actionsGlobalDrawer.actions.length)
//            var tmpActions=[]
//            //                    actionsGlobalDrawer.actions
//            for( var i = 0; i < actionsGlobalDrawer.actions.length; i++){


//                if ( actionsGlobalDrawer.actions[i] !== object) {
//                    tmpActions.push(actionsGlobalDrawer.actions[i])

//                }

//            }
//            if(object===objectAt(count-1)) actionsGlobalDrawer.actions=tmpActions
//            console.log("after length "+objectAt(count-1)+" object "+object)
//        }
        function clear(){
//            important to avoid actions with preesistent connection tableName list considered undefined
            active=false
            model=0
        }

    }


    ActionGroup{
        id: tableActionGroup

    }
    ActionGroup{
        id: drawerActionGroup
    }

    pageStack.layers.onCurrentItemChanged:  {
        //the referement is explicit to layers because mainPagePool.lastLoadedItem is not valid for MainPage when clicked back button
        if(pageStack.layers.currentItem!==actionTable.pageItem()){
//            tableActionGroup.checkedAction=null
        }

        console.log("current layer item changed "+pageStack.layers.currentItem+" pageStack currentItem "+ pageStack.currentItem+" pageStack "+pageStack+" isMobile "+Kirigami.Settings.isMobile)
    }


    Kirigami.PromptDialog{
        id: errorDialog
        title: i18n("Error")
        showCloseButton: true
    }


    BusyIndicator{
        id:progressBusyIndicator
        x:root.width-width
        y:pageStack.globalToolBar.height
        running: serviceTests.runningState || schemaShow.runningState || instantiatorTables.runningState || instantiatorRelations.runningState
        onRunningChanged: {
//            console.log("Application: busyIndicator running "+running)
        }
    }


    ProgressBar {
        id:progressBar

        //                il valore si imposta ad 1 per poter avvertire la variazione changed
        value:1
        //            indeterminate: true
        visible: false
        Layout.alignment: Qt.AlignCenter
        x:root.width-width
        y:height/2
        onValueChanged:{
        pageStack.items.contains()

        }
    }


    // pageStack.onWideModeChanged: console.log("Application: wideMode "+pageStack.wideMode+ " wideScreen "+wideScreen+" windowWidth "+root.width)
    // pageStack.onPagePushed: console.log("Application: pagePushed "+page)
    // pageStack.onCurrentIndexChanged: console.log("Application: currentIndex changed "+pageStack.currentIndex)


    function localDateFormat(){
        var localParameters= Qt.locale()
        var dateFormat=localParameters.dateFormat(Locale.ShortFormat)
        while(dateFormat.indexOf("yyyy")===-1){
            dateFormat=dateFormat.replace("y","yy")
        }
        return dateFormat
    }
    function replaceAll(string, search, replace) {
      return string.split(search).join(replace);
    }

}
