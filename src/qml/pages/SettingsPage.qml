/*
Kuickgest: Resource managment system interfaced through REST API.

Copyright (c) 2022 Marcello Di Guglielmo

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

import QtQuick 2.6

// property var serverField. Then inside urlField: Component.onCompleted: serverField = urlField. Access that field by serverField
import QtQuick.Controls 2.14
//import plasma.kuickgest.InterfaceSettings 1.0
import org.kde.kirigami 2.19 as Kirigami
import QtQuick.Layouts 1.2
import Qt.labs.settings 1.0
Kirigami.ScrollablePage {
    // Sheet mode

    id: settingsRoot
    title: i18nc("@title:window","Settings")

    Settings{
        id:settings
        property string password: ""
        property string login: ""
        property string server: ""
    }


//    InterfaceSettings {
//        id:settingsConfiguration
//    }



    Kirigami.FormLayout {
//        height: urlField.height+qualitySlider.height+clearAllButton.height+120
        // Textfields let you input text in a thin textbox

        TextField {
            id: urlField
//            width: parent.width
            //                EnterKey.onClicked: accept()

//            text:connectSheet.connectionUrl
            text: settings.server
            // Provides label attached to the textfield
            Kirigami.FormData.label: i18nc("@label:textbox", "Url")
            // Placeholder text is visible before you enter anything
            placeholderText: i18n("Http Server host URL")
            // What to do after input is accepted (i.e. pressed enter)
            // In this case, it moves the focus to the next field
            onAccepted: acceptButton.forceActiveFocus()
            onTextChanged: settings.server=text
        }
        TextField {
            id: loginField
            placeholderText: i18n("Access credential")
//            width: parent.width
            text: settings.login
            echoMode: TextInput.Normal
            //                EnterKey.onClicked: accept()

            // Provides label attached to the textfield
            Kirigami.FormData.label: i18nc("@label:textbox", "Login")
            // Placeholder text is visible before you enter anything

            // What to do after input is accepted (i.e. pressed enter)
            // In this case, it moves the focus to the next field
            onAccepted: acceptButton.forceActiveFocus()
            onTextChanged: settings.login=text
        }
        TextField {
            id: passwordField
            placeholderText: i18n("Server Password")
//            width: parent.width
            text: settings.password
            echoMode: TextInput.Password
            //                EnterKey.onClicked: accept()

            // Provides label attached to the textfield
            Kirigami.FormData.label: i18nc("@label:textbox", "Password")
            // Placeholder text is visible before you enter anything

            // What to do after input is accepted (i.e. pressed enter)
            // In this case, it moves the focus to the next field
            onAccepted: acceptButton.forceActiveFocus()
            onTextChanged: settings.password=text
        }
        // This is a button.



    }





}


