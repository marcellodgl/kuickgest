/*
Kuickgest: Resource managment system interfaced through REST API.

Copyright (c) 2022 Marcello Di Guglielmo

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

import QtQuick 2.10
import QtQuick.Controls 2.5
import org.kde.kirigami 2.19 as Kirigami
import QtQuick.Layouts 1.2

Kirigami.Page {
    id: pageRoot
    //    property bool mouseActive: false
    enum Status{Connected, Disconnected}
    property int status: MainPage.Status.Disconnected

    property string tableRange : ""
    title: i18nc("@title","Kuickgest")
    property var cardObjects: ({})
    
    
    



    Kirigami.ScrollablePage{
        anchors.fill: parent
        ColumnLayout{

            Kirigami.CardsLayout {
                visible: !root.pageStack.wideMode
                Layout.topMargin: Kirigami.Units.largeSpacing
                Layout.leftMargin: Kirigami.Units.gridUnit
                Layout.rightMargin: Kirigami.Units.gridUnit
                Repeater {
                    id: cardRepeater
                    focus: true
                    model: root.model
                    Component.onCompleted:{
                        console.log("model")
                        console.log(root.model)
                    }
                    onItemAdded: function(index,item) {
                        console.log("item added "+item.banner.title)
                        cardObjects[item.banner.title]=item
                    }
                    delegate: Kirigami.Card {
                        id: listItem
                        banner {
                            //                        source: Qt.resolvedUrl(img)
                            title: model.type
                            titleAlignment: Qt.AlignBottom | Qt.AlignLeft
                            titleIcon: "table"
                            //                            source:"qrc:/kuickgest/icons/table.svg"
                        }
                        property int tableIndex: -1
                        property string tableContentRange: ""
                        property string tableContentAdaptedQuery: ""
                        property string tableContentAdaptedOrder: ""

                        Rectangle {
                            anchors.fill: parent
                            color: "transparent"
                            border {
                                width: listItem.activeFocus ? 2 : 0
                                color: Kirigami.Theme.activeTextColor

                            }
                        }
                        activeFocusOnTab: true
                        showClickFeedback: true
                        onClicked: {
                            //solutions becouse in asynchronous mode of instantiator element are disposed in reverse order
                            var tableAction=actionsInstantiator.objectAt(actionsInstantiator.count-index-1)
                            tableAction.trigger()
                        }
                        highlighted: action.checked

                        Keys.onReturnPressed: action.trigger()
                        Keys.onEnterPressed: action.trigger()
                        //                    Not used highlighted because referred to a unique page so are all cards illuminated
                        //                    highlighted: action.checked
                        implicitWidth: Kirigami.Units.gridUnit * 30
                        Layout.maximumWidth: Kirigami.Units.gridUnit * 30
                        Kirigami.Action {
                            id: action
                            checked: if(actionsInstantiator.object && actionsInstantiator.object.ActionGroup.group.checkedAction ){

                                         return actionsInstantiator.object.ActionGroup.group.checkedAction===actionsInstantiator.objectAt(index)
                                     }
                                     else{

                                         return false
                                     }

                            //                            onCheckedChanged: console.log("action checked changed"+actionsInstantiator.object.ActionGroup.group.checkedAction+" object at index "+actionsInstantiator.objectAt(index) )

                        }
                        contentItem: Column{

                            RowLayout{
                                width: listItem.contentItem.width

                                Kirigami.Heading{
                                    text: "Index:"
                                    type: Kirigami.Heading.Secondary
                                    level: 3
                                }
                                Label{
                                    text: tableIndex===-1 ? "" : tableIndex
                                    Layout.fillWidth: true
                                }
                            }
                            RowLayout{
                                width: listItem.contentItem.width

                                Kirigami.Heading{
                                    text: "Range:"
                                    type: Kirigami.Heading.Secondary
                                    level:3
                                }
                                Label{
                                    text: listItem.tableContentRange
                                    Layout.fillWidth: true
                                }
                            }
                            RowLayout{
                                width: listItem.contentItem.width

                                Kirigami.Heading{
                                    text: "Query:"
                                    type: Kirigami.Heading.Secondary
                                    level:3
                                }
                                Label{
                                    text: listItem.tableContentAdaptedQuery
                                    wrapMode: Text.Wrap
                                    Layout.fillWidth: true
                                }


                            }
                            RowLayout{
                                width: listItem.contentItem.width

                                Kirigami.Heading{
                                    text: "Order:"
                                    type: Kirigami.Heading.Secondary
                                    level:3
                                }
                                Label{
                                    text: listItem.tableContentAdaptedOrder
                                    wrapMode: Text.Wrap
                                    Layout.fillWidth: true
                                }

                            }
                        }
                    }
                }
            }




        }
    }
    
    
    function elaborateQuery(data){
        var equalPosition=data.indexOf("=")
        var dotPosition=data.indexOf(".")
        var item=data.slice(0,equalPosition)
        var operator=data.slice(equalPosition+1,dotPosition)
        var value=data.slice(dotPosition+1)
        var operatorKey=""
        if(operator==="eq"){
            operatorKey="="
        }
        else if(operator==="ilike"){
            operatorKey="~"
            value=value.slice(1)
            value=value.slice(0,value.length-1)
        }
        else if(operator==="gte"){
            operatorKey="≥"
        }
        else if(operator==="lte"){
            operatorKey="≤"
        }
        else if(operatorKey==="is" && value==="null"){
            operatorKey="="
            value="Ø"
        }
        var displayValue
        //                if(mainQueryItems.indexOf(item)===-1) displayValue=root.mapper.itemDisplay(item)
        //                else displayValue=root.mapper.secondaryItemDisplay(item)

        var query=item+"  "+operatorKey+"  "+value
        //                mainQueryItems.push(item)
        //prendere il secondo displayValue
        console.log("item "+item+" operator "+operator+" value "+value+" display "+displayValue+" query "+query)
        return query



    }






    function elaborateOrder(data){
        var item=data.split(".")[0]
        var versus=data.split(".")[1]
        var versusKey= versus==="asc" ? "\u0056" : "\u0245"
        var order=item+"  "+versusKey
        return order
    }


    function setTableIndex(table,index){
        if(table.length){
            var card=cardObjects[table]
            console.log("cardObjects "+cardObjects+" table "+table+" card "+card+" index "+index)
            card.tableIndex=index
        }
    }
    function tableIndex(table){
        if(table.length){
            var card=cardObjects[table]
            if(card !== undefined){
                return card.tableIndex
            }
            else{
                return -1
            }
        }
        else{
            return -1
        }
    }

    function setTableContentRange(table,contentRange){
        if(table.length){
            var card=cardObjects[table]
            card.tableContentRange=contentRange
            console.log("cardObjects "+cardObjects+" table "+table+" card "+card+" contentRange "+contentRange)
        }
    }
    function setTableContentQuery(table,contentQuery){
        if(table.length){
            var card=cardObjects[table]
            var contentQueryList=contentQuery.split("&")
            var queryAdaptedList=[]
            for(var i in contentQueryList){
                var query=contentQueryList[i]
                var queryAdapted=elaborateQuery(query)
                queryAdaptedList.push(queryAdapted)
            }
            card.tableContentAdaptedQuery=queryAdaptedList.join(" - ")

            //            queryListView.model=card.tableContentQueryList
            console.log("cardObjects "+cardObjects+" table "+table+" card "+card+" contentQuery "+contentQuery+" tableContentQueryList "+card.tableContentAdaptedQuery)
        }
    }
    function setTableContentOrder(table,contentOrder){
        if(table.length){
            var card=cardObjects[table]
            var contentOrderList=contentOrder.split(",")
            var orderAdaptedList=[]
            for(var i in contentOrderList){
                var order=contentOrderList[i]
                var orderAdapted=elaborateOrder(order)
                orderAdaptedList.push(orderAdapted)
            }
            card.tableContentAdaptedOrder=orderAdaptedList.join(" - ")

            //            queryListView.model=card.tableContentQueryList
            console.log("cardObjects "+cardObjects+" table "+table+" card "+card+" contentOrder "+contentOrder+" tableContentOrderList "+card.tableContentAdaptedOrder)
        }
    }
}









