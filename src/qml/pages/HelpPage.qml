/*
Kuickgest: Resource managment system interfaced through REST API.

Copyright (c) 2022 Marcello Di Guglielmo

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

import QtQuick 2.10
import QtQuick.Controls 2.5
import org.kde.kirigami 2.19 as Kirigami
import QtQuick.Layouts 1.2

Kirigami.ScrollablePage {
    id: pageRoot
    property string tableRange : ""
    title: i18nc("@title","Help")


    TextArea{
        id: textHelp
        readOnly: true
        textFormat: TextEdit.MarkdownText
        wrapMode: Text.WordWrap
        Component.onCompleted: {
            readTextFile("qrc:/kuickgest/doc/manual.md")
        }
    }

    function readTextFile(fileUrl){
       var xhr = new XMLHttpRequest;
       xhr.open("GET", fileUrl); // set Method and File
       xhr.onreadystatechange = function () {
           if(xhr.readyState === XMLHttpRequest.DONE){ // if request_status == DONE
               var response = xhr.responseText;

               response=root.replaceAll(response,"](images/","](qrc:/../../kuickgest/doc/images/")
               console.log("HelpPage: response "+response)
               textHelp.text = response
           }
       }
       xhr.send(); // begin the request
    }

}
