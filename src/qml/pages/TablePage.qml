/*
Kuickgest: Resource managment system interfaced through REST API.

Copyright (c) 2022 Marcello Di Guglielmo

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

import QtQuick 2.10
import QtQuick.Controls 2.13
import org.kde.kirigami as Kirigami
import QtQuick.Layouts 1.2
import "tools"
Kirigami.ScrollablePage {
    id: pageRoot
    //    property bool mouseActive: false
    enum Status{Connected, Disconnected}
    property int status: MainPage.Status.Disconnected
    property var itemsRoleList: []
    property string tableName: ""
    property int fromRange: 0
    property int toRange: 1
    property int valueRange: 0
    property bool subTabled: false
    title: tableName
    Layout.fillWidth: true
    //    supportsRefreshing: true

    actions: [
        Kirigami.Action{
            text: i18n("Add")
            icon.name: "list-add"
            onTriggered: addObject()
        },
        Kirigami.Action{
            text: i18n("Last")
            icon.name: "arrow-down-double"
            onTriggered: lastObjects()
        },
        Kirigami.Action{
            id: actionSubTables
            text: i18n("Subtables")
            icon.name: "add-subtitle"
            enabled: subTabled && mainListView.currentIndex!==-1
            onTriggered: {
                var model=mainListView.model
                var subForeignKeysList=model.subForeignKeysList
                var subReferenceIndexesList=model.subReferenceIndexesList
                var constraintsList=[]
                for(var i=0;i<model.subTablesList.length;i++){
                    var foreignKey=subForeignKeysList[i]
                    var referenceIndex=subReferenceIndexesList[i]

                    var itemRoles=mainListView.model.itemsRoleList()
                    var referenceRole=-1
                    for(var j in itemRoles)
                    {
                        var role=itemRoles[j]
                        var roleName=mainListView.model.itemName(role)
                        if(roleName===referenceIndex){
                            referenceRole=role
                            break
                        }
                    }
                    if(referenceRole>=0){
                        var currentModelIndex=mainListView.model.index(mainListView.currentIndex,0)
                        var constraint=foreignKey+"=eq."+mainListView.model.data(currentModelIndex,referenceRole)
                        constraintsList.push(constraint)
                        console.log("TablePage: currentIndex "+mainListView.currentIndex+" currentModelIndex "+currentModelIndex+" foreignKey "+foreignKey+" referenceIndex "+referenceIndex+" constraint "+constraint)
                    }
                    else{
                        //case referenceRole is not found the constraintsList fail
                        constraintsList=[]
                        break
                    }

                }
                if(constraintsList.length){
                    bottomDrawer.setConstraints(constraintsList)
                    bottomDrawer.open()
                }
            }
        },
        Kirigami.Action{
            text: i18n("Previous")
            icon.name: "arrow-up"
            enabled: valueRange<toRange
            onTriggered: {
                if (mainListView.model.goNext()){
                    mainListView.model.loadContents()
                }
            }
        },
        Kirigami.Action{
            text: i18n("Find")
            icon.name: "edit-find"
            onTriggered: findObjects()
        },
        Kirigami.Action{
            text: i18n("All")
            icon.name: "go-top"
            onTriggered: allObjects()
        },
        Kirigami.Action{
            text: i18n("Update")
            icon.name: "update"
            onTriggered: mainListView.model.loadContents()
        },
        Kirigami.Action{
            text: i18n("Order")
            onTriggered: orderDialog.open()
        },
        Kirigami.Action{
            id: actionSwipe
            text: i18n("Swipe")
            icon.name: "auto-scale-x"
            checkable: true
        }
    ]

    SubTableDrawer{
        id: bottomDrawer
        onClosed: {
            console.log("visible "+visible)

        }

    }

    Component {
        id: delegateComponent
        Kirigami.SwipeListItem {
            id: listItem
            width: ListView.view ? ListView.view.width : implicitWidth
            height: root.wideScreen ? Kirigami.SwipeListItem.implicitHeight : Kirigami.Units.iconSizes.smallMedium*(5/2)
            highlighted: mainListView.currentItem==listItem
            onClicked: mainListView.currentIndex=index
            onPressed: console.log("TablePage: SwipeListItem pressed")
            Keys.onReturnPressed: loadSample()
            contentItem: ListView{
                id:rowView
                interactive: actionSwipe.checked
                MouseArea{
                    id: rowMouseArea
                    anchors.fill: parent
                    onClicked: {
                        console.log("TablePage: MouseArea cliched")
                        //The reason use of Mouse area is that rowView is upper the Swipe listItem and mask inputs.
                        mainListView.currentIndex=index
                        changeSampleIndex(index)

                    }
                    onDoubleClicked: actionSampleEdit.trigger() //loadSample()

                }


                header: ColumnLayout{
                    spacing: 0
                    Kirigami.Icon {
                        id:itemIcon
                        source: "item"
                        Layout.fillHeight: true
                        Layout.maximumHeight: Kirigami.Units.iconSizes.smallMedium
                        Layout.preferredWidth: height

                    }
                    Label {
                        id: labelIndex
//                        text: mainListView.count-model.index
                        text: model.index
                        fontSizeMode: Text.Fit
                        horizontalAlignment: Text.AlignHCenter
                        Layout.maximumHeight:Kirigami.Units.iconSizes.small
                        //important to specify minimumPointSize
                        Layout.preferredWidth: itemIcon.width
                        minimumPointSize: 3
                        color: listItem.checked || rowMouseArea.pressed || (listItem.pressed && !listItem.checked && !listItem.sectionDelegate) ? listItem.activeTextColor : listItem.textColor

                    }

                }
                orientation: ListView.Horizontal
                model:delegateModel
                spacing: 20
                clip:true
                ListModel{
                    id:delegateModel
                }
                delegate: ColumnLayout {
                    spacing: 0
                    Kirigami.Heading {
                        level: 5
                        type: Kirigami.Heading.Type.Secondary
                        text: LabelElement
                        font.bold: true
                    }
                    Label {
                        Layout.fillWidth: true
                        Layout.maximumWidth: Kirigami.Units.largeSpacing*40
                        text: TextElement
                        elide: Text.ElideRight
                        wrapMode: Text.WordWrap
                    }
                }
            }
            
            actions: [
                Kirigami.Action {
                    icon.name: "delete"
                    text: i18n("Remove")
                    onTriggered: confirmationRemoveDialog.open()
                },
                Kirigami.Action {
                    icon.name: "document-edit"
                    id: actionSampleEdit
                    text: i18n("Edit")
                    onTriggered: {
                        mainListView.currentIndex=index
                        changeSampleIndex(index)
                        loadSample()
                    }
                }
            ]

            function updateRole(roleName,roleRelationTable,roleRelationDisplay,roleFormat){
                var roleText=""
                var roleRelationDisplayValue=""
                var roleValue=model[roleName]
                if(roleFormat==="date"){
                    
                    var dateText=roleValue ? roleValue.toString() : ""
                    //                            var localeParameters=Qt.locale()
                    //                            var roleDate = Date.fromLocaleDateString(localeParameters,dateText,"yyyy-MM-dd")
                    var dateFormat=root.localDateFormat()
                    //                            roleText= Qt.formatDate(dateText,Qt.DefaultLocaleShortDate)
                    roleText= Qt.formatDate(dateText,dateFormat)
                    //                            roleText=roleDate.toLocaleDateString(localeParameters,Locale.ShortFormat)
                    //                            console.log("roleData "+dateText+" data "+roleText+"dateFormat"+"roleDate"+roleDate)
                    
                }
                else if(roleFormat==="numeric"){
                    roleText=roleValue ? Number(roleValue).toLocaleString() : ""
                }
                else if(roleRelationTable.length){
                    if(roleValue){
                        roleRelationDisplayValue=roleValue[roleRelationDisplay]
//                        Important use method toString() becouse displayValue could be a number or integer that could not be directly passed to TextElement
                        roleText=(roleRelationDisplayValue ? roleRelationDisplayValue.toString() : "")
                    }
                    else{
                        roleText=""
                    }
                }
                
                //+                        else if(roleRelationTable.length)
                //                        {
                //                            var relModel=instantiatorRelations.objectItem(roleRelationTable)
                //                            if(relModel && roleValue){
                //                                var relItemsValue=relModel.catchValueMap(roleValue)
                //                                roleText=relItemsValue[roleRelationDisplay]
                ////                                var indexText=roleValue ? roleValue.toString() : ""
                ////                                console.log("index text "+indexText+" relText "+relText)
                //                            }
                //                        }
                else{
                    roleText=(roleValue ? roleValue.toString() : "")
                }
                
                console.log("TablePage: updateRole - append roleName ",roleName," roleValue ",roleValue,"roleFormat",roleFormat,"roleRelationTable",roleRelationTable,"roleRelationDisplay",roleRelationDisplay,"roleRelationDisplayValue",roleRelationDisplayValue,"roleText",roleText)
                rowView.model.append({"LabelElement":roleName,"TextElement" : roleText})
            }
            function clearRoles(){
                rowView.model.clear()
            }

            Component.onCompleted: {
                //deve essere formalmente predisposto l'update in quanto i delegate vengono caricati in fase
                //di avviamento successivamente ai date changed e quindi al loro interno non vengono inseriti
                //i valori.
                var itemRoles=mainListView.model.itemsRoleList()
                for(var i in itemRoles)
                {
                    var role=itemRoles[i]
                    var roleName=mainListView.model.itemName(role)
                    //il model è quello generale del delegate di tutta la riga della pagina test
                    var roleRelationTable=mainListView.model.itemRelationTable(role)
                    var roleRelationDisplay=mainListView.model.itemRelationDisplay(role)
                    var roleFormat=mainListView.model.itemFormat(role)
                    console.log("updateDelegate role"+role+" roleName "+roleName)
                    updateRole(roleName,roleRelationTable,roleRelationDisplay,roleFormat)
                }
            }
        }
    }

    ListView {
        id: mainListView
        model:10
        //        highlightFollowsCurrentItem seems not operate as planned behaviour, used manual comparison
        //        highlightFollowsCurrentItem: true
        onCurrentIndexChanged: {
            console.log("TablePage: currentIndex "+currentIndex)
            console.log("TablePage: currentItem "+currentItem)
            var samplePage=root.pagePool.pageForUrl("pages/SamplePage.qml")
            if(samplePage.model===mainListView.model){
                samplePage.setIndex(mainListView.currentIndex)
            }
            var tablePage=root.pagePool.pageForUrl("pages/TablePage.qml")
            var mainPage=root.pagePool.pageForUrl("pages/MainPage.qml")
            samplePage.state=""
            if(currentIndex==-1){
                mainPage.setTableIndex(tableName,-1)
            }
            else{
                mainPage.setTableIndex(tableName,currentIndex)
            }

            if(!root.wideScreen) {
                root.pageStack.insertPage(1,samplePage)
                //                root.pageStack.push(tablePage)
            }
        }
        onVerticalOvershootChanged: listViewVOvershoot(mainListView.verticalOvershoot)
        //        interactive: false
        Timer {
            id: refreshRequestTimer
            interval: 3000
            onTriggered: pageRoot.refreshing = false
        }
        moveDisplaced: Transition {
            YAnimator {
                duration: Kirigami.Units.longDuration
                easing.type: Easing.InOutQuad
            }
        }
        delegate: delegateComponent
        /*header: Pane{
            z:2
            width: mainListView.width
            height: headerProgressBar.height
            padding: 0*/
        header:RowLayout{
            width: mainListView.width
            z:2
            ProgressBar {
                id: headerProgressBar
                Layout.alignment: Qt.AlignCenter
                from: fromRange
                to: toRange
                value: valueRange
            }
        }
        /*}*/
        footer: Pane{

        }
        footerPositioning: ListView.OverlayFooter
        headerPositioning: ListView.OverlayHeader
    }

    Kirigami.PromptDialog {
        id: confirmationRemoveDialog
        title: "Confirmation"
        subtitle: i18n("Are you sure to remove selected row?")
        standardButtons: Kirigami.Dialog.Yes | Kirigami.Dialog.No

        onAccepted: {
            console.log("TablePage: Removing row "+mainListView.currentIndex)
            mainListView.model.remove(mainListView.currentIndex)
        }
        onRejected: console.log("TablePage: Remove rejected")
    }

    OrderDialog{
        id: orderDialog
        onAccepted: {
            orderObjects(getOrderSequence())
        }
    }

    //Update that correspond to dataChanged or first load of contents in listView Model
    function updateDelegate(topLeft,bottomRight){
        //si verifica se nell'aggiornamento che c'è stato al modello riguardi proprio la riga del presente delegate
        //riempe la riga della pagina
        var item=mainListView.itemAtIndex(topLeft.row)
        item.clearRoles()
        var itemRoles=mainListView.model.itemsRoleList()
        
        for(var i in itemRoles)
        {
            
            var role=itemRoles[i]
            var roleName=mainListView.model.itemName(role)
            //il model è quello generale del delegate di tutta la riga della pagina test
            var roleRelationTable=mainListView.model.itemRelationTable(role)
            var roleRelationDisplay=mainListView.model.itemRelationDisplay(role)
            var roleFormat=mainListView.model.itemFormat(role)
            console.log("TablePage: updateDelegate - role"+role+" roleName "+roleName)
            item.updateRole(roleName,roleRelationTable,roleRelationDisplay,roleFormat)
        }
    }
    function updateContentRange(contentRange){
        console.log("TablePage: updateContentRange "+contentRange)
        var minmaxRange=contentRange.split("/")[0]
        var numItems=contentRange.split("/")[1]
        var minRange=minmaxRange.split("-")[0]
        var maxRange=minmaxRange.split("-")[1]

        fromRange=parseInt(minRange)
        toRange=parseInt(numItems)
        valueRange=parseInt(maxRange)+1
    }

    function setModel(model,name){
        console.log("Model "+model+" pagePool "+root.pagePool+" name "+name+" subTables "+model.subTablesList+" subforeignKeys "+model.subForeignKeysList+" subReferenceKeys "+model.subReferenceIndexesList)
        tableName=name
        //        mapper has to be cleared before mainListView model because in clearing that there are movement with mapper currentIndex that refer to an inexistent model
        if(model){
            var mainPage=root.pagePool.pageForUrl("pages/MainPage.qml")
            var tableIndex=mainPage.tableIndex(tableName)

            //loaded because the object has to be instantiated and for samplePage there isn't trigger
            var samplePage=root.pagePool.loadPage("pages/SamplePage.qml")
            mainListView.model=model
            bottomDrawer.setTables(model.subTablesList)


            if(model.subTablesList.length){
                subTabled=true
            }
            else{
                subTabled=false
            }

            console.log("after pushed model "+tableName)

            updateContentRange(model.contentRange)
            //si preddisponngono i widget del mapper della pagina dei sample perchè possano essere riempiti in fase
            //di selezione nella seconda pagina
            samplePage.setModel(model,tableName)
            orderDialog.tablesList=samplePage.tableNameList

//            var orderSequence=model.primaryKey()+".desc"
            var orderSequence=model.contentOrder
            orderDialog.setOrderSequence(orderSequence)
            console.log("tableNameList "+samplePage.tableNameList+" orderSequence "+orderSequence)


            //currentIndex=-1 for deselect first item on start of tablePage
//            tableIndex has to be cached before mainListView.setModel becouse on it there is a setCurrentIndex to 0 that interfere with tableIndex update
            console.log("TablePage: table - "+tableName+" currentIndex "+tableIndex)
            mainListView.currentIndex=tableIndex
            //forceActiveFocus for keyboard navigation enabled on table selected
            forceActiveFocus()
        }
    }

    function loadSample(){
        //        console.log("TablePage: loadSample index "+index)
        var samplePage=root.pagePool.pageForUrl("pages/SamplePage.qml")
        //        samplePage.title="Index "+(mainList.count-index)
        //        samplePage.verticalScrollBarPolicy=Qt.ScrollBarAlwaysOff
        //        samplePage.verticalScrollBarPolicy=Qt.ScrollBarAsNeeded
        if(mainListView.model!==samplePage.model){
            samplePage.setModel(mainListView.model,tableName)
        }
        root.pageStack.push(samplePage)
        samplePage.setIndex(mainListView.currentIndex)


    }
    function changeSampleIndex(index){
        //seems unuseful
        var samplePage=root.pagePool.pageForUrl("pages/SamplePage.qml")
//        samplePage.setIndex(index)
    }
    function orderObjects(orderSequence){
        mainListView.model.contentOrder=orderSequence
        mainListView.model.loadContents()

    }
    function addObject(){
        var samplePage=root.pagePool.pageForUrl("pages/SamplePage.qml")
        root.pageStack.push(samplePage)
        var tableModel=mainListView.model
        var rowCount=tableModel.rowCount()
        tableModel.add();
        //superfluo settare il mapper in quando al cambiamento di index si autoaggiorna anche il mapper
        mainListView.currentIndex=rowCount
    }
    function lastObjects(){
        var tableModel=mainListView.model
        tableModel.contentQuery=""
        var orderSequence=tableModel.primaryKey()+".desc"
        orderDialog.setOrderSequence(orderSequence)
        tableModel.contentOrder=orderSequence
        tableModel.numScreenItems=5
        tableModel.goLast()
        tableModel.loadContents()
        //reset currentIndex selection in other case currentIndex could be dirty for states (ex actionSubTables)
        mainListView.currentIndex=-1
    }
    function allObjects(){
        var tableModel=mainListView.model
        tableModel.contentQuery=""
        var orderSequence=tableModel.primaryKey()+".desc"
        orderDialog.setOrderSequence(orderSequence)
        tableModel.contentOrder=orderSequence
        tableModel.numScreenItems=0
        tableModel.goLast()
        tableModel.loadContents()
        //reset currentIndex selection in other case currentIndex could be dirty for states (ex actionSubTables)
        mainListView.currentIndex=-1
    }
    function findObjects(){
        var samplePage=root.pagePool.pageForUrl("pages/SamplePage.qml")
        root.pageStack.push(samplePage)
        console.log("TablePage: findObject - samplePage "+samplePage)
        mainListView.currentIndex=-1
        samplePage.state="search"

    }
    function listViewVOvershoot(value){
        if(Math.abs(value/pageRoot.height)>(1/10)){
            console.log("overshoot Vertical value "+value+"height"+pageRoot.height+" ratio "+value/pageRoot.height)

            if (mainListView.model.goNext()){
                mainListView.model.loadContents()
            }
        }
    }

}











