/*
Kuickgest: Resource managment system interfaced through REST API.

Copyright (c) 2022 Marcello Di Guglielmo

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

import QtQuick 2.10
import QtQuick.Controls 2.15
import org.kde.kirigami 2.19 as Kirigami
import QtQuick.Layouts 1.2

import "tools"

Kirigami.ScrollablePage {
    id: pageRoot
    property var model: root.mapper.model
    property var tableNameList: []
    signal itemCreated(int section)
    signal resetSamples()
    property string tableName: ""
//    title: Kirigami.Settings.isMobile ? i18nc("@title","Samples") : i18nc("@title","Samples - ")+tableName
    title: i18nc("@title","Samples - ")+tableName
    states: [
        State {
            name: "search"
            PropertyChanges { target: sampleActionSearch; visible: true }
            PropertyChanges { target: sampleActionReset; visible: true }
            PropertyChanges { target: sampleActionUndo; visible: false }
            PropertyChanges { target: sampleActionSave; visible: false }
        }
    ]
    ListModel{
        //        model used to represent fields row in Sample page.
        id: sampleModel
    }

    onHeightChanged: console.log("height "+(listSampleView.height-listSampleView.spacing*5-3)+" listSampleView height "+listSampleView.contentHeight)
    


    ListView{
        id: listSampleView
        topMargin: Kirigami.Units.smallSpacing
        //        verificare
        clip: true
        delegate: delegateSampleComponent
        model:sampleModel
        spacing: Kirigami.Units.smallSpacing
        section.property: "itemName"

        section.delegate: delegateSection
        //        footer: delegateSection


    }




    
    footer: Kirigami.NavigationTabBar{
        id:sampleNavigationTabBar
        actions: [
            Kirigami.Action{
                id: sampleActionUndo
                icon.name: "edit-undo"
                text: i18nc("@undo","undo")
                shortcut: StandardKey.Undo
                onTriggered: {
                    undoObject();
                }
            },
            Kirigami.Action{
                id: sampleActionSearch
                icon.name: "search"
                text: i18nc("@search","search")
                visible:false
                onTriggered: {
                    pageRoot.state=""
                    if(root.wideScreen) pageStack.pop()
                    else pageStack.flickBack();
                    searchObject();
                }
            },
            Kirigami.Action{
                id: sampleActionReset
                icon.name: "dialog-cancel"
                text: i18nc("@reset","reset")
//                shortcut: StandardKey.Cancel
                // Shortcut{
                //     enabled: sampleActionReset.visible
                //     sequence: StandardKey.Cancel
                //     onActivated: sampleActionReset.trigger()
                // }
                visible: false
                onTriggered: {
                    resetSamples();
                    root.mapper.setCurrentIndex(-1);
                }
            },
            Kirigami.Action{
                id: sampleActionSave
                icon.name: "document-save"
                text: i18nc("@save","save")
                shortcut: StandardKey.Save
                onTriggered: {
                    submitObject();
                }
            },
            Kirigami.Action{
                id: sampleActionClose
                icon.name: "dialog-close"
                text: i18nc("@close","close")
                shortcut: StandardKey.Close
                onTriggered: {
                    pageRoot.state=""
                    var tablePage=root.pagePool.pageForUrl("pages/TablePage.qml")
                    if(root.wideScreen) pageStack.pop()
                    else pageStack.flickBack();
                }
            }
        ]
    }


    Component{
        id:delegateSampleComponent
        Loader{
            id:loaderSample
            property string labelName: itemName
            property int lengthField: itemLength
            property bool listEnum: itemEnum
            property string relationTable: itemRelationTable
            property string relationDisplay: itemRelationDisplay
            property string relationIndex: itemRelationIndex
            property bool sampleEnabled: itemEnabled

            property int section : itemRole


            //incrementCurrentIndex incrementa il currentIndex di ListView
            Keys.onReturnPressed:listSampleView.incrementCurrentIndex()



            //importante che ogni component abbia il focus:true o almeno in Component.onCompleted focus=true
            focus:index===listSampleView.currentIndex
            //            questa mossa serve a impostare il corrente focus in base a quello che è attivato con una selezione
            onActiveFocusChanged: {
                if(focus) listSampleView.currentIndex=index
            }

            //            width: (listSampleView.height-listSampleView.spacing*5) < listSampleView.contentHeight ? pageRoot.contentItem.width -2*Kirigami.Units.largeSpacing : pageRoot.contentItem.width
            width: listSampleView.contentItem.width
            sourceComponent: formComponent(itemType,itemFormat,itemLength,itemEnum,itemEnabled,itemRelationTable)

        }

    }
    
    Component{
        id:textFormComponent
        RowLayout{
            id:formRow

            spacing: Kirigami.Units.smallSpacing
            Kirigami.Heading{
                Layout.leftMargin: Kirigami.Units.smallSpacing
                text: labelName
                level:3
            }

            ComboBox{
                id:textOperator
                model: ["~", "=","Ø"]
                //                flat:true
                Layout.preferredWidth:(parent.width)*(1/10)

                Layout.minimumWidth: 60
                visible: pageRoot.state==="search"
                Connections{
                    target: pageRoot
                    function onResetSamples(){
                        textOperator.currentIndex=0
                    }
                }
                onCurrentTextChanged: if(currentText=="Ø") textField.clear()



            }

            Kirigami.ActionTextField{
                id:textField
                property string operator: textOperator.currentText
                property bool altered : false
                onTextChanged: altered=true
                //tecnica per rendere possibile in ogni caso la ricerca anche se si tratta di primary key
                //                enabled: textOperator.visible ? textOperator.currentText!=="Ø" : sampleEnabled
                //                enabled: textOperator.visible  | sampleEnabled
                Layout.fillWidth: true
                focus:true
                maximumLength: lengthField
                Component.onCompleted: {
                    console.log("add textfield on section "+section+" name "+labelName+" item "+textField+" width "+width+" formRowWidth "+formRow.width+" type "+root.mapper.String)
                    root.mapper.addMapping(textField,section,root.mapper.typeString)
                    itemCreated(section)

                }


            }
            ToolButton{
                    id:cleanAction
                    onClicked: textField.clear()
                    flat:true
                    enabled: textOperator.visible ? textOperator.currentText!=="Ø" : sampleEnabled
                    Layout.rightMargin: Kirigami.Units.smallSpacing
                    icon.name: "edit-clear"
            }


        }

    }

    Component{
        id:scriptFormComponent

        RowLayout{
            spacing: Kirigami.Units.smallSpacing
            Kirigami.Heading{
                Layout.leftMargin: Kirigami.Units.smallSpacing
                text: labelName
                level:3
            }

            ComboBox{
                id:scriptOperator

                model: ["~", "=","Ø"]
                Layout.preferredWidth:(parent.width)*(1/10)

                Layout.minimumWidth: 60
                visible: pageRoot.state==="search"
                Connections{
                    target: pageRoot
                    function onResetSamples(){
                        scriptOperator.currentIndex=0
                    }
                }

                onCurrentTextChanged: if(currentText=="Ø") scriptField.clear()



            }


            TextArea{
                Layout.topMargin: height>scriptOperator.height ? 0 : Kirigami.Units.largeSpacing

                Layout.bottomMargin: height>scriptOperator.height ? 0 : Kirigami.Units.largeSpacing
                id:scriptField

                property string operator: scriptOperator.currentText
                property bool altered : false
                onTextChanged: altered=true
                //tecnica per rendere possibile in ogni caso la ricerca anche se si tratta di primary key
                enabled: scriptOperator.visible ? scriptOperator.currentText!=="Ø" : sampleEnabled
                //                enabled: textOperator.visible  | sampleEnabled
                wrapMode: Text.WordWrap
                focus:true
                Layout.fillWidth: true
                Component.onCompleted: {
                    console.log("add textfield on section "+section+" type "+root.mapper.Script)
                    root.mapper.addMapping(scriptField,section,root.mapper.typeScript)
                    itemCreated(section)
                }
            }
            //                }

            //            }
            ToolButton{
                id:cleanButton
                //                font.family: "fontello"
                //                text:qsTr("\uE821")
                flat: true
                onClicked: scriptField.clear()
                enabled: scriptOperator.visible ? scriptOperator.currentText!=="Ø" : sampleEnabled
                Layout.rightMargin: Kirigami.Units.smallSpacing
                icon.name: "edit-clear"
            }


        }

    }
    
    Component{
        id:dateFormComponent
        RowLayout{
            id:rowDate
            spacing: Kirigami.Units.smallSpacing
            Kirigami.Heading{
                Layout.leftMargin: Kirigami.Units.smallSpacing
                text: labelName
                level:3
            }

            ComboBox{
                id:dateOperator
                model: ["=","≤","≥","÷","Ø"]
                //                flat:true
                Layout.preferredWidth:(parent.width)*(1/10)

                Layout.minimumWidth: 60
                visible: pageRoot.state==="search"
                Connections{
                    target: pageRoot
                    function onResetSamples(){
                        dateOperator.currentIndex=0
                    }
                }
                onCurrentTextChanged: if(currentText=="Ø") dateField.clear()

            }

            Item{
                //                Item che ha ruolo di spacer perchè si espande alla massima disponibilità di larghezza
                id:spacer
                Layout.fillWidth: true
                //                preferredwidth a zero per nn far assumere una dimensione allo spacer,
                //                quando la larghezza disponibile si riduce gli altri componenti si riducono quando lo spazio finisce
                //                senza che però lo specer occupi spazio
                Layout.preferredWidth: 0

            }
            ToolButton{
                id:calendarButton
                Layout.alignment: Qt.AlignRight
                icon.name: "view-calendar"
                //                onClicked: dateField.clear()
                enabled: dateOperator.visible ? dateOperator.currentText!=="Ø" : sampleEnabled
                onClicked: {
                    var dateFormat=root.localDateFormat()
                    var dateCalendar=Date.fromLocaleDateString(Qt.locale(),dateField.text,dateFormat)
                    console.log("dateCalendar "+dateCalendar)
                    var dateText=Qt.formatDate(dateCalendar)
                    calendarSheet.open()
                    calendarSheet.selectedDate=dateCalendar
                }

                CalendarSheet{
                    id: calendarSheet
                    onVisibleChanged: {
                        if(!visible) {
                            if(selectedDate != null){
                                var dateFormat=root.localDateFormat()
                                dateField.text=selectedDate.toLocaleString(Qt.locale(),dateFormat)
                            }
                        }
                    }
                }
            }
            Kirigami.ActionTextField{
                id:dateField
                leftActions: Kirigami.Action{
                    id:downButton
                    icon.name: "arrow-down"
                    //                autoRepeat: true
                    onTriggered: {
                        var dateText=dateField.text
                        var dateFormat=root.localDateFormat()
                        var dateFormatList=dateFormat.split("/")
                        var date=Date.fromLocaleDateString(Qt.locale(),dateText,dateFormat)

                        var cursorPosition=dateField.cursorPosition
                        console.log("dateFormatList "+dateFormatList +"cursor Position "+cursorPosition)
                        console.log("date "+date)
                        var firstSeparator=dateText.indexOf("/")
                        var lastSeparator=dateText.lastIndexOf("/")
                        if(cursorPosition<=firstSeparator || cursorPosition===dateText.length){
                            if(dateFormatList[0].includes("d")) date.setDate(date.getDate()-1)
                            else if(dateFormatList[0].includes("M")) date.setMonth(date.getMonth()-1)
                            else if(dateFormatList[0].includes("y")) date.setFullYear(date.getFullYear()-1)

                        }
                        else if(cursorPosition>firstSeparator && cursorPosition<=lastSeparator){
                            if(dateFormatList[1].includes("d")) date.setDate(date.getDate()-1)
                            else if(dateFormatList[1].includes("M")) date.setMonth(date.getMonth()-1)
                            else if(dateFormatList[1].includes("y")) date.setFullYear(date.getFullYear()-1)

                        }
                        else if(cursorPosition>=lastSeparator){
                            if(dateFormatList[2].includes("d")) date.setDate(date.getDate()-1)
                            else if(dateFormatList[2].includes("M")) date.setMonth(date.getMonth()-1)
                            else if(dateFormatList[2].includes("y")) date.setFullYear(date.getFullYear()-1)
                        }

                        console.log("date minus"+date)
                        dateField.text=Qt.formatDate(date,dateFormat)
                        dateField.cursorPosition=cursorPosition

                    }

                }



                property string operator: dateOperator.currentText
                property bool altered : false
                property string secondaryText : secondaryDateField.text
                //                non si crea ricorsione in un loop infinito in quanto alla corrispondenza di un valueChanged si verifica se il valore è differente dal precedente
                //                solo in questo caso si procede a riaggiornare la proprietà
                Kirigami.FormData.label: labelName

                onTextChanged: altered=true
                //tecnica per rendere possibile in ogni caso la ricerca anche se si tratta di primary key
                enabled: dateOperator.visible ? dateOperator.currentText!=="Ø" : sampleEnabled
                //                enabled: textOperator.visible  | sampleEnabled
                Layout.fillWidth: true

                Layout.maximumWidth:  font.pointSize<=10 ? dateField.implicitWidth/(3/2) : dateField.implicitWidth
                horizontalAlignment: TextInput.AlignHCenter
                Layout.alignment: Qt.AlignRight
                focus:true
                inputMethodHints: Qt.ImhPreferNumbers
                //could happen date format of type d/M/yyyy
                inputMask: root.replaceAll(root.replaceAll(root.replaceAll(root.replaceAll(root.replaceAll(root.localDateFormat(),"MM","00"),"dd","00"),"d","00"),"y","0"),"M","00")
                //                validator: RegExpValidator{
                //                    property var localParameters: Qt.locale()
                //                    // slash all'interno di espressione regolare con \/

                //                    regExp: if(localParameters.dateFormat(Locale.ShortFormat).includes("dd/MM/yy")) {/((0[1-9]|[12]\d|3[01])\/(0[1-9]|1[0-2])\/[12]\d{3})/}
                //                            else if(localParameters.dateFormat(Locale.ShortFormat).includes("yy/MM/dd")) {/([12]\d{3}\/(0[1-9]|1[0-2])\/(0[1-9]|[12]\d|3[01]))/}
                //                            else { /.*/ }


                //                }
                //QT6
                //                validator: RegularExpressionValidator{
                //                    property var localParameters: Qt.locale()
                //                    // slash all'interno di espressione regolare con \/

                //                    regularExpression: if(localParameters.dateFormat(Locale.ShortFormat).includes("dd/MM/yy")) {/((0[1-9]|[12]\d|3[01])\/(0[1-9]|1[0-2])\/[12]\d{3})/}
                //                            else if(localParameters.dateFormat(Locale.ShortFormat).includes("yy/MM/dd")) {/([12]\d{3}\/(0[1-9]|1[0-2])\/(0[1-9]|[12]\d|3[01]))/}
                //                            else { /.*/ }


                //                }


                //                width: TextField.implicitWidth
                //                width: Layout.minimumWidth
                Component.onCompleted: {
                    console.log("datefield pointSize "+font.pointSize+" implicit width "+implicitWidth)
                    console.log("add textfield on section "+section+" type "+root.mapper.Date)

                    console.log("validator "+TextField.validator)

                    root.mapper.addMapping(dateField,section,root.mapper.typeDate)
                    itemCreated(section)

                }
                onSecondaryTextChanged: console.log("secondaryText changed "+secondaryText)


                rightActions: Kirigami.Action{
                    id:upButton
                    icon.name: "arrow-up"
                    //                autoRepeat: true
                    onTriggered: {
                        var dateText=dateField.text
                        var dateFormat=root.localDateFormat()
                        var dateFormatList=dateFormat.split("/")
                        var date=Date.fromLocaleDateString(Qt.locale(),dateText,dateFormat)

                        var cursorPosition=dateField.cursorPosition
                        console.log("dateFormatList "+dateFormatList +"cursor Position "+cursorPosition)
                        console.log("date "+date)
                        var firstSeparator=dateText.indexOf("/")
                        var lastSeparator=dateText.lastIndexOf("/")
                        if(cursorPosition<=firstSeparator || cursorPosition===dateText.length){
                            if(dateFormatList[0].includes("d")) date.setDate(date.getDate()+1)
                            else if(dateFormatList[0].includes("M")) date.setMonth(date.getMonth()+1)
                            else if(dateFormatList[0].includes("y")) date.setFullYear(date.getFullYear()+1)

                        }
                        else if(cursorPosition>firstSeparator && cursorPosition<=lastSeparator){
                            if(dateFormatList[1].includes("d")) date.setDate(date.getDate()+1)
                            else if(dateFormatList[1].includes("M")) date.setMonth(date.getMonth()+1)
                            else if(dateFormatList[1].includes("y")) date.setFullYear(date.getFullYear()+1)

                        }
                        else if(cursorPosition>=lastSeparator){
                            if(dateFormatList[2].includes("d")) date.setDate(date.getDate()+1)
                            else if(dateFormatList[2].includes("M"))  date.setMonth(date.getMonth()+1)
                            else if(dateFormatList[2].includes("y")) date.setFullYear(date.getFullYear()+1)
                        }

                        console.log("date minus"+date)
                        dateField.text=Qt.formatDate(date,dateFormat)
                        dateField.cursorPosition=cursorPosition

                    }
                }
            }
            ToolButton{
                id:secondaryCalendarButton
                visible: dateOperator.visible && dateOperator.currentText==="÷"
                Layout.alignment: Qt.AlignRight
                icon.name: "view-calendar"

                enabled: dateOperator.visible ? dateOperator.currentText!=="Ø" : sampleEnabled
                onClicked: {
                    var dateFormat=root.localDateFormat()
                    var dateCalendar=Date.fromLocaleDateString(Qt.locale(),secondaryDateField.text,dateFormat)
                    console.log("dateCalendar "+dateCalendar)

                    var dateText=Qt.formatDate(dateCalendar)
                    secondaryCalendarSheet.selectedDate=dateText.length ? dateCalendar : new Date()
                    secondaryCalendarSheet.open()
                }
                CalendarSheet{
                    id: secondaryCalendarSheet
                    onVisibleChanged: {
                        if(!visible) {
                            if(selectedDate != null){
                                var dateFormat=root.localDateFormat()
                                secondaryDateField.text=selectedDate.toLocaleString(Qt.locale(),dateFormat)
                            }
                        }
                    }
                }

            }



            Kirigami.ActionTextField{
                id:secondaryDateField
                leftActions:  Kirigami.Action {
                    id:secondaryDownButton
                    visible: dateOperator.visible && dateOperator.currentText==="÷"
                    icon.name: "arrow-down"
                    onTriggered: {
                        var dateText=secondaryDateField.text
                        var dateFormat=window.localDateFormat()
                        var dateFormatList=dateFormat.split("/")
                        var date=Date.fromLocaleDateString(Qt.locale(),dateText,dateFormat)

                        var cursorPosition=secondaryDateField.cursorPosition
                        var firstSeparator=dateText.indexOf("/")
                        var lastSeparator=dateText.lastIndexOf("/")
                        if(cursorPosition<=firstSeparator || cursorPosition===dateText.length){
                            if(dateFormatList[0].includes("d")) date.setDate(date.getDate()-1)
                            else if(dateFormatList[0].includes("M")) date.setMonth(date.getMonth()-1)
                            else if(dateFormatList[0].includes("y")) date.setFullYear(date.getFullYear()-1)

                        }
                        else if(cursorPosition>firstSeparator && cursorPosition<=lastSeparator){
                            if(dateFormatList[1].includes("d")) date.setDate(date.getDate()-1)
                            else if(dateFormatList[1].includes("M")) date.setMonth(date.getMonth()-1)
                            else if(dateFormatList[1].includes("y")) date.setFullYear(date.getFullYear()-1)

                        }
                        else if(cursorPosition>=lastSeparator){
                            if(dateFormatList[2].includes("d")) date.setDate(date.getDate()-1)
                            else if(dateFormatList[2].includes("M")) date.setMonth(date.getMonth()-1)
                            else if(dateFormatList[2].includes("y")) date.setFullYear(date.getFullYear()-1)
                        }

                        console.log("date minus"+date)
                        secondaryDateField.text=Qt.formatDate(date,dateFormat)
                        secondaryDateField.cursorPosition=cursorPosition

                    }

                }
                //                non si crea ricorsione in un loop infinito in quanto alla corrispondenza di un valueChanged si verifica se il valore è differente dal precedente
                //                solo in questo caso si procede a riaggiornare la proprietà
                //                text:dateField.secondaryText
                //                (non è necessario imporlo perchè al momento è di sola lettura)
                visible: dateOperator.visible && dateOperator.currentText==="÷"

                Layout.fillWidth: true
                Layout.maximumWidth:  font.pointSize<=10 ? secondaryDateField.implicitWidth/(3/2) : secondaryDateField.implicitWidth

                horizontalAlignment: TextInput.AlignHCenter
                Layout.alignment: Qt.AlignRight
                focus:true
                inputMethodHints: Qt.ImhPreferNumbers

                rightActions: Kirigami.Action {
                    id:secondaryUpButton
                    icon.name: "arrow-up"
                    visible: dateOperator.visible && dateOperator.currentText==="÷"
                    onTriggered: {

                        var dateText=secondaryDateField.text
                        var dateFormat=window.localDateFormat()
                        var dateFormatList=dateFormat.split("/")
                        var date=Date.fromLocaleDateString(Qt.locale(),dateText,dateFormat)

                        var cursorPosition=secondaryDateField.cursorPosition

                        var firstSeparator=dateText.indexOf("/")
                        var lastSeparator=dateText.lastIndexOf("/")
                        if(cursorPosition<=firstSeparator || cursorPosition===dateText.length){
                            if(dateFormatList[0].includes("d")) date.setDate(date.getDate()+1)
                            else if(dateFormatList[0].includes("M")) date.setMonth(date.getMonth()+1)
                            else if(dateFormatList[0].includes("y")) date.setFullYear(date.getFullYear()+1)

                        }
                        else if(cursorPosition>firstSeparator && cursorPosition<=lastSeparator){
                            if(dateFormatList[1].includes("d")) date.setDate(date.getDate()+1)
                            else if(dateFormatList[1].includes("M")) date.setMonth(date.getMonth()+1)
                            else if(dateFormatList[1].includes("y")) date.setFullYear(date.getFullYear()+1)

                        }
                        else if(cursorPosition>=lastSeparator){
                            if(dateFormatList[2].includes("d")) date.setDate(date.getDate()+1)
                            else if(dateFormatList[2].includes("M"))  date.setMonth(date.getMonth()+1)
                            else if(dateFormatList[2].includes("y")) date.setFullYear(date.getFullYear()+1)
                        }

                        console.log("date minus"+date)
                        secondaryDateField.text=Qt.formatDate(date,dateFormat)
                        secondaryDateField.cursorPosition=cursorPosition

                    }

                }
            }
            ToolButton{
                id:cleanButton
                Layout.alignment: Qt.AlignRight
                icon.name: "edit-clear"
                flat: true
                onClicked: {
                    dateField.clear()
                    secondaryDateField.clear()
                }
                Layout.rightMargin: Kirigami.Units.smallSpacing
                enabled: dateOperator.visible ? dateOperator.currentText!=="Ø" : sampleEnabled
            }
        }

    }

    Component{
        id:integerFormComponent
        RowLayout{

            spacing: Kirigami.Units.smallSpacing
            Kirigami.Heading{
                Layout.leftMargin: Kirigami.Units.smallSpacing
                text: labelName
                level:3
            }

            ComboBox{
                id:integerOperator
                model: ["=", "≤","≥","÷","Ø"]

                Layout.preferredWidth:(parent.width)*(1/10)
                Layout.minimumWidth: 60
                visible: pageRoot.state==="search"
                Connections{
                    target: pageRoot
                    function onResetSamples(){
                        integerOperator.currentIndex=0
                    }
                }


            }


            Item{
                //                Item che ha ruolo di spacer perchè si espande alla massima disponibilità di larghezza
                id:spacer
                Layout.fillWidth: true
                Layout.preferredWidth: 0
            }

            SpinBox {
                id: spinBox
                //tecnica per rendere possibile in ogni caso la ricerca anche se si tratta di primary key

                enabled: integerOperator.visible ? integerOperator.currentText!=="Ø" : sampleEnabled
                property bool altered : false
                property double secondaryValue: secondarySpinBox.value
                Kirigami.FormData.label: labelName
                onValueChanged: altered=true
                Layout.alignment: Qt.AlignRight
                Layout.fillWidth: true
                Layout.maximumWidth: spinBox.implicitWidth

                editable: true

                from:-2000000000
                to:2000000000
                property string operator: integerOperator.currentText


                validator: IntValidator {
                    bottom: Math.min(spinBox.from, spinBox.to)
                    top:  Math.max(spinBox.from, spinBox.to)
                }

                focus:true
                Component.onCompleted: {
                    root.mapper.addMapping(spinBox,section,root.mapper.typeInt)
                    console.log("add spinbox on section "+section+" name "+labelName+" type "+root.mapper.Int)
                    itemCreated(section)
                    console.log("upindicator "+spinBox.up.indicator)
                }
            }
            SpinBox {
                id: secondarySpinBox
                visible: integerOperator.visible && integerOperator.currentText==="÷"

                Layout.fillWidth: true
                Layout.maximumWidth: secondarySpinBox.implicitWidth
                Layout.alignment: Qt.AlignRight


                editable: true

                from:-2000000000
                to:2000000000

                validator: IntValidator {
                    bottom: Math.min(secondarySpinBox.from, secondarySpinBox.to)
                    top:  Math.max(secondarySpinBox.from, secondarySpinBox.to)
                }


                focus:true



            }

            ToolButton{
                id:cleanButton
                icon.name: "edit-clear"
                flat: true
                enabled: integerOperator.visible ? integerOperator.currentText!=="Ø" : sampleEnabled
                onClicked: {
                    spinBox.value=0
                    secondarySpinBox.value=0
                }
                Layout.rightMargin: Kirigami.Units.smallSpacing
            }


        }
    }
    
    Component{
        id:numberFormComponent
        RowLayout{
            spacing: Kirigami.Units.smallSpacing
            Kirigami.Heading{
                Layout.leftMargin: Kirigami.Units.smallSpacing
                text: labelName
                level:3
            }

            ComboBox{
                id:numberOperator
                model: ["=", "≤","≥","÷","Ø"]


                //allineamento fatto tramite anchors invece di Layout
                //in quanto altrimenti nn si riesce ad affiancare al campo successivo
                //                anchors.left: numberLabel.right
                //                anchors.leftMargin: 2
                Layout.preferredWidth:(parent.width)*(1/10)
                Layout.minimumWidth: 60
                visible: pageRoot.state==="search"
                Connections{
                    target: pageRoot
                    function onResetSamples(){
                        numberOperator.currentIndex=0
                    }
                }


            }

            Item{
                //                Item che ha ruolo di spacer perchè si espande alla massima disponibilità di larghezza
                id:spacer
                Layout.fillWidth: true
                Layout.preferredWidth: 0
            }

            SpinBox {
                id: doubleSpinBox
                //tecnica per rendere possibile in ogni caso la ricerca anche se si tratta di primary key
                //                enabled: numberOperator.visible | sampleEnabled

                enabled: numberOperator.visible ? numberOperator.currentText!=="Ø" : sampleEnabled
                property bool altered : false
                property double secondaryInputValue: secondaryDoubleSpinBox.inputValue
                Kirigami.FormData.label: labelName
                onInputValueChanged: {
                    console.log("SamplePage: input value changed "+inputValue)
                    value= Math.round(inputValue*100)
                    altered=true
                }

                Layout.alignment: Qt.AlignRight
                Layout.fillWidth: true
                Layout.maximumWidth: doubleSpinBox.implicitWidth

                from: -100 * 1000000
                to: 100 * 1000000
                value: Math.round(inputValue*100)

                //                function returnValue() {
                //                    console.log("input value giunto "+ inputValue+ " *100 "+inputValue*100+"*100 round"+Math.round(inputValue*100))

                //                    return inputValue*100
                //                }
                editable: true
                onValueChanged: {
                    console.log("value "+value)
                    console.log("realValue "+realValue)
                    console.log("inputValue "+inputValue)
                    //                    if(realValue!==value/100) realValue=value/100
                }
                stepSize: 100
                property int decimals: 2
                property double realValue: value / 100

                property string operator: numberOperator.currentText
                //importante che inputValue segua realValue altrimenti non va bene in fase iniziale di reset
                //il valore non rivelerebbe i cambiamenti che accadrebbero con inizializzazione
                //property real inputValue: 0
                //Esempio: un widget è stato assegnato a una precedente record in un campo = 3
                //
                //in questo caso di assegnazione (pre modifica di value -> e quindi realValue manualmente) avviene un submit i dati vengono passati al modello ma inputValue nn viene modificato e resta all'inizializzazione 0
                // successivamente alla crazione di un nuovo record con un setInput a 0 non viene rilevato il changed
                //che serve a riportare il valore da 3 a 0, value resta immutato, pertanto ci ritroviamo con il 3 nel Widget
                //soluzione che funziona in qualche modo: imporre che inputValue segua realValue = value/100 (pari quindi a quel precedentemente digitato) in modo che il submit si ripercutota anche sul valore
                //attuale di inputValue

                property double inputValue: realValue


                validator: DoubleValidator {
                    bottom: Math.min(doubleSpinBox.from, doubleSpinBox.to)
                    top:  Math.max(doubleSpinBox.from, doubleSpinBox.to)
                }

                textFromValue: function(value, locale) {
                    var numberText =Number(value / 100).toLocaleString()
                    console.log("SamplePage: textFromValue value"+value+" numbertext "+numberText)
                    return numberText
                }

                valueFromText: function(text, locale) {
                    var value=Math.round(Number.fromLocaleString(locale, text) * 100)
                    console.log("SamplePage: valueFromText text "+text+" value "+value)
                    return value
                    //                    return Math.round(Number.fromLocaleString(locale, text) * 100)
                }
                focus:true
                inputMethodHints: Qt.ImhFormattedNumbersOnly
                Component.onCompleted: {
                    root.mapper.addMapping(doubleSpinBox,section,root.mapper.typeNumber)
                    console.log("add spinbox on section "+section+" type "+root.mapper.Number)
                    itemCreated(section)
                }

            }
            SpinBox {
                id: secondaryDoubleSpinBox
                visible: numberOperator.visible && numberOperator.currentText==="÷"
                Layout.fillWidth: true
                Layout.maximumWidth: secondaryDoubleSpinBox.implicitWidth
                Layout.alignment: Qt.AlignRight

                from: -100 * 1000000
                to: 100 * 1000000
                editable: true
                stepSize: 100
                property int decimals: 2
                property double realValue: value / 100


                property double inputValue: realValue
                value:  Math.round(inputValue*100)
                onValueChanged: {
                    console.log("value ",realValue)
                    console.log("secondaryinputValue "+inputValue)
                }

                validator: DoubleValidator {
                    bottom: Math.min(secondaryDoubleSpinBox.from, secondaryDoubleSpinBox.to)
                    top:  Math.max(secondaryDoubleSpinBox.from, secondaryDoubleSpinBox.to)
                }

                textFromValue: function(value, locale) {
                    var numberText =Number(value / 100).toLocaleString()
                    return numberText
                }

                valueFromText: function(text, locale) {
                    var value=Math.round(Number.fromLocaleString(locale, text) * 100)
                    return value
                }
                focus:true


            }

            ToolButton{
                id:cleanButton
                icon.name: "edit-clear"
                flat: true
                enabled: numberOperator.visible ? numberOperator.currentText!=="Ø" : sampleEnabled
                onClicked: {
                    //importante resettare value e non inputValue altrimenti quest'ultimo resta fissato a 0 nonostante le modifiche.
                    //questo determinerebbe problemi al mapper che non sarebbe in grado più di modificare i valori
                    //Con questo accrocchio si forza la proprietà ad azzerarsi senza fissarla in maniera definitiva.
                    doubleSpinBox.from=0
                    doubleSpinBox.to=0
                    doubleSpinBox.from= -100 * 1000000
                    doubleSpinBox.to= 100 * 1000000
                    secondaryDoubleSpinBox.from=0
                    secondaryDoubleSpinBox.to=0
                    secondaryDoubleSpinBox.from= -100 * 1000000
                    secondaryDoubleSpinBox.to= 100 * 1000000


                }
                Layout.rightMargin: Kirigami.Units.smallSpacing

            }


        }
    }

    Component{
        id:boolFormComponent

        RowLayout{
            spacing: Kirigami.Units.smallSpacing
            Kirigami.Heading{
                Layout.leftMargin: Kirigami.Units.smallSpacing
                text: labelName
                level:3
            }
            ComboBox{
                id:boolOperator
                model: ["=","Ø"]
                //                flat:true
                Layout.preferredWidth:(parent.width)*(1/10)
                //                anchors.left: boolLabel.right
                //                anchors.leftMargin: 2
                Layout.minimumWidth: 60
                visible: pageRoot.state==="search"
                Connections{
                    target: pageRoot
                    function onResetSamples(){
                        boolOperator.currentIndex=0
                    }
                }

            }
            Item{
                //                Item che ha ruolo di spacer perchè si espande alla massima disponibilità di larghezza
                id:spacer
                Layout.fillWidth: true
            }
            CheckBox{
                id:checkBox
                property string operator: boolOperator.currentText
                property bool altered : false
                Kirigami.FormData.label: labelName
                enabled: boolOperator.visible ? boolOperator.currentText!=="Ø" : sampleEnabled
                //rileva modifiche, l'azzeramento avviene quando il mapper passa ad un nuovo index.
                onCheckedChanged: altered=true
                focus:true
                Layout.alignment: Qt.AlignRight
                Component.onCompleted: {
                    root.mapper.addMapping(checkBox,section,root.mapper.typeBool)
                    itemCreated(section)
                }
            }
            ToolButton{
                id:cleanButton
                icon.name: "edit-clear"
                flat: true
                //                display: AbstractButton.TextOnly
                onClicked: checkBox.checked=false
                enabled: boolOperator.visible ? boolOperator.currentText!=="Ø" : sampleEnabled
                Layout.rightMargin: Kirigami.Units.smallSpacing
            }

        }
    }

    Component{
        id:enumFormComponent

        RowLayout{
            spacing: Kirigami.Units.smallSpacing
            Kirigami.Heading{
                Layout.leftMargin: Kirigami.Units.smallSpacing
                text: labelName
                level:3
            }

            ComboBox{
                id:listOperator
                model: ["=","Ø"]
                //                flat:true
                Layout.preferredWidth:(parent.width)*(1/10)
                Layout.minimumWidth: 60
                visible: pageRoot.state==="search"
                Connections{
                    target: pageRoot
                    function onResetSamples(){
                        listOperator.currentIndex=0
                    }
                }

            }
            ComboBox{
                id:comboBox
                property string operator: listOperator.currentText
                property bool altered : false
                property string indexRole: relationIndex
                Kirigami.FormData.label: labelName
                //                enabled: sampleEnabled
                enabled: listOperator.visible ? listOperator.currentText!=="Ø" : sampleEnabled
                onCurrentIndexChanged: altered=true
                focus:true
                //                editable: true
                focusPolicy: Qt.WheelFocus

                /*****                Layout.alignment: Qt.AlignRight
  */
                Layout.fillWidth: true

                /****                model:listEnum.split(",")
    */
                model:instantiatorRelations.objectItem(relationTable)

                //              In the text role is fixed the relation display field
                textRole:relationDisplay
                currentIndex: -1
                Component.onCompleted: {
                    console.log("combo box completed "+labelName+" model "+instantiatorRelations.objectItem(relationTable)+" type "+root.mapper.Script)
                    console.log(" popup "+comboBox.indicator)
                    if(!model.rowCount()){
                        model.loadContents()
                    }

                    root.mapper.addMapping(comboBox,section,root.mapper.typeEnum)
                    itemCreated(section)


                }

                onDisplayTextChanged:  console.log("combo display text"+displayText)
                onCurrentTextChanged:  console.log("combo Current text"+currentText)




                onFocusChanged: console.log("focus in")
                onHoveredChanged: console.log("hovered")


            }
            ToolButton{
                id:cleanButton
                flat: true
                icon.name: "edit-clear"
                //                display: AbstractButton.TextOnly
                onClicked: comboBox.currentIndex=-1
                enabled: listOperator.visible ? listOperator.currentText!=="Ø" : sampleEnabled
                Layout.rightMargin: Kirigami.Units.smallSpacing
            }

        }
    }

    Component{
        id:listFormComponent

        RowLayout{
            spacing: Kirigami.Units.smallSpacing
            Kirigami.Heading{
                Layout.leftMargin: Kirigami.Units.smallSpacing
                text: labelName
                level:3
            }

            ComboBox{
                id:listOperator
                model: ["=","Ø"]
                //                flat:true
                Layout.preferredWidth:(parent.width)*(1/10)
                Layout.minimumWidth: 60
                visible: pageRoot.state==="search"
                Connections{
                    target: pageRoot
                    function onResetSamples(){
                        listOperator.currentIndex=0
                    }
                }

            }
            ToolButton{
                id:detailsButton
                icon.name: "view-list-details"
                autoRepeat: true
                onClicked: {
                    comboBox.popup.visible=false
                    var text=comboBox.editText
                    comboBox.model=null
                    detailsSheet.open()
                    var relModel=instantiatorRelations.objectItem(comboBox.tableName)
//                    valutare se necessaria pulizia modello prima di mostrare nel detailsSheet
                    //                    relModel.clear()
                    detailsSheet.setModel(relModel,comboBox.tableName)

                    console.log("comboBox editText "+text)
                    relModel.setContentQuery(comboBox.textRole+"=ilike.*"+text+"*")
//                    model=instantiatorRelations.objectItem(tableName)
//                    relModel.setContentQuery("")
                    relModel.loadContents()
                    comboBox.model=null
                }
                DetailsSheet{
                    id:detailsSheet
//                    model: instantiatorRelations.objectItem(comboBox.tableName)
                    textRole: comboBox.textRole
                    tableName: comboBox.tableName

                    onVisibleChanged: {
                        if(!visible){
                            console.log("selectedText "+selectedText)
                            detailsSheet.model=null
                            if(selectedText.length){
                                comboBox.model=instantiatorRelations.objectItem(comboBox.tableName)

                                //                        comboBox.focus=true
                                comboBox.loadData(selectedText)
                                comboBox.forceActiveFocus()
                            }
                            else{

                            }
                        }
                    }

                    onSortUpChanged: {
                        if(sortUp){
                            model.orderRole=comboBox.textRole
                            model.sortItems(0,Qt.AscendingOrder)
                        }
                        else{
                            if(!sortDown){
                                model.orderRole=comboBox.indexRole
                                model.sortItems(0,Qt.AscendingOrder)
                            }
                        }
                    }
                    onSortDownChanged: {
                        if(sortDown){
                            model.orderRole=comboBox.textRole
                            model.sortItems(0,Qt.DescendingOrder)
                        }
                        else{
                            if(!sortUp){
                                model.orderRole=comboBox.indexRole
                                model.sortItems(0,Qt.AscendingOrder)
                            }
                        }
                    }

                }
            }



            ComboBox{
                id:comboBox

                property string operator: listOperator.currentText
                property bool altered : false
                property string indexRole: relationIndex
                property string tableName: relationTable
                property var relationIndexValue: null
                property string currentDisplayText: ""
                property bool recoverText : false
                //                property var modelObject: instantiatorRelations.objectItem(tableName)
                //                modelObject.onLoadData: console.log("SamplePage: load completed")

                Kirigami.FormData.label: labelName
                //                enabled: sampleEnabled
                enabled: listOperator.visible ? listOperator.currentText!=="Ø" : sampleEnabled
                onCurrentIndexChanged: {
                    if(model){
                        console.log("Current index "+currentIndex)
                        var indexRelationIndex=model.index(currentIndex,0)

                        console.log("relationDisplay "+relationDisplay+" indexRole "+indexRole+"data"+model.dataRole(indexRelationIndex,0))

                        altered=true
                    }
                }
                onEditTextChanged: console.log("editText changed")
                focus:true
                editable: true
                focusPolicy: Qt.WheelFocus
                //                modelObject.onLoadCompleted: console.log("SamplePage: load completed")
                /*****                Layout.alignment: Qt.AlignRight
  */
                Layout.fillWidth: true

                /****                model:listEnum.split(",")
    */
                //From instantiator for relation get the whole object InterfaceData
//                model:instantiatorRelations.objectItem(tableName)

                //                 currentText:
                textRole:relationDisplay
                currentIndex: -1
                Component.onCompleted: {
                    console.log("add combo box on section "+section+" name "+labelName+" item "+comboBox+" model "+instantiatorRelations.objectItem(relationTable)+" type "+root.mapper.Script)
                    console.log(" popup "+comboBox.indicator)
                    root.mapper.addMapping(comboBox,section,root.mapper.typeList)
                    itemCreated(section)



                }
                onDisplayTextChanged:  console.log("combo display text"+displayText)
                onCurrentTextChanged:  console.log("combo Current text"+currentText)
                popup.height: popup.contentHeight<=pageRoot.availableHeight ? popup.contentHeight : pageRoot.availableHeight
                popup.onAboutToShow: {
                    console.log("visible changed "+visible+" editText "+editText+" activeFocus "+popup.activeFocus+" popup focus"+focus)
                    if(visible){
                        var text=editText
                        model=instantiatorRelations.objectItem(tableName)
                        loadData(text)
                    }
                }
                popup.onAboutToHide: {
                    console.log("popup hide "+editText+" currentIndex "+currentIndex+" currentDisplayText "+currentDisplayText+"currentText"+currentText)
                    //                    var textIndex=comboBox.find(comboBox.currentDisplayText)
                    //                    console.log("textIndex "+textIndex)
                    //                    if(textIndex!==-1){
                    //                        comboBox.currentIndex=textIndex

                    //                    }
                    //                    if(detailsPopup.selectedText.length) {
                    //                        comboBox.currentIndex=0
                    //                        detailsPopup.selectedText=""
                    //                    }
//                    model has to be emptyied when no item is selected
                    if(currentIndex==-1){
                        model=null
                    }
                }

                function loadData(dataText){
                    if(dataText.length>0){
                        console.log("combo Edit text: "+dataText)

                        currentDisplayText=dataText
                        model.abortContent()
                        //putting currentIndex=-1 becouse setting model realize currentIndex to 0 shown but is preferred empty text on comboBox
                        comboBox.currentIndex=-1
                        model.setContentQuery(relationDisplay+"=ilike.*"+dataText+"*")

                        model.loadContents()

                    }
                    else{
                        console.log("model "+model+" comboBox.model "+comboBox.model)
                        model.abortContent()
                    }

                }

                Connections{
                    target:instantiatorRelations.objectItem(comboBox.tableName)
                    function onLoadCompleted() {
                        console.log("completed "+comboBox.currentDisplayText)
                        comboBox.recoverText=true
                        if(!detailsSheet.sheetOpen) {
                            comboBox.popup.open()
                        }
                        detailsSheet.addMode=false
                        comboBox.recoverText=false
                    }
                    function onErrorHappened(errorText) {
                        if(detailsSheet.sheetOpen){
                            detailsSheet.setError(errorText)
                        }
                    }
                }



                // Event return press on comboBox, set visible popup if text is not empty in other case clean model
                onAccepted:{
                    console.log("accepted editText "+editText)
                    if(editText.length){
                        popup.visible=true
                    }
                    else{
                        currentDisplayText=""
                        //                        model.abortContent()
                        //                        model.clean()
                    }
                }

                onFocusChanged: console.log("focus "+focus)
                onActiveFocusChanged: {
                    console.log("activeFocus "+activeFocus)
                    //                    if(!activeFocus && ){
                    //                        if(editText.length==0){
                    //                            currentDisplayText=""
                    //                            model.abortContent()
                    //                            model.clean()
                    //                        }
                    //                    }

                }
                onHoveredChanged: console.log("hovered")


            }

            ToolButton{
                id:cleanButton
                icon.name: "edit-clear"
                flat: true
                //                display: AbstractButton.TextOnly
                onClicked: {
                    if(comboBox.model!==undefined && comboBox.model!==null){
                        comboBox.model.abortContent()
                        comboBox.model.clean()
                        comboBox.currentIndex=-1
                    }
                    else{
                        comboBox.editText=null
                    }
                }
                enabled: listOperator.visible ? listOperator.currentText!=="Ø" : sampleEnabled
                Layout.rightMargin: Kirigami.Units.smallSpacing
            }

        }
    }
    //per fare la barra di separazione tra i campi
    Component{
        id:delegateSection
        Rectangle{
            width: pageRoot.contentItem.width
            height: Kirigami.Units.smallSpacing
            Kirigami.Separator{
                Layout.fillWidth: true
                width: pageRoot.contentItem.width
                //                height: 50
                color: Kirigami.Theme.highlightColor
            }
        }

        //        Rectangle{
        //            height: 5
        //            width: parent.width
        //            Rectangle{
        //                color: "gray"
        //                anchors.verticalCenter: parent.verticalCenter
        //                height: 1
        //                width: parent.width
        //                Component.onCompleted: {
        //                }
        //            }
        //        }
    }
    
    
    function formComponent(typeItem,formatItem,lengthItem,enumItem,enabledItem,relationTableItem){
        console.log("form component "+ typeItem+" lenghtItem "+lengthItem+" enumitem "+enumItem+" relationTableItem "+relationTableItem)
        //        The property in this function are visible even in the component that are stantiated
        if(relationTableItem.length) return listFormComponent
        switch (typeItem) {
        case "string":  return elaborateTextComponent(formatItem,lengthItem)
        case "integer" : return integerFormComponent
        case "number" : return numberFormComponent
        case "bool" : return boolFormComponent
        case "boolean" : return boolFormComponent
        }

    }
    function elaborateTextComponent(format,length){
        if(format==="character varying"){
            if(length<=200)  {
                return textFormComponent
            }
            else{
                return scriptFormComponent
            }
        }
        else {
            if(format==="date") return dateFormComponent
        }



    }
    function setIndex(index){
        root.mapper.setCurrentIndex(index)
    }
    function searchObject(){
        var tableModel=root.mapper.model
        var itemsValueMap=root.mapper.itemsValueMap()
        var secondaryItemsValueMap=root.mapper.secondaryItemsValueMap()
        var itemsOperatorMap=root.mapper.itemsOperatorMap()
        tableModel.numScreenItems=5
        tableModel.searchItems(itemsValueMap,secondaryItemsValueMap,itemsOperatorMap)
    }
    function undoObject(){
        var tableModel=root.mapper.model
        tableModel.revert()
        root.mapper.revert()
    }
    function submitObject(){
        var submitOk=root.mapper.submit()
        console.log("mapper submitOk "+submitOk)
        var tableModel=root.mapper.model
        //verifica necessaria in quanto qualora non sia cambiato proprio nulla nel modello non vi è modifica da trasmettere
        if(submitOk) tableModel.submitRow(root.mapper.currentIndex)
    }
    function setModel(model,name){
        if(model){
            root.mapper.clear()
            root.mapper.model=model
            //loaded because the object has to be instantiated and for samplePage there isn't trigger
            tableName=name
            sampleModel.clear()

            var itemsRoleList=model.itemsRoleList();
            //si preddisponngono i widget del mapper della pagina dei sample perchè possano essere riempiti in fase
            //di selezione nella seconda pagina

            for(var i in itemsRoleList) {
                var role=itemsRoleList[i]
                //            console.log(role)
                var itemName=model.itemName(role)
                var itemType=model.itemType(role)
                var itemFormat=model.itemFormat(role)
                var itemLength=model.itemLength(role)
                var itemEnum=model.itemEnum(role).toString()
                var itemEnabled=model.itemEnabled(role)
                var itemRelationTable=model.itemRelationTable(role)
                var itemRelationDisplay=model.itemRelationDisplay(role)
                var itemRelationIndex=model.itemRelationIndex(role)
                console.log("TablePage: pageSampleModel append - Name "+itemName+" itemType "+itemType+"itemFormat"+itemFormat+" type "+typeof(itemEnum)+" itemRelationTable "+itemRelationTable+" itemRelationDisplay "+itemRelationDisplay+" itemRelationIndex "+itemRelationIndex)
//                var samplePage=root.pagePool.pageForUrl("pages/SamplePage.qml")
                sampleModel.append({"itemName":itemName, "itemType":itemType,"itemFormat":itemFormat,"itemLength":itemLength,"itemEnum":itemEnum,"itemEnabled":itemEnabled,"itemRelationTable":itemRelationTable,"itemRelationDisplay":itemRelationDisplay,"itemRelationIndex":itemRelationIndex,"itemRole":role})
                tableNameList.push(itemName)
            }



        }

    }
}







