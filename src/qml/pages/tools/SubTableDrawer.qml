/*
Kuickgest: Resource managment system interfaced through REST API.

Copyright (c) 2022 Marcello Di Guglielmo

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

import QtQuick 2.10
import org.kde.kirigami 2.19 as Kirigami
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.14
// import Qt.labs.calendar 1.0
Kirigami.OverlayDrawer {
    id: listViewDrawer
    edge: Qt.BottomEdge
    property var foreignKeys: []
    property var referenceIndexes: []
    property var constraints: []
//    onClosed: tabBar.setCurrentIndex(1)
    // Set modal to false to make this drawer inline!
    modal: true
//    property alias tabBar1: tabBar

    contentItem: ColumnLayout{

        TabBar {
            id: tabBar
            property alias model: repeater.model
            //            currentIndex: swipeView.currentIndex

            onCurrentIndexChanged: {
                //
                console.log("TabBar currentIndex changed to "+currentIndex)
                var samplePage=root.pagePool.loadPage("pages/SamplePage.qml")
                if(tabBar.model){
                    mainListView.model=instantiatorSubTables.objectItem(tabBar.model[tabBar.currentIndex])
//                    samplePage.setModel(instantiatorSubTables.objectItem(tabBar.model[currentIndex]))
                }
                else{
                    mainListView.model=null
                }
            }
            Repeater{
                id: repeater
                TabButton {
                    id: tabButton
                    text: modelData
                    Connections{
                        target: listViewDrawer
                        function onClosed(){

                        }
                    }


                }
            }
//            TabButton {
//                id:tabButton1
//                text: "Third"
////                autoExclusive: false
//                Connections{
//                    target: listViewDrawer
//                    function onClosed(){
//                        tabButton1.checked=false
//                    }
//                }

//            }
//            TabButton {
//                id:tabButton2
//                text: "Third"
////                autoExclusive: false
//                Connections{
//                    target: listViewDrawer
//                    function onClosed(){
//                        tabButton2.checked=false
//                    }
//                }

//            }
        }
//        ListView{
//            Layout.fillWidth: true
//            height: root.height/4
//        }


        Component {
            id: delegateComponent
            Kirigami.SwipeListItem {
                id: listItem
                width: ListView.view ? ListView.view.width : implicitWidth
                height: root.wideScreen ? Kirigami.SwipeListItem.implicitHeight : Kirigami.Units.iconSizes.smallMedium*(5/2)
                highlighted: mainListView.currentItem==listItem
                onClicked: mainListView.currentIndex=index
                onPressed: console.log("TablePage: SwipeListItem pressed")
                Keys.onReturnPressed: loadSubSample()
                contentItem: ListView{
                    id:rowView
                    interactive: actionSwipe.checked
                    MouseArea{
                        id: rowMouseArea
                        anchors.fill: parent
                        onClicked: {
                            console.log("TablePage: MouseArea cliched")
                            //The reason use of Mouse area is that rowView is upper the Swipe listItem and mask inputs.
                            mainListView.currentIndex=index


                        }
                        onDoubleClicked: loadSubSample()

                    }


                    header: ColumnLayout{
                        spacing: 0
                        Kirigami.Icon {
                            id:itemIcon
                            source: "item"
                            Layout.fillHeight: true
                            Layout.maximumHeight: Kirigami.Units.iconSizes.smallMedium
                            Layout.preferredWidth: height

                        }
                        Label {
                            id: labelIndex
    //                        text: mainListView.count-model.index
                            text: model.index
                            fontSizeMode: Text.Fit
                            horizontalAlignment: Text.AlignHCenter
                            Layout.maximumHeight:Kirigami.Units.iconSizes.small
                            //important to specify minimumPointSize
                            Layout.preferredWidth: itemIcon.width
                            minimumPointSize: 3
                            color: listItem.checked || rowMouseArea.pressed || (listItem.pressed && !listItem.checked && !listItem.sectionDelegate) ? listItem.activeTextColor : listItem.textColor

                        }

                    }
                    orientation: ListView.Horizontal
                    model:delegateModel
                    spacing: 20
                    clip:true
                    ListModel{
                        id:delegateModel
                    }
                    delegate: ColumnLayout {
                        spacing: 0
                        Kirigami.Heading {
                            level: 5
                            type: Kirigami.Heading.Type.Secondary
                            text: LabelElement
                            font.bold: true
                        }
                        Label {
                            Layout.fillWidth: true
                            Layout.maximumWidth: Kirigami.Units.largeSpacing*40
                            text: TextElement
                            elide: Text.ElideRight
                            wrapMode: Text.WordWrap
                        }
                    }
                }

                actions: [
                    Kirigami.Action {
                        icon.name: "delete"
                        text: i18n("Remove")
                        onTriggered: confirmationRemoveDialog.open()
                    },
                    Kirigami.Action {
                        icon.name: "document-edit"
                        text: i18n("Edit")
                        onTriggered: {
                            mainListView.currentIndex=index
                            loadSubSample()
                        }
                    }
                ]

                function updateRole(roleName,roleRelationTable,roleRelationDisplay,roleFormat){
                    var roleText=""
                    var roleRelationDisplayValue=""
                    var roleValue=model[roleName]
                    if(roleFormat==="date"){

                        var dateText=roleValue ? roleValue.toString() : ""
                        //                            var localeParameters=Qt.locale()
                        //                            var roleDate = Date.fromLocaleDateString(localeParameters,dateText,"yyyy-MM-dd")
                        var dateFormat=root.localDateFormat()
                        //                            roleText= Qt.formatDate(dateText,Qt.DefaultLocaleShortDate)
                        roleText= Qt.formatDate(dateText,dateFormat)
                        //                            roleText=roleDate.toLocaleDateString(localeParameters,Locale.ShortFormat)
                        //                            console.log("roleData "+dateText+" data "+roleText+"dateFormat"+"roleDate"+roleDate)

                    }
                    else if(roleFormat==="numeric"){
                        roleText=roleValue ? Number(roleValue).toLocaleString() : ""
                    }
                    else if(roleRelationTable.length){
                        if(roleValue){
                            roleRelationDisplayValue=roleValue[roleRelationDisplay]
    //                        Important use method toString() becouse displayValue could be a number or integer that could not be directly passed to TextElement
                            roleText=(roleRelationDisplayValue ? roleRelationDisplayValue.toString() : "")
                        }
                        else{
                            roleText=""
                        }
                    }

                    //+                        else if(roleRelationTable.length)
                    //                        {
                    //                            var relModel=instantiatorRelations.objectItem(roleRelationTable)
                    //                            if(relModel && roleValue){
                    //                                var relItemsValue=relModel.catchValueMap(roleValue)
                    //                                roleText=relItemsValue[roleRelationDisplay]
                    ////                                var indexText=roleValue ? roleValue.toString() : ""
                    ////                                console.log("index text "+indexText+" relText "+relText)
                    //                            }
                    //                        }
                    else{
                        roleText=(roleValue ? roleValue.toString() : "")
                    }

                    console.log("TablePage: updateRole - append roleName ",roleName," roleValue ",roleValue,"roleFormat",roleFormat,"roleRelationTable",roleRelationTable,"roleRelationDisplay",roleRelationDisplay,"roleRelationDisplayValue",roleRelationDisplayValue,"roleText",roleText)
                    rowView.model.append({"LabelElement":roleName,"TextElement" : roleText})
                }
                function clearRoles(){
                    rowView.model.clear()
                }

                Component.onCompleted: {
                    //deve essere formalmente predisposto l'update in quanto i delegate vengono caricati in fase
                    //di avviamento successivamente ai date changed e quindi al loro interno non vengono inseriti
                    //i valori.
                    var itemRoles=mainListView.model.itemsRoleList()
                    for(var i in itemRoles)
                    {
                        var role=itemRoles[i]
                        var roleName=mainListView.model.itemName(role)
                        //il model è quello generale del delegate di tutta la riga della pagina test
                        var roleRelationTable=mainListView.model.itemRelationTable(role)
                        var roleRelationDisplay=mainListView.model.itemRelationDisplay(role)
                        var roleFormat=mainListView.model.itemFormat(role)
                        console.log("updateDelegate role"+role+" roleName "+roleName)
                        updateRole(roleName,roleRelationTable,roleRelationDisplay,roleFormat)
                    }
                }
            }
        }

        ListView {
            id: mainListView
//            model: tabBar.model ? instantiatorSubTables.objectItem(tabBar.model[tabBar.currentIndex]) : null
            clip: true
            Layout.fillWidth: true
            height: root.height/4
            //        highlightFollowsCurrentItem seems not operate as planned behaviour, used manual comparison
            //        highlightFollowsCurrentItem: true
            onCurrentIndexChanged: {
                console.log("SubTableDrawer: currentIndex "+currentIndex)
                console.log("SubTableDrawer: currentItem "+currentItem)
                var samplePage=root.pagePool.pageForUrl("pages/SamplePage.qml")
                if(samplePage.model===mainListView.model){
                    samplePage.setIndex(mainListView.currentIndex)
                }
                var tablePage=root.pagePool.pageForUrl("pages/TablePage.qml")
                var mainPage=root.pagePool.pageForUrl("pages/MainPage.qml")
                samplePage.state=""
                mainPage.setTableIndex(tableName,currentIndex)
                // if(currentIndex==-1){
                //     mainPage.setTableIndex(tableName,"")
                // }
                // else{
                //     mainPage.setTableIndex(tableName,currentIndex)
                // }

                if(!root.wideScreen) {
                    root.pageStack.insertPage(1,samplePage)
                    //                root.pageStack.push(tablePage)
                }
            }
            onVerticalOvershootChanged: listViewVOvershoot(mainListView.verticalOvershoot)
            //        interactive: false
            Timer {
                id: refreshRequestTimer
                interval: 3000
                onTriggered: pageRoot.refreshing = false
            }
            moveDisplaced: Transition {
                YAnimator {
                    duration: Kirigami.Units.longDuration
                    easing.type: Easing.InOutQuad
                }
            }
            delegate: delegateComponent


            headerPositioning: ListView.OverlayHeader
        }


        Kirigami.ActionToolBar{
            id:sampleNavigationTabBar
            alignment: Qt.AlignHCenter
            actions: [
                Kirigami.Action{
                    id: sampleActionUndo
                    icon.name: "edit-undo"
                    text: i18nc("@undo","undo")
//                    shortcut: StandardKey.Undo
                    onTriggered: {
                        undoObject();
                    }
                },
                Kirigami.Action{
                    id: sampleActionSearch
                    icon.name: "search"
                    text: i18nc("@search","search")
                    visible:false
                    onTriggered: {
                        pageRoot.state=""
                        if(root.wideScreen) pageStack.pop()
                        else pageStack.flickBack();
                        searchObject();
                    }
                },
                Kirigami.Action{
                    id: sampleActionReset
                    icon.name: "dialog-cancel"
                    text: i18nc("@reset","reset")
//                    shortcut: StandardKey.Cancel
                    visible: false
                    onTriggered: {
                        resetSamples();
                        root.mapper.setCurrentIndex(-1);
                    }
                },
                Kirigami.Action{
                    id: sampleActionSave
                    icon.name: "document-save"
                    text: i18nc("@save","save")
//                    shortcut: StandardKey.Save
                    onTriggered: {
                        submitObject();
                    }
                },
                Kirigami.Action{
                    id: sampleActionClose
                    icon.name: "dialog-close"
                    text: i18nc("@close","close")
//                    shortcut: StandardKey.Close
                    onTriggered: {
                        close()
                    }
                }
            ]
        }

    }

    function setTables(tableList){
        tabBar.setCurrentIndex(-1)
        console.log("tabBar "+tabBar.children+" currentIndex "+tabBar.currentIndex)
        tabBar.model=tableList
    }

    function setConstraints(constraints){
        for(var i=0; i<tabBar.count; i++){
            var tabName=tabBar.itemAt(i).text
            var constraint=constraints[i]
            var tableModel=instantiatorSubTables.objectItem(tabName)
            console.log("SubTableDrawer constraint "+constraint+" tabName "+tabName)
            if(tableModel && constraint!==undefined){
                tableModel.contentQuery=constraint
                var orderSequence=tableModel.primaryKey()+".desc"
                tableModel.contentOrder=orderSequence
                tableModel.numScreenItems=0
                tableModel.goLast()
                tableModel.loadContents()
            }
        }
    }

    function loadSubSample(){
        //        console.log("TablePage: loadSample index "+index)
        var samplePage=root.pagePool.pageForUrl("pages/SamplePage.qml")
        //        samplePage.title="Index "+(mainList.count-index)
        //        samplePage.verticalScrollBarPolicy=Qt.ScrollBarAlwaysOff
        //        samplePage.verticalScrollBarPolicy=Qt.ScrollBarAsNeeded
        if(mainListView.model!==samplePage.model){
            samplePage.setModel(mainListView.model,tabBar.model[tabBar.currentIndex])
        }
        root.pageStack.push(samplePage)
        samplePage.setIndex(mainListView.currentIndex)
    }
}

