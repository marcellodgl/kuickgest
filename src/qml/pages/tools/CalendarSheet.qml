/*
Kuickgest: Resource managment system interfaced through REST API.

Copyright (c) 2022 Marcello Di Guglielmo

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

import QtQuick 2.10
import org.kde.kirigami as Kirigami
import QtQuick.Controls
import QtQuick.Layouts 1.14

Kirigami.OverlaySheet {
    id: gridViewSheet
    property date selectedDate
    property date controlDate : new Date()
    onVisibleChanged: {
        if(visible) controlDate = new Date()
    }
    header: RowLayout{
        Kirigami.Heading {
            text: grid.title
            Layout.alignment: Qt.AlignCenter
        }
    }

    footer: RowLayout{
        ToolButton{
            id:leftButtonMonth
            icon.name: "arrow-left"
            autoRepeat: true
            Layout.alignment: Qt.AlignLeft
            onClicked: if(grid.month>Calendar.January) grid.month--
        }
        ToolButton{
            id:rightButtonMonth
            icon.name: "arrow-right"
            autoRepeat: true
            Layout.alignment: Qt.AlignLeft
            onClicked: if(grid.month<Calendar.December) grid.month++
        }

        ToolButton{
            id:leftButtonYear
            icon.name: "arrow-left"
            autoRepeat: true
            Layout.alignment: Qt.AlignRight
            onClicked: grid.year--
        }
        ToolButton{
            id:rightButtonYear
            icon.name: "arrow-right"
            autoRepeat: true
            Layout.alignment: Qt.AlignRight
            onClicked: grid.year++
        }

    }


    GridLayout {
        columns: 2
        DayOfWeekRow {
            locale: grid.locale

            Layout.column: 1
            Layout.fillWidth: true
        }

        WeekNumberColumn {
            month: grid.month
            year: grid.year
            locale: grid.locale

            Layout.fillHeight: true
        }

        MonthGrid {
            id: grid

            month: selectedDate.toLocaleDateString()!=="" ? selectedDate.getMonth() : controlDate.getMonth()
            year: selectedDate.toLocaleDateString()!=="" ? selectedDate.getFullYear() : controlDate.getFullYear()
            locale: Qt.locale()

            Layout.fillWidth: true
            Layout.fillHeight: true
            delegate: Text {
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                opacity: model.month === grid.month ? 1 : 0.5
                font.bold: model.date.getDate() === controlDate.getDate() && model.date.getMonth() === controlDate.getMonth() && model.date.getYear() === controlDate.getYear()// ? Kirigami.Theme.highlightColor : Kirigami.Theme.textColor
                text: model.day
//                Component.onCompleted: console.log("date "+model.date.toLocaleDateString()+" currentDate "+controlDate)

                required property var model
                MouseArea{
                    id: cellMouseArea
                    anchors.fill: parent
                    hoverEnabled: true
                    propagateComposedEvents: true
                    onClicked: grid.clicked(model.date)
                }

                Rectangle{
                    z:-1
                    anchors.fill: parent
                    color: Kirigami.Theme.highlightColor
                    radius: height/2
                    visible: cellMouseArea.containsMouse || (model.date.getDate() === selectedDate.getDate() && model.date.getMonth() === selectedDate.getMonth() && model.date.getYear() === selectedDate.getYear())
                }
            }
            onClicked: {
                selectedDate=date
                gridViewSheet.close()
            }

        }

    }
}

