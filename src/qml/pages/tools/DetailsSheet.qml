/*
Kuickgest: Resource managment system interfaced through REST API.

Copyright (c) 2022 Marcello Di Guglielmo

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

import QtQuick 2.15
import org.kde.kirigami as Kirigami
import QtQuick.Controls
import QtQuick.Layouts 1.14

Kirigami.OverlaySheet {
    id: sheetListView

    property string filterText: ""
    property string tableName: ""
    property alias model: detailsList.model
    property string textRole: ""
    property bool sortUp: false
    property bool sortDown: false
    property bool warningVisible: false
    property bool errorVisible: false
    property bool addMode: false
    property string errorText: ""
    //    signal selectedText(string text)

    property string selectedText: ""
    contentItem.height: 100
    onVisibleChanged: {
        if(visible){
            sheetListView.footer.detailsFilterText=""
            sheetListView.footer.detailsSortUp=false
            sheetListView.footer.detailsSortDown=false
//On start selectedText has to be emptyied for next load
            selectedText=""
            warningVisible=false
        }
    }
    header: RowLayout{
        Kirigami.Heading {
            text: tableName
//            Layout.alignment: Qt.AlignCenter
        }
        Pane{
            Layout.fillWidth: true
        }

        ToolButton{
            text: i18n("Add")
            icon.name: "list-add"
            Layout.alignment: Qt.AlignRight
            enabled: !addMode
            onClicked: {
                var rowCount=detailsList.model.rowCount()
                detailsList.model.add()
                detailsList.currentIndex=rowCount
                detailsList.currentItem.editMode=true
                detailsList.positionViewAtEnd()
                addMode=true
            }
        }
        ToolButton{
            text: i18n("Refresh")
            icon.name: "view-refresh"
            onClicked: detailsList.model.loadContents()
            Layout.alignment: Qt.AlignRight
            Layout.rightMargin: Kirigami.Units.mediumSpacing

        }
    }





    ListView {
        id: detailsList
        implicitWidth: Kirigami.Units.gridUnit*16

        highlightFollowsCurrentItem: true
        delegate: Kirigami.SwipeListItem {
            id: listItem
//            highlighted: index===detailsList.currentIndex
            property bool editMode: false
            RowLayout{
                Label{
                    id: swipeLabel
                    visible: !editMode || index!==detailsList.currentIndex
                    text:model[textRole]!==undefined ? model[textRole] : ""
                }
                Kirigami.ActionTextField{
                    id: swipeTextField
                    visible: editMode && index===detailsList.currentIndex
                    Layout.fillWidth: true
                    rightActions: [
                        Kirigami.Action{
                            id: closeAction
                            icon.name: "dialog-close"
                            onTriggered: listItem.editMode=false
                            // Shortcut{
                            //     enabled: listItem.hovered
                            //     sequence: StandardKey.Cancel
                            //     onActivated: closeAction.trigger()
                            // }
                        },
                        Kirigami.Action{
                            id: saveAction
                            icon.name: "document-save"
                            onTriggered: {
                                if(swipeTextField.visible){
                                    setData(index,swipeTextField.text)
                                    if(addMode && index===detailsList.count-1) addMode=false
                                    listItem.editMode=false
                                }
                            }
                        }

                    ]
                    onAccepted: saveAction.trigger()
                    onVisibleChanged: {
                        if(visible){
                            text=model[textRole]!==undefined ? model[textRole] : ""
                        }
                    }
                }
            }


            width: detailsList.width

            visible: sheetListView.filterText.length>0 ? model[textRole]!==null && model[textRole].toLowerCase().includes(sheetListView.filterText.toLowerCase()) : true

            height: visible===true ? implicitHeight : 0
            onClicked: {
                selectedText=swipeLabel.text
                close()
            }
            actions: [
                Kirigami.Action {
                    id: deleteAction
                    icon.name: "delete"
                    text: i18n("Remove")
                    // Shortcut{
                    //     enabled: listItem.hovered
                    //     sequence: StandardKey.Delete
                    //     onActivated: deleteAction.trigger()
                    // }

                    onTriggered: {
                        detailsList.currentIndex=index
                        warningVisible=true
                    }
                },
                Kirigami.Action {
                    id: modifyAction
                    icon.name: "text-field"
                    text: i18n("Modify")
                    // Shortcut{
                    //     enabled: listItem.hovered
                    //     sequence: "F2"
                    //     onActivated: modifyAction.trigger()
                    // }

                    onTriggered: {
                        listItem.editMode=true
                        detailsList.currentIndex=index
                    }
                },
                Kirigami.Action {
                    id: editAction
                    icon.name: "document-edit"
                    text: i18n("Edit")
                    // Shortcut{
                    //     enabled: listItem.hovered
                    //     sequence: "F3"
                    //     onActivated: editAction.trigger()
                    // }
                    onTriggered: {
                        detailsList.currentIndex=index
                        loadSubSample()
                    }
                }
            ]
        }
        headerPositioning: ListView.OverlayHeader
        header: Pane{
            id: detailsHeader
            z:2
            width: detailsList.width
            Kirigami.InlineMessage {
                type: Kirigami.MessageType.Error
                id: errorDialog
                width: detailsHeader.width-2*detailsHeader.padding
                visible: errorVisible

                text: errorText
                actions: [
                    Kirigami.Action{
                        text: i18n("Close")
                        icon.name: "dialog-close"

                        onTriggered: {
                            detailsList.currentIndex=-1
                            errorVisible=false
                            errorText=""
                            detailsList.model.revert()
                        }
                    }

                ]
            }
        }



        footerPositioning: ListView.OverlayFooter
        footer: Pane{
            id: detailsFooter
            z:2
            width: detailsList.width
            Kirigami.InlineMessage {
                type: Kirigami.MessageType.Warning
                id: confirmationRemoveDialog
                width: detailsFooter.width-2*detailsFooter.padding
                visible: warningVisible

                text: i18n("Are you sure to remove selected row?")
                actions: [
                    Kirigami.Action{
                        text: i18n("Delete")
                        icon.name: "delete"

                        onTriggered: {
                            console.log("Delete")
                            detailsList.model.remove(detailsList.currentIndex)
                            warningVisible=false
                        }
                    },
                    Kirigami.Action{
                        text: i18n("Close")
                        icon.name: "dialog-close"

                        onTriggered: {
                            detailsList.currentIndex=-1
                            warningVisible=false
                        }
                    }

                ]
            }
        }
    }
    footer: RowLayout{
        id: detailListRow
        property alias detailsFilterText: filterField.text
        property alias detailsSortUp: sortUpButton.checked
        property alias detailsSortDown: sortDownButton.checked
        z:2
        spacing: 5
        TextField {
            id: filterField

            width: parent.width-sortUpButton.width-sortDownButton.width-parent.spacing*2
            placeholderText: "Filter"
            onTextChanged: {
                filterText=text
            }
        }
        ToolButton{
            id:sortUpButton

            checkable: true

            icon.name: "view-sort-ascending"
            autoRepeat: true
            onCheckedChanged: {
                sortUp=checked
                if(checked) sortDownButton.checked=false
            }
        }
        ToolButton{
            id:sortDownButton

            checkable: true
            icon.name: "view-sort-descending"
            autoRepeat: true
            onCheckedChanged: {
                sortDown=checked
                if(checked) sortUpButton.checked=false
            }

        }
    }

    function setModel(model,name){
        tableName=name
        //        mapper has to be cleared before mainListView model because in clearing that there are movement with mapper currentIndex that refer to an inexistent model
        if(model){
//            console.log("relMapper "+root)
//            root.relMapper.clear()
//            root.relMapper.setModel(model)

            sheetListView.model=model
        }
    }
    function setData(index,value){
        console.log("DetailsSheet: setData "+index+" value "+value)
        var modelIndex=sheetListView.model.index(index,0)
        var itemRoles=sheetListView.model.itemsRoleList()
        var section=-1
        for(var i in itemRoles)
        {
            var role=itemRoles[i]
            var roleName=sheetListView.model.itemName(role)
            if(roleName===textRole){
                section=role
                break
            }
        }
        if(section>=0){
            var submitOk=sheetListView.model.setData(modelIndex,value,section)
            if(submitOk) sheetListView.model.submitRow(index)
        }

        console.log("role section "+section)
    }
    function setError(text){
        errorVisible=true
        errorText=text
    }
    function loadSubSample(){
        //        console.log("TablePage: loadSample index "+index)
        var samplePage=root.pagePool.pageForUrl("pages/SamplePage.qml")
        //        samplePage.title="Index "+(mainList.count-index)
        //        samplePage.verticalScrollBarPolicy=Qt.ScrollBarAlwaysOff
        //        samplePage.verticalScrollBarPolicy=Qt.ScrollBarAsNeeded
        if(detailsList.model!==samplePage.model){
            samplePage.setModel(detailsList.model,tableName)
        }
        root.pageStack.push(samplePage)
        samplePage.setIndex(detailsList.currentIndex)
    }
}

