/*
Kuickgest: Resource managment system interfaced through REST API.

Copyright (c) 2022 Marcello Di Guglielmo

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

import QtQuick 2.10
import org.kde.kirigami 2.19 as Kirigami
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.14
// import Qt.labs.calendar 1.0
Kirigami.Dialog {
    id: listViewDialog
    property var tablesList: []
    title: i18n("Table Order")
    standardButtons: Kirigami.Dialog.Ok
    padding: Kirigami.Units.smallSpacing

    ListView{
        id: orderListView
        property alias orderModel: orderModel
//        property var tablesList: []

//        onTablesListChanged: headerItem.comboBoxModel=tablesList

        onCurrentIndexChanged: console.log("currentIndex "+currentIndex)
        onCurrentItemChanged: console.log("currentItem "+currentItem)
        implicitWidth: Kirigami.Units.gridUnit*16
        implicitHeight: Kirigami.Units.gridUnit*16

        header:RowLayout{
            id:headerRow
            property alias comboBoxModel: orderComboBox.model
            spacing: Kirigami.Units.smallSpacing
            width: orderListView.width


            ComboBox{
                id:orderComboBox

                Layout.alignment: Qt.AlignLeft
//                width: root.availableWidth - addButton.width - removeButton.width-addRow.spacing*2
                model: tablesList
                Connections{
                    target:listViewDialog
                    function onTablesListChanged() {
                        orderComboBox.model=listViewDialog.tablesList
                    }
                }

            }
            Item{
                id: spacer
                Layout.fillWidth: true
            }

            RoundButton{
                id:addButton
                Layout.alignment: Qt.AlignRight
                text: i18n("Add")
                onClicked:   {
                    var comboList=orderComboBox.model
                    var tableName=comboList.splice(orderComboBox.currentIndex,1)
                    orderComboBox.model=comboList
                    //                    tableName è una lista come formato pertanto ne prendo il primo elemento
                    orderModel.append({"table":tableName[0],"direction":"desc"})
                    orderListView.currentIndex=orderModel.count-1
                }
            }


            RoundButton{
                id:removeButton
                Layout.alignment: Qt.AlignRight
                text:i18n("Remove")
                enabled: orderListView.currentIndex>=0
                onClicked: {
                    var orderTable=orderModel.get(orderListView.currentIndex)["table"]
                    orderModel.remove(orderListView.currentIndex)
                    var comboList=orderComboBox.model
                    comboList.push(orderTable)
                    orderComboBox.model=comboList
                }
            }
        }
        model: orderModel

        delegate: ItemDelegate{
            id:orderItemDelegate
            text:model.table
            implicitWidth: orderListView.width
            highlighted: orderListView.currentIndex===index
            onClicked: orderListView.currentIndex=index
            indicator: RoundButton{
                id:buttonVersus
                anchors.right: parent.right
                anchors.verticalCenter: parent.verticalCenter
                anchors.margins: Kirigami.Units.smallSpacing
                icon.name: model.direction==="asc" ? "arrow-down" : "arrow-up"
                display: AbstractButton.IconOnly
                text:i18n("Versus")
                onClicked: model.direction==="desc" ? model.direction="asc" : model.direction="desc"
            }

        }

        ListModel{
            id:orderModel
        }
    }
    function setOrderSequence(value){
        var orderSequenceList = value.split(",")
        orderListView.orderModel.clear()
        var comboList=tablesList
        for(var i=0;i<orderSequenceList.length;i++){
            var orderKey=orderSequenceList[orderSequenceList.length-i-1]
            var orderTable=orderKey.split(".")[0]
            var orderDirection=orderKey.split(".")[1]
            orderListView.orderModel.append({"table":orderTable,"direction":orderDirection})
            comboList.splice(comboList.indexOf(orderTable),1)
        }
        tablesList=comboList

    }
    function getOrderSequence(){
        var orderList = []
        for(var i=0;i<orderListView.orderModel.count;i++){
            var orderElement=orderListView.orderModel.get(orderListView.orderModel.count-i-1)
            var orderKey=orderElement["table"]+"."+orderElement["direction"]
            orderList.push(orderKey)
        }
        var orderSequence=orderList.join(",")
        return orderSequence
    }
}

