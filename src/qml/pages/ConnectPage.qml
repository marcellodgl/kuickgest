/*
Kuickgest: Resource managment system interfaced through REST API.

Copyright (c) 2022 Marcello Di Guglielmo

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

import QtQuick 2.6

// property var serverField. Then inside urlField: Component.onCompleted: serverField = urlField. Access that field by serverField
import QtQuick.Controls 2.5
import plasma.kuickgest.InterfaceSettings 1.0
import org.kde.kirigami 2.8 as Kirigami
import QtQuick.Layouts 1.2

Kirigami.OverlaySheet {
    // Sheet mode
    property string connectionUrl: ""
    property int screenQuality: 0
    property var historyList: []
    signal accepted()

    id: connectSheet

    function readModelList(){


    }



    InterfaceSettings {
        id:settingsConfiguration

    }

    //il caricamento della lista di url dai settings va fatto partendo da una lista vuota perchè non viene caricata come component
    //ma è sempre la stessa.
    onSheetOpenChanged: {
        if(sheetOpen){
            historyModel.clear()
            historyList=settingsConfiguration.value("urlHistory")
            if(typeof(historyList)!="undefined"){
	        for(var i=0;i<historyList.length;i++){
                    historyModel.append({"connectionUrl":historyList[i]})
                }
	    }
        }
    }

    header: Kirigami.Heading {
        id: sheetHeader
        text: i18nc("@title:window", "Connect")
    }
    Kirigami.FormLayout {
        height: urlField.height+qualitySlider.height+clearAllButton.height+120
        // Textfields let you input text in a thin textbox
        TextField {
            id: urlField
            width: parent.width
            //                EnterKey.onClicked: accept()
            onTextChanged: connectSheet.connectionUrl=text
            text:connectSheet.connectionUrl

            // Provides label attached to the textfield
            Kirigami.FormData.label: i18nc("@label:textbox", "Url")
            // Placeholder text is visible before you enter anything
            placeholderText: i18n("Server URL")
            // What to do after input is accepted (i.e. pressed enter)
            // In this case, it moves the focus to the next field
            onAccepted: acceptButton.forceActiveFocus()
        }
        // This is a button.

        Slider {
            id: qualitySlider
//            width: parent.width
//            anchors.horizontalCenter: parent.horizontalCenter
            Kirigami.FormData.label: i18n("Quality")
            from: 1
            to: 3
            stepSize: 1
            function sliderLabel(){
                if(value==1) return i18n("Low (ISDN)")
                else if(value==2) return i18n("Medium (DSL)")
                else if(value==3) return i18n("High (LAN)")
                else return ""
            }
            //            valueText: sliderLabel()
            onValueChanged: connectSheet.screenQuality=value
	    ToolTip.visible: pressed
            ToolTip.text: sliderLabel() 
            value:1
        }
        Button {
            id: clearAllButton
            Layout.fillWidth: true
            icon.name: "edit-clear-history"
            text: i18nc("@action:button", "Clear all")
            onClicked: {
                historyModel.clear()
                settingsConfiguration.setValue("urlHistory",[])
            }
        }

        ScrollView{
            height: 100
            Layout.fillWidth: true

            ListView {
                id:historyView
                anchors.fill: parent

                flickableDirection: Flickable.AutoFlickDirection
                //                headerPositioning: ListView.OverlayHeader
                //headerPositioning: ListView.PullBackHeader
                header: Kirigami.ItemViewHeader {
                    maximumHeight: urlField.height
                    id:historyLabel
                    title:i18n("Connection history")

                    //                    font.bold: true
                }



                model: ListModel {
                    id:historyModel
                }

                delegate:
                    Kirigami.SwipeListItem {

                    id:listEntry

                    Label{
                        id: labelEntry
                        text: connectionUrl
                    }
                    onClicked: {
                        console.log("Swipe configuration clicked")
                        connectSheet.connectionUrl=labelEntry.text
                    }



                    actions: [
//                        Kirigami.Action{
//                            iconName: "select"
//                            text:i18n("Select")
//                            onTriggered: connectSheet.connectionUrl=labelEntry.text
//                        },
                        Kirigami.Action{
                            iconName: "delete"
                            text:i18n("Remove")
                            onTriggered: function removeListItem() {
                                historyModel.remove(model.index)
                            }
                        }
                    ]
                }

            }
        }
    }

    footer: Button {
        id: acceptButton
        Layout.fillWidth: true
        text: i18nc("@action:button", "Accept")
        // Button is only enabled if the user has entered something into the nameField
        enabled: urlField.text.length > 0
        icon.name: "dialog-ok"
        onClicked: {
            // Add a listelement to the kountdownModel ListModel
            historyList=[]
            for(var i=0;i<historyModel.count;i++)
            {
                var listElement=historyModel.get(i)
                historyList[i]=listElement["connectionUrl"]

            }
            console.log("before "+historyList)
            console.log("connectSheet.connectionUrl"+connectSheet.connectionUrl)
            if(connectSheet.connectionUrl.length>0 )
            {
                if(historyList.indexOf(connectSheet.connectionUrl)!=-1)
                {
                    console.log("exist"+historyList.indexOf(connectSheet.connectionUrl))
                    historyList.splice(historyList.indexOf(connectSheet.connectionUrl),1)
                }
                console.log("historyList "+historyList)
                historyList.unshift(connectSheet.connectionUrl)
                console.log("saving historyList "+historyList)
                settingsConfiguration.setValue("urlHistory",historyList)
            }
            close();
            connectSheet.accepted()


        }
    }





}


