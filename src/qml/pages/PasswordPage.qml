/*
Kuickgest: Resource managment system interfaced interfaced through REST API.

Copyright (c) 2022 Marcello Di Guglielmo

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

import QtQuick 2.0
import org.kde.kirigami 2.5 as Kirigami
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3
Kirigami.OverlaySheet {
    id: passwordSheet
    property string connectionPassword: ""
    signal accepted()
    header: Kirigami.Heading{
        id: header
        text: i18nc("@title:window", "Password")
    }
    onSheetOpenChanged: {
        if(sheetOpen){
            passwordField.text=""
        }
    }

    Kirigami.FormLayout {
        // Textfields let you input text in a thin textbox
        TextField {
            id: passwordField
            placeholderText: i18n("Server Password")
            width: parent.width
            echoMode: TextInput.Password
            //                EnterKey.onClicked: accept()

            // Provides label attached to the textfield
            Kirigami.FormData.label: i18nc("@label:textbox", "Password")
            // Placeholder text is visible before you enter anything

            // What to do after input is accepted (i.e. pressed enter)
            // In this case, it moves the focus to the next field
            onAccepted: acceptButton.forceActiveFocus()
        }
    }
    // This is a button.

    footer:Button {
        id: acceptButton
        Layout.fillWidth: true
        text: i18nc("@action:button", "Ok")
        // Button is only enabled if the user has entered something into the nameField
        enabled: passwordField.text.length > 0
        icon.name: "dialog-ok"
        onClicked: {
            // Add a listelement to the kountdownModel ListModel
            connectionPassword=passwordField.text
            passwordSheet.accepted()
            close();

        }
    }

}


