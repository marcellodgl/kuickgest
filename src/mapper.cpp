#include "mapper.h"

Mapper::Mapper(QObject *parent)
    : QObject(parent)
{
    m_model = 0;
    m_currentIndex = -1;
    signalMapper = new QSignalMapper(this);
    signalMapper1 = new QSignalMapper(this);

#if QT_VERSION >= 0x060000
    connect(signalMapper, SIGNAL(mappedInt(int)), this, SLOT(valueChanged(int)));
    connect(signalMapper1, SIGNAL(mappedInt(int)), this, SLOT(displayChanged(int)));
#else
    connect(signalMapper, SIGNAL(mapped(int)), this, SLOT(valueChanged(int)));
    connect(signalMapper1, SIGNAL(mapped(int)), this, SLOT(displayChanged(int)));
#endif
}

void Mapper::addMapping(QObject *widget, int section, Mapper::Type type)
{
    qDebug() << "Mapper - addMapping: widget" << widget << "section" << section << "type" << type;
    signalMapper->setMapping(widget, section);
    signalMapper1->setMapping(widget, section);
    widgetMap.insert(section, widget);
    widgetTypeMap.insert(section, type);
    QVariant value = widgetValueMap.value(section);
    //    Initializing value valid even when the item reappear throught delegate after been deleted because not visible
    //    the value is stored in widgetValueMap
    //    It could be happen that addMapping occur after setCurrentIndex so it's need to force setWidgetValue here
    setWidgetValue(section, value);
    //    connection needed to follow the Property of item: text, value currentIndex ecc.
    //    react with method valueChanged to update inside the Mapper acquiring the value in the widgetValueMap
    if (type == Mapper::String || type == Mapper::Script) {
        connect(widget, SIGNAL(textChanged()), signalMapper, SLOT(map()));
    } else if (type == Mapper::Date) {
        connect(widget, SIGNAL(textChanged()), signalMapper, SLOT(map()));
        connect(widget, SIGNAL(secondaryTextChanged()), signalMapper, SLOT(map()));
    } else if (type == Mapper::Int) {
        connect(widget, SIGNAL(valueChanged()), signalMapper, SLOT(map()));
        connect(widget, SIGNAL(secondaryValueChanged()), signalMapper, SLOT(map()));
    } else if (type == Mapper::Number) {
        connect(widget, SIGNAL(inputValueChanged()), signalMapper, SLOT(map()));
        connect(widget, SIGNAL(secondaryInputValueChanged()), signalMapper, SLOT(map()));
    } else if (type == Mapper::Bool) {
        connect(widget, SIGNAL(checkedChanged()), signalMapper, SLOT(map()));
    } else if (type == Mapper::Enum) {
        connect(widget, SIGNAL(currentIndexChanged()), signalMapper, SLOT(map()));
    } else if (type == Mapper::List) {
        connect(widget, SIGNAL(currentIndexChanged()), signalMapper, SLOT(map()));
        connect(widget, SIGNAL(editTextChanged()), signalMapper1, SLOT(map()));
    }

    connect(widget, SIGNAL(destroyed(QObject *)), this, SLOT(deleteItem(QObject *)));
}

QAbstractItemModel *Mapper::model() const
{
    return m_model;
}

void Mapper::setCurrentIndex(int currentIndex)
{
    // Senza la condizione index negativo si sfrutta per azzerare tutti i campi del mapper
    //     if(index<0)
    //     {
    //         return;
    //     }

    if (m_model) {
        qDebug() << "Mapper: setCurrentIndex" << currentIndex;
        qDebug() << "- m_model" << m_model;
        qDebug() << "- roleNames" << m_model->roleNames();
        QHash<int, QByteArray> roleNamesMap = m_model->roleNames();
        QMapIterator<int, QObject *> iterator(widgetMap);
        qDebug() << "Mapper: setCurrentIndex - widgetMap length" << widgetMap.count();
        QList<int> roles = roleNamesMap.keys();
        QListIterator<int> iteratorRoles(roles);
        while (iteratorRoles.hasNext()) {
            int section = iteratorRoles.next();
            QModelIndex modelIndex = m_model->index(currentIndex, 0);

            QVariant value = m_model->data(modelIndex, section);
            qDebug() << "Mapper: setCurrentIndex - modelIndex" << modelIndex << "value" << value << "section" << section;
            //            the insert in widgetValueMap is used to store the value for the addMapping() use that in first case has not instantiated widget whose
            //            value are not available.
            widgetValueMap.insert(section, value);
            setWidgetValue(section, value);
        }
    }
    if (m_currentIndex == currentIndex)
        return;
    m_currentIndex = currentIndex;
    Q_EMIT currentIndexChanged();
}

void Mapper::setModel(QAbstractItemModel *model)
{
    if (m_model == model)
        return;

    m_model = model;
    widgetMap.clear();
    widgetTypeMap.clear();

    Q_EMIT modelChanged(m_model);
}

bool Mapper::submit()
{
    bool submitOk = true;
    if (m_model) {
        qDebug() << "submit on index" << m_currentIndex;
        QMapIterator<int, QObject *> iterator(widgetMap);

        while (iterator.hasNext()) {
            iterator.next();
            int section = iterator.key();
            QObject *widget = iterator.value();
            Type widgetType = widgetTypeMap.value(section);

            QModelIndex modelIndex = m_model->index(m_currentIndex, 0);
            if (widgetType == Mapper::String) {
                qDebug() << "submit string";
                QVariant value = widgetValueMap.value(section, QVariant());
                // Conversion to string necessary for comparison on empty value modelIndex.data(section) is Null and value is Invalid considered different
                if (modelIndex.data(section).toString() != value.toString()) {
                    qDebug() << "inserting text on" << modelIndex << "value" << value << "valueNull" << value.isNull() << "section" << section << "data"
                             << modelIndex.data(section) << "modelIndexDataNull" << modelIndex.data(section).isNull();
                    QString valueText = value.toString();
                    if (valueText.isEmpty()) {
                        value = QVariant();
                    }
                    bool ok = m_model->setData(modelIndex, value, section);

                    submitOk = submitOk & ok;
                }
            } else if (widgetType == Mapper::Script) {
                QVariant value = widgetValueMap.value(section);
                // Conversion to string necessary for comparison on empty value modelIndex.data(section) is Null and value is Invalid considered different
                if (modelIndex.data(section).toString() != value.toString()) {
                    qDebug() << "inserting text on" << modelIndex << "value" << value << "section" << section;
                    QString valueText = value.toString();
                    if (valueText.isEmpty()) {
                        value = QVariant();
                    }
                    bool ok = m_model->setData(modelIndex, value, section);

                    submitOk = submitOk & ok;
                }
            } else if (widgetType == Mapper::Date) {
                QVariant value = widgetValueMap.value(section);
                QString valueText = value.toString();
                if (valueText.isEmpty()) {
                    value = QVariant();
                }
                // Conversion to date necessary for comparison on empty value modelIndex.data(section) is Null and value is Invalid considered different
                if (modelIndex.data(section).toDate() != value.toDate()) {
                    qDebug() << "inserting text on" << modelIndex << "value" << value << "section" << section << "modelIndex.data"
                             << modelIndex.data(section).toDate();
                    bool ok = m_model->setData(modelIndex, value, section);
                    submitOk = submitOk & ok;
                }
            } else if (widgetType == Mapper::Int) {
                QVariant value = widgetValueMap.value(section);
                // Conversion to string necessary for comparison on empty value modelIndex.data(section) is Null and value is Invalid considered different
                if (modelIndex.data(section).toInt() != value.toInt()) {
                    qDebug() << "inserting int on" << modelIndex << "value" << value << "section" << section;
                    int valueInteger = value.toInt();
                    if (valueInteger == 0) {
                        value = QVariant();
                    }
                    bool ok = m_model->setData(modelIndex, value, section);
                    submitOk = submitOk & ok;
                }

            } else if (widgetType == Mapper::Number) {
                QVariant value = widgetValueMap.value(section);
                // Conversion to string necessary for comparison on empty value modelIndex.data(section) is Null and value is Invalid considered different
                if (modelIndex.data(section).toDouble() != value.toDouble()) {
                    qDebug() << "inserting int on" << modelIndex << "value" << value << "section" << section;
                    double valueNumber = value.toDouble();
                    if (valueNumber == 0) {
                        value = QVariant();
                    }
                    bool ok = m_model->setData(modelIndex, value, section);
                    submitOk = submitOk & ok;
                }

            } else if (widgetType == Mapper::Bool) {
                QVariant value = widgetValueMap.value(section);
                // Conversion to bool necessary for comparison on empty value modelIndex.data(section) is Null and value is Invalid considered different
                if (modelIndex.data(section).toBool() != value.toBool()) {
                    qDebug() << "inserting bool on" << modelIndex << "value" << value << "section" << section;
                    if (value.toBool() == false) {
                        value = QVariant();
                    }
                    bool ok = m_model->setData(modelIndex, value, section);
                    submitOk = submitOk & ok;
                }
            } else if (widgetType == Mapper::Enum) {
                QVariant value = widgetValueMap.value(section);
                int currentIndex = value.toInt();
                //                QAbstractItemModel *comboModel=widget->property("model").value<QAbstractItemModel*>();
                //                QModelIndex comboModelIndex=comboModel->index(currentRow,0);

                //                QByteArray comboIndex=widget->property("indexRole").toByteArray();
                //                QHash<int,QByteArray> comboRoles= comboModel->roleNames();
                //                int comboIndexRole=comboRoles.key(comboIndex);
                //                QVariant valueData=comboModelIndex.data(comboIndexRole);
                //                qDebug()<<"combo submit for index"<<currentRow<<"value"<<valueData<<"modelData"<<modelIndex.data(section)<<"modelData is
                //                null"<<modelIndex.data(section).isNull();
                //                //nel caso in cui il currentrow=-1 -> value e anche nel modello il valore è nullo evita di agire con l'if.
                //                if(!(modelIndex.data(section).isNull()&&valueData.isNull()))
                //                {
                //                    if(modelIndex.data(section)!=valueData)
                //                    {
                //                        qDebug()<<"inserting list
                //                        on"<<modelIndex<<"value"<<value<<"null"<<value.isNull()<<"section"<<section<<"modeldata"<<modelIndex.data(section);
                //                        bool ok=m_model->setData(modelIndex,valueData,section);
                //                        submitOk=submitOk & ok;
                //                    }
                //                }
            } else if (widgetType == Mapper::List) {
                /*****
                QVariant value=widget->property("currentText");
                if(value.isNull())
                {
                    value=QVariant(QString());
                }
                */

                QVariant valueData = widgetValueMap.value(section);

                qDebug() << "Mapper modelData" << modelIndex.data(section) << "has key" << widgetValueMap.contains(section) << "valueData" << valueData;
                // Conversion to map necessary for comparison on empty value modelIndex.data(section) is Null and value is Invalid considered different
                if (modelIndex.data(section).toMap() != valueData) {
                    qDebug() << "inserting list on" << modelIndex << "section" << section << "modeldata" << modelIndex.data(section);
                    bool ok = m_model->setData(modelIndex, valueData, section);
                    submitOk = submitOk & ok;
                }

            } else {
                // caso in cui il campo non è cambiato per nulla
                submitOk = submitOk & true;
            }
        }
    }

    return submitOk;
}

void Mapper::revert()
{
    qDebug() << "Mapper: revert - appling m_currentIndex" << m_currentIndex;
    setCurrentIndex(m_currentIndex);
}

void Mapper::clear()
{
    setModel(nullptr);
    widgetMap.clear();
    widgetTypeMap.clear();
    // Important to clear values, in other case on changing of the model items find values of precedent model
    widgetValueMap.clear();
    widgetSecondaryValueMap.clear();
}

QVariantMap Mapper::itemsValueMap()
{
    QVariantMap valueMap;
    if (m_model) {
        QMapIterator<int, QObject *> iterator(widgetMap);

        QHash<int, QByteArray> roleNames = m_model->roleNames();
        while (iterator.hasNext()) {
            iterator.next();
            int section = iterator.key();
            QObject *widget = iterator.value();
            Type widgetType = widgetTypeMap.value(section);
            QString roleName = QString::fromUtf8(roleNames.value(section));
            //            QModelIndex modelIndex=m_model->index(currentIndex,0);
            qDebug() << "section " << section << "widget" << widget;
            QVariant value = widgetValueMap.value(section);
            if (widgetType == Mapper::List) {
                int currentRow = value.toInt();
                QAbstractItemModel *comboModel = widget->property("model").value<QAbstractItemModel *>();
                qDebug() << "comboModel" << comboModel << "currentRow" << currentRow << "value" << value << "widgetValueMap" << widgetValueMap;
                if (comboModel) {
                    QModelIndex comboModelIndex = comboModel->index(currentRow, 0);

                    QByteArray comboIndex = widget->property("indexRole").toByteArray();
                    QHash<int, QByteArray> comboRoles = comboModel->roleNames();
                    int comboIndexRole = comboRoles.key(comboIndex);
                    qDebug() << "comboModelIndex" << comboModelIndex;
                    QVariant comboIndexValue = comboModelIndex.data(comboIndexRole);
                    qDebug() << "combo submit for index" << currentRow << "value" << value;
                    // nel caso in cui il currentrow=-1 -> value e anche nel modello il valore è nullo evita di inserire il campo nella ricerca
                    valueMap.insert(roleName, comboIndexValue);
                }
            } else {
                valueMap.insert(roleName, value);
            }
        }
    }

    return valueMap;
}

QVariantMap Mapper::secondaryItemsValueMap()
{
    QVariantMap secondaryValueMap;
    if (m_model) {
        QMapIterator<int, QObject *> iterator(widgetMap);

        QHash<int, QByteArray> roleNames = m_model->roleNames();
        while (iterator.hasNext()) {
            iterator.next();
            int section = iterator.key();
            QObject *widget = iterator.value();
            Type widgetType = widgetTypeMap.value(section);
            QString roleName = QString::fromUtf8(roleNames.value(section));
            //            QModelIndex modelIndex=m_model->index(currentIndex,0);
            qDebug() << "section " << section << "widget" << widget;
            QVariant secondaryValue = widgetSecondaryValueMap.value(section);
            secondaryValueMap.insert(roleName, secondaryValue);
        }
    }

    return secondaryValueMap;
}

QVariantMap Mapper::itemsOperatorMap()
{
    QVariantMap operatorMap;
    if (m_model) {
        QMapIterator<int, QObject *> iterator(widgetMap);
        QHash<int, QByteArray> roleNames = m_model->roleNames();
        while (iterator.hasNext()) {
            iterator.next();

            int section = iterator.key();
            QString roleName = QString::fromUtf8(roleNames.value(section));
            QObject *widget = iterator.value();
            QVariant value = widget->property("operator");
            bool widgetAltered = widget->property("altered").toBool();
            if (widgetAltered) {
                operatorMap.insert(roleName, value);
            } else {
                operatorMap.insert(roleName, QStringLiteral(""));
            }
        }
    }
    return operatorMap;
}

QVariant Mapper::itemDisplay(QString name)
{
    if (m_model) {
        QMapIterator<int, QObject *> iterator(widgetMap);

        QHash<int, QByteArray> roleNames = m_model->roleNames();
        while (iterator.hasNext()) {
            iterator.next();
            int section = iterator.key();
            QObject *widget = iterator.value();
            Type widgetType = widgetTypeMap.value(section);
            QString roleName = QString::fromUtf8(roleNames.value(section));
            if (roleName == name) {
                QVariant value = widgetValueMap.value(section);
                if (widgetType == Mapper::List) {
                    int currentRow = value.toInt();
                    QAbstractItemModel *comboModel = widget->property("model").value<QAbstractItemModel *>();
                    qDebug() << "comboModel" << comboModel << "currentRow" << currentRow;
                    if (comboModel) {
                        QModelIndex comboModelIndex = comboModel->index(currentRow, 0);
                        QByteArray comboDisplay = widget->property("textRole").toByteArray();
                        QHash<int, QByteArray> comboRoles = comboModel->roleNames();
                        int comboDisplayRole = comboRoles.key(comboDisplay);
                        qDebug() << "comboModelIndex" << comboModelIndex;
                        QVariant comboDisplayValue = comboModelIndex.data(comboDisplayRole);
                        return comboDisplayValue;
                    }
                } else {
                    return value;
                }
            }
        }
    }
    return QVariant();
}

QVariant Mapper::secondaryItemDisplay(QString name)
{
    if (m_model) {
        QMapIterator<int, QObject *> iterator(widgetMap);

        QHash<int, QByteArray> roleNames = m_model->roleNames();
        while (iterator.hasNext()) {
            iterator.next();
            int section = iterator.key();
            QObject *widget = iterator.value();
            Type widgetType = widgetTypeMap.value(section);
            QString roleName = QString::fromUtf8(roleNames.value(section));

            if (roleName == name) {
                QVariant secondaryValue = widgetSecondaryValueMap.value(section);
                return secondaryValue;
            }
        }
    }
    return QVariant();
}

void Mapper::setWidgetValue(int section, QVariant value)
{
    QObject *widget = widgetMap.value(section);
    Type widgetType = widgetTypeMap.value(section);
    if (!widget) {
        return;
    }
    qDebug() << "Mapper - setWidgetValue: section " << section << "value" << value << "\n-- widget" << widget << "widgetType" << widgetType;
    if (widgetType == Mapper::String) {
        if (value.isNull()) {
            value = QVariant(QStringLiteral(""));
        }
        widget->setProperty("text", value);
        widget->setProperty("altered", false);

    } else if (widgetType == Mapper::Script) {
        if (value.isNull()) {
            value = QVariant(QStringLiteral(""));
        }
        widget->setProperty("text", value);
        widget->setProperty("altered", false);
    } else if (widgetType == Mapper::Date) {
        QDate date = value.toDate();
        QLocale locale;
        QString dateFormat = locale.dateFormat(QLocale::ShortFormat);
        if (!dateFormat.contains(QStringLiteral("yyyy"))) {
            dateFormat.replace(QStringLiteral("y"), QStringLiteral("yy"));
        }
        QString dateText = date.toString(dateFormat);
        widget->setProperty("text", dateText);
        widget->setProperty("altered", false);
        //        widget->setProperty("secondaryText",QVariant());
    } else if (widgetType == Mapper::Int) {
        // si mette il toDouble() per passare 0 in caso di valore nullo altrimenti non viene rappresentato nulla
        // e lasciato il valore di una precedente assegnazione
        widget->setProperty("value", value.toInt());
        widget->setProperty("altered", false);
        //        widget->setProperty("secondaryValue",QVariant());
    } else if (widgetType == Mapper::Number) {
        qDebug() << "current spinbox value inputValue" << widget->property("inputValue") << " realValue" << widget->property("realValue");
        qDebug() << "set spinbox inputValue" << value << "toDouble" << value.toDouble();
        // si mette il toDouble() per passare 0 in caso di valore nullo altrimenti non viene rappresentato nulla
        // e lasciato il valore di una precedente assegnazione
        widget->setProperty("inputValue", value.toDouble());
        widget->setProperty("altered", false);
        //        widget->setProperty("secondaryInputValue",QVariant());
    } else if (widgetType == Mapper::Bool) {
        widget->setProperty("checked", value);
        widget->setProperty("altered", false);
    } else if (widgetType == Mapper::List) {
        QString comboDisplay = widget->property("textRole").toString();
        QVariantMap valuesMap = value.toMap();
        QVariant displayValue = valuesMap.value(comboDisplay);
        qDebug() << "comboDisplay" << comboDisplay;
        widget->setProperty("editText", displayValue);
        widget->setProperty("altered", false);
    }
}

void Mapper::valueChanged(int section)
{
    Type widgetType = widgetTypeMap.value(section);
    QObject *widget = widgetMap.value(section);
    qDebug() << "Changed on section " << section << "widgetType" << widgetType << "widget" << widget;

    if (widgetType == Mapper::String) {
        QVariant value = widget->property("text");
        widgetValueMap.insert(section, value);
    }
    if (widgetType == Mapper::Script) {
        QVariant value = widget->property("text");
        widgetValueMap.insert(section, value);
    } else if (widgetType == Mapper::Date) {
        QVariant value = widget->property("text");
        QLocale locale;
        QString dateFormat = locale.dateFormat(QLocale::ShortFormat);
        if (!dateFormat.contains(QStringLiteral("yyyy"))) {
            dateFormat.replace(QStringLiteral("y"), QStringLiteral("yy"));
        }
        QString dateText = value.toString();
        QDate date = QDate::fromString(dateText, dateFormat);
        widgetValueMap.insert(section, date.toString(Qt::ISODate));
        QVariant secondaryValue = widget->property("secondaryText");
        QString secondaryDateText = value.toString();
        QDate secondaryDate = QDate::fromString(secondaryDateText, dateFormat);
        widgetSecondaryValueMap.insert(section, secondaryDate.toString(Qt::ISODate));
    } else if (widgetType == Mapper::Int) {
        QVariant value = widget->property("value");
        widgetValueMap.insert(section, value);
        QVariant secondaryValue = widget->property("secondaryValue");
        widgetSecondaryValueMap.insert(section, secondaryValue);
    } else if (widgetType == Mapper::Number) {
        QVariant value = widget->property("inputValue");
        widgetValueMap.insert(section, value);
        QVariant secondaryValue = widget->property("secondaryInputValue");
        widgetSecondaryValueMap.insert(section, secondaryValue);
    } else if (widgetType == Mapper::Bool) {
        QVariant value = widget->property("checked");
        widgetValueMap.insert(section, value);
    } else if (widgetType == Mapper::Enum) {
        QVariant value = widget->property("currentIndex");
        widgetValueMap.insert(section, value);
    } else if (widgetType == Mapper::List) {
        QVariant value = widget->property("currentIndex");
        int currentRow = value.toInt();

        QAbstractItemModel *comboModel = widget->property("model").value<QAbstractItemModel *>();

        QVariantMap valueData;
        if (comboModel) {
            QHash<int, QByteArray> comboRoleNames = comboModel->roleNames();
            QList<int> comboRoles = comboRoleNames.keys();

            for (int i = 0; i < comboRoles.count(); i++) {
                int comboRole = comboRoles.value(i);
                QString comboRoleName = QString::fromUtf8(comboRoleNames.value(comboRole));
                QModelIndex comboModelIndex = comboModel->index(currentRow, 0);
                QVariant comboValue = comboModelIndex.data(comboRole);
                valueData.insert(comboRoleName, comboValue);
            }
        }
        widgetValueMap.insert(section, valueData);
    }
}

void Mapper::displayChanged(int section)
{
    Type widgetType = widgetTypeMap.value(section);
    QObject *widget = widgetMap.value(section);
    qDebug() << "displayChanged on section " << section << "widgetType" << widgetType << "widget" << widget;
    if (widgetType == Mapper::List) {
        QVariant value = widget->property("editText");
        QString editText = value.toString();
        if (editText.isEmpty()) {
            qDebug() << "editText is empty";

            widgetValueMap.insert(section, -1);
        }
    }
}

void Mapper::deleteItem(QObject *widget)
{
    qDebug() << "Delete item " << widget;
    int section = widgetMap.key(widget);
    widgetMap.remove(section);
    widgetTypeMap.remove(section);
    //    values in map are not cancelled because could be used by added item in mapping
}

int Mapper::currentIndex() const
{
    return m_currentIndex;
}
