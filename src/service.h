#ifndef SERVICE_H
#define SERVICE_H

#include "support.h"
#include <QCoreApplication>
#include <QDebug>
#include <QFile>
#include <QHttpPart>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QLatin1String>
#include <QMimeDatabase>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QObject>
#include <QSettings>
#include <QSignalMapper>
#include <QTimer>
#include <QUrl>
#include <QUrlQuery>
class Service : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString section READ section WRITE setSection NOTIFY sectionChanged)
    Q_PROPERTY(QString loginTokenPath READ loginTokenPath WRITE setLoginTokenPath NOTIFY loginTokenPathChanged)
    Q_PROPERTY(QString descriptionKey READ descriptionKey WRITE setDescriptionKey NOTIFY descriptionKeyChanged)

    // la cosa migliore è far generare a lui i metodi e correggerli appropriatamente
    // quando manca qualche metodo come voluto possono accadere problemi di binding.
    Q_PROPERTY(bool runningState READ runningState WRITE setRunningState NOTIFY runningStateChanged)
    Q_PROPERTY(float progressValue READ progressValue WRITE setProgressValue NOTIFY progressValueChanged)

    Q_ENUMS(Request)
    Q_ENUMS(Answer)
public:
    explicit Service(QObject *parent = nullptr);
    enum Request { None, Load };
    enum Answer { No = 0x10000, Yes = 0x4000 };
    Q_INVOKABLE bool updateData();

    Q_INVOKABLE void acquireSchema(QByteArray schemaData);

    float progressValue() const;

    bool runningState() const;

    int questionValue() const;

    QString descriptionKey() const;

    QString loginTokenPath() const;

    QString section() const;

private:
    QNetworkAccessManager *netAccessManager;
    QNetworkReply *replyTests;
    QNetworkReply *replyToken;
    QMap<QString, QNetworkReply *> replyTestMap;
    QMap<QString, QNetworkReply *> replyDataMap;
    QMap<QString, QNetworkReply *> replyAttachmentMap;
    QMap<QString, QJsonArray> testDataMap;
    QNetworkReply *replyFormsTransfer;
    QNetworkReply *replyAttachmentsTransfer;
    QSettings *settingsConfiguration;

    QMimeDatabase mimeDb;

    float m_progressValue;

    bool m_runningState;
    QList<QVariant> postAttachmentList;

    QString m_descriptionKey;

    QString m_loginTokenPath;

    QString m_section;
    Support *support;
Q_SIGNALS:
    void tokenAcquired();
    void parametersAcquired();
    void testsAcquired();
    void testTransferred();

    void progressValueChanged(float progressValue);
    void errorHappened(QString text);
    void questionRequest(QString text, QString testId, Request request);
    void runningStateChanged(bool runningState);
    void descriptionKeyChanged(QString descriptionKey);
    void loginTokenPathChanged(QString loginTokenPath);
    void sectionChanged(QString section);

public Q_SLOTS:
    void setProgressValue(float progressValue);
    void setRunningState(bool runningState);
    void cleanRunningState();
    void setDescriptionKey(QString descriptionKey);
    void setLoginTokenPath(QString loginTokenPath);
    void setSection(QString section);

private Q_SLOTS:
    void readyToken();
    void onComunicationError(QNetworkReply::NetworkError code);
    void testsStart();
};

#endif // SERVICE_H
