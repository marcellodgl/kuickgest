﻿#include "interfacedata.h"

InterfaceData::InterfaceData(QObject *parent)
    : QStandardItemModel(parent)
{
    m_runningState = false;
    // definzione del tool di gestione di rete
    netAccessManager = new QNetworkAccessManager(this);
    settingsConfiguration = new QSettings(QCoreApplication::organizationName(), QCoreApplication::applicationName());
    baseRoleNames = QHash<int, QByteArray>(this->roleNames());
    qDebug() << "InterfaceData: *********"; // baseRoleNames "<<baseRoleNames;
    m_itemsCount = 0;
    support = new Support();
    rowInserting = -1;
    rowDeleting = -1;
    rowChanged = -1;
    m_rolePrimaryKey = -1;
    m_filterColumn = 0;
    offsetFactor = 0;
    m_numScreenItems = 0;
    numItems = 0;
    minRange = 0;
    maxRange = 0;
    replyContent = 0;
    connect(this, SIGNAL(rowsInserted(QModelIndex, int, int)), this, SLOT(onRowsInserted(QModelIndex, int, int)));
    connect(this, SIGNAL(dataChanged(QModelIndex, QModelIndex, QVector<int>)), this, SLOT(onDataChanged(QModelIndex, QModelIndex, QVector<int>)));
}
// In setData the old values are incrementally inserted in savedRowData to memorize precedent value until a currentRow change. The savedRowData map used
// eventually in revert. savedRowData is a QList<QMap> that could memorize all the structure of memorized data
bool InterfaceData::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (currentRow != index.row()) {
        savedRowData.clear();
        currentRow = index.row();
    }

    if (index.column() < savedRowData.size()) {
        QMap<int, QVariant> savedItemData = savedRowData.value(index.column());
        QVariant savedValue = savedItemData.value(role);
        if (savedValue.isNull()) {
            savedItemData.insert(role, index.data(role));
            savedRowData.replace(index.column(), savedItemData);
        }
    } else {
        for (int j = savedRowData.size(); j <= index.column(); j++) {
            if (j < index.column()) {
                savedRowData.insert(j, QMap<int, QVariant>());
            } else {
                QMap<int, QVariant> savedItemData;
                QVariant savedValue = savedItemData.value(role);
                if (savedValue.isNull()) {
                    savedItemData.insert(role, index.data(role));
                    savedRowData.insert(j, savedItemData);
                }
            }
        }
    }
    qDebug() << "setData value" << value << " savedRowData " << savedRowData;
    return QStandardItemModel::setData(index, value, role);
}

void InterfaceData::setSchemaKey(QString schemaKey)
{
    qDebug() << "setSchemaKey: sckema key" << schemaKey;
    if (m_schemaKey == schemaKey)
        return;

    m_schemaKey = schemaKey;
    Q_EMIT schemaKeyChanged(m_schemaKey);
}
void InterfaceData::initRoles()
{
    // set the schemaKey, before acquired with method "acquireSchema" in service
    // parameter that could be find saved in settings and that get info on table structure
    //

    qDebug() << "m_indexesKey" << m_indexesKey << "m_schemaKey" << m_schemaKey << "m_schemaPath" << m_schemaPath;

    // azzeramento dei dati di schema
    setItemRoleNames(baseRoleNames);
    roleTypes.clear();
    roleDescriptions.clear();
    roleEnums.clear();
    roleEnabled.clear();
    if (!m_schemaPath.isEmpty()) {
        settingsConfiguration->beginGroup(m_schemaPath);
    }
    QVariant schemaValue = settingsConfiguration->value(m_schemaKey);
    if (!m_schemaPath.isEmpty()) {
        settingsConfiguration->endGroup();
    }
    // create  a json document from QVariant acquired by settings
    QJsonDocument jsonSchemaDocument = QJsonDocument::fromVariant(schemaValue);
    // in settings per even table there is all OpenAPI definition, so to catch fields needs to be considered subObject "properties"
    QJsonValue propertiesValue = parseJsonObject(jsonSchemaDocument.object(), QStringLiteral("properties"));

    QHash<int, QByteArray> roleNames = baseRoleNames;

    if (propertiesValue.isObject()) {
        QJsonObject propertiesObj = propertiesValue.toObject();
        if (propertiesObj.isEmpty()) {
            qDebug() << "setSchemaKey: propertiesObj is empty";
            //            roleNames.insert(Qt::UserRole,QString("type").toUtf8());
            roleNames.insert(Qt::UserRole, QByteArray("type"));
            roleKeys.insert(Qt::UserRole, QStringLiteral("type"));
            m_itemsCount = 1;
        } else {
            QStringList propertiesList;

            QStringList schemaKeysList;
            // catch in settings in list of keys table and respectively fields
            if (!m_indexesPath.isEmpty()) {
                settingsConfiguration->beginGroup(m_indexesPath);
                schemaKeysList = settingsConfiguration->value(m_schemaKey).toStringList();
                settingsConfiguration->endGroup();
            }
            //            QStringList schemaKeysList=settingsConfiguration->value(m_indexesKey).toStringList();

            if (schemaKeysList.isEmpty()) {
                // if non present in settings keys catch fields element without order from propertyObj
                propertiesList = propertiesObj.keys();
            } else {
                propertiesList = schemaKeysList;
            }

            for (int i = 0; i < propertiesList.count(); i++) {
                QString propertyKey = propertiesList.value(i);
                QJsonValue propertyValue = propertiesObj.value(propertyKey);
                if (propertyValue.isObject()) {
                    QJsonObject parametersObj = propertyValue.toObject();
                    qDebug() << "InterfaceData: initRoles - propertyKey " << propertyKey << "paramentersObj" << parametersObj;
                    QString titleProperty = parametersObj.value(QStringLiteral("title")).toString();
                    QString typeProperty = parametersObj.value(QStringLiteral("type")).toString();
                    QString formatProperty = parametersObj.value(QStringLiteral("format")).toString();
                    int lengthProperty = parametersObj.value(QStringLiteral("maxLength")).toInt();
                    QString descriprionPropertyText = parametersObj.value(QStringLiteral("description")).toString();
                    // i sample verranno abilitati se non contengono la chiave "Primary Key"
                    if (descriprionPropertyText.contains(QStringLiteral("Primary Key"))) {
                        roleEnabled.insert(Qt::UserRole + i, false);
                        //         m_primaryKey=propertyKey;
                        // precisa assegnazione del ruolo per primary key
                        m_rolePrimaryKey = Qt::UserRole + i;
                    }
                    //                    roleEnabled.insert(Qt::UserRole+i,!descriprionProperty.contains("Primary Key"));
                    if (descriprionPropertyText.contains(QStringLiteral("Foreign Key"))) {
                        QJsonDocument descriptionDocument = QJsonDocument::fromJson(descriprionPropertyText.toUtf8());
                        QRegularExpression rxJson(QStringLiteral("\\{(.*)\\}"));
                        if (rxJson.match(descriprionPropertyText).hasMatch()) {
                            QString descriptionJsonText = rxJson.match(descriprionPropertyText).captured(0);
                        }
                        ////Correction for compatibility to QT6
                        //                        QRegExp rxJson("\\{(.*)\\}");
                        //                        if(rxJson.indexIn(descriprionProperty) >= 0)
                        //                        {
                        //                            QString descriptionJsonText=rxJson.cap(0);
                        //                        }

                        if (descriptionDocument.isObject()) {
                            qDebug() << "setSchemaKey: descriptionDocument is valid";
                        }
                        // Sistema per individuare testo all'interno di apici ' mediante espressione regolare
                        QRegularExpression rxFKey(QStringLiteral("'([a-zA-Z0-9 ]+)'"));
                        QStringList list;
                        int pos = 0;

                        while (rxFKey.match(descriprionPropertyText, pos).hasMatch()) {
                            pos = rxFKey.match(descriprionPropertyText, pos).capturedStart();
                            list << rxFKey.match(descriprionPropertyText, pos).captured(1);
                            pos += rxFKey.match(descriprionPropertyText, pos).capturedLength();
                        }
                        ////Correction for compatibility to QT6
                        //                        QRegExp rxFKey("'([a-zA-Z0-9 ]+)'");
                        //                        QStringList list;
                        //                        int pos = 0;
                        //                        while ((pos = rxFKey.indexIn(descriprionProperty, pos)) != -1)
                        //                        {
                        //                            list << rxFKey.cap(1);
                        //                            pos += rxFKey.matchedLength();
                        //                        }
                        QString relationTable = list.value(0);
                        QString relationColumn = list.value(1);
                        roleRelationTables.insert(Qt::UserRole + i, relationTable);
                        // the relation request is used by the instantiator to add relation table.
                        Q_EMIT relationRequest(relationTable);
                        Q_EMIT foreignKeyRequest(propertyKey, relationTable, relationColumn);
                        roleRelationIndexes.insert(Qt::UserRole + i, relationColumn);
                        QJsonValue descriptionValue = parametersObj.value(QStringLiteral("description"));
                        QString extraParameterText = descriprionPropertyText.split(QStringLiteral("\n"))[0];
                        QJsonDocument extraParameterDocument = QJsonDocument::fromJson(extraParameterText.toUtf8());
                        qDebug() << "descriptionValue" << descriptionValue << "descriprionProperty" << descriprionPropertyText << "extraParameterText"
                                 << extraParameterText << "extraParameter Object" << extraParameterDocument.object();
                        QString relationDisplay;
                        if (extraParameterDocument.isObject()) {
                            QJsonObject extraParameterObject = extraParameterDocument.object();
                            if (extraParameterObject.contains(QStringLiteral("reldisplay"))) {
                                relationDisplay = extraParameterObject.value(QStringLiteral("reldisplay")).toString();
                                roleRelationDisplays.insert(Qt::UserRole + i, relationDisplay);
                            }
                        }
                        if (relationDisplay.isEmpty()) {
                            if (!m_indexesPath.isEmpty()) {
                                settingsConfiguration->beginGroup(m_indexesPath);
                                QStringList relationTableKeys = settingsConfiguration->value(relationTable).toStringList();
                                settingsConfiguration->endGroup();

                                QString relationDisplay = relationTableKeys.value(0);
                                // prende come display per il role quello del primo campo della tabella relazionata
                                // questa è prelevabile nella lista dei campi memorizzata nei settings
                                roleRelationDisplays.insert(Qt::UserRole + i, relationDisplay);
                            }
                        }

                        qDebug() << "setSchemaKey: extracted foreign" << list;
                    }

                    //                    qDebug()<<"descriptionProperty"<<descriprionProperty;
                    roleTypes.insert(Qt::UserRole + i, typeProperty);
                    roleFormats.insert(Qt::UserRole + i, formatProperty);
                    roleLengths.insert(Qt::UserRole + i, lengthProperty);
                    roleDescriptions.insert(Qt::UserRole + i, descriprionPropertyText);
                    roleKeys.insert(Qt::UserRole + i, propertyKey);
                    if (titleProperty.isNull()) {
                        roleNames.insert(Qt::UserRole + i, propertyKey.toUtf8());
                    } else {
                        roleNames.insert(Qt::UserRole + i, titleProperty.toUtf8());
                        //                        roleKeys.insert(Qt::UserRole+i,propertyKey.toUtf8());
                    }
                }
            }
            m_itemsCount = propertiesList.count();
        }
    }

    setItemRoleNames(roleNames);
    qDebug() << "setSchemaKey: fatto setitemrolenames";

    qDebug() << "setSchemaKey: rolenames after" << roleNames;
    qDebug() << "setSchemaKey: roleKeys" << roleKeys;
    qDebug() << "setSchemaKey: roleTypes" << roleTypes;
}

void InterfaceData::setContentKey(QString contentKey)
{
    if (m_contentKey == contentKey)
        return;

    m_contentKey = contentKey;
    Q_EMIT contentKeyChanged(m_contentKey);
    qDebug() << "contentKey" << contentKey;
}

bool InterfaceData::submitRow(int row)
{
    qDebug() << "submit Row" << row << "rowInserting" << rowInserting;
    //    No savedRowData means that no precentent field has been memorized in setData so no new items are arrived to transfer
    if (savedRowData.isEmpty())
        return true;
    QStandardItem *rowItem = item(row);
    /****
      int idRole=Qt::UserRole-1;
      */

    if (rowItem) {
        // if(m_primaryKey.isEmpty())
        if (m_rolePrimaryKey == -1) {
            return 0;
            /******
             *
             * Aggiungere messaggio di errore
             */
        }
        QList<int> roleList = itemsRoleList();
        QVariantMap itemMap;
        QJsonObject itemJsonObj;
        QString primaryKeyText;

        for (int i = 0; i < roleList.count(); i++) {
            int role = roleList.value(i);

            QString roleName = itemName(role);

            QString roleKey = itemKey(role);
            QVariant roleValue = rowItem->data(role);
            QString roleRelationTable = itemRelationTable(role);
            QJsonValue roleJsonValue = QJsonValue::fromVariant(roleValue);
            qDebug() << "roleValue" << roleValue << "roleJsonValue" << roleJsonValue;
            //            if(roleName==m_primaryKey)
            if (role == m_rolePrimaryKey) {
                // caso di insserimeno, la primary key non va considerata
                if (rowInserting == -1) {
                    primaryKeyText = roleValue.toString();
                    itemJsonObj.insert(roleKey, roleJsonValue);
                }
            } else {
                if (roleRelationTable.isEmpty()) {
                    itemJsonObj.insert(roleKey, roleJsonValue);
                } else {
                    QVariantMap roleRelValues = roleValue.toMap();
                    QString roleRelationIndex = itemRelationIndex(role);
                    QVariant roleRelIndexValue = roleRelValues.value(roleRelationIndex);
                    QJsonValue roleRelIndexJsonValue = QJsonValue::fromVariant(roleRelIndexValue);
                    itemJsonObj.insert(roleKey, roleRelIndexJsonValue);
                }
                //-                itemJsonObj.insert(roleKey,roleJsonValue);
            }
            qDebug() << "role" << role << "roleName" << itemName(role) << "value" << rowItem->data(role) << "primaryKeyText" << primaryKeyText;
        }
        QJsonDocument itemJsonDoc = QJsonDocument(itemJsonObj);
        QByteArray itemJsonData = itemJsonDoc.toJson();
        qDebug() << "itemJsonData" << itemJsonData;

        QString serverHost = settingsConfiguration->value(QStringLiteral("server")).toString();
        QString loginToken = settingsConfiguration->value(QStringLiteral("token")).toString();
        QUrl urlSubmit(serverHost);
        urlSubmit.setPath(m_contentPath);
        QNetworkRequest requestSubmit;
        if (rowInserting == -1) {
            if (rowChanged >= 0) {
                QUrlQuery urlQuery;
                QString primaryKey = itemName(m_rolePrimaryKey);
                //                urlQuery.addQueryItem(m_primaryKey,"eq."+primaryKeyText);
                urlQuery.addQueryItem(primaryKey, QString::fromLatin1("eq.%1").arg(primaryKeyText));
                urlSubmit.setQuery(urlQuery);
                qDebug() << "urlsubmit" << urlSubmit.toString();

                requestSubmit.setUrl(urlSubmit);
                requestSubmit.setRawHeader("Authorization", QString::fromLatin1("Bearer %1").arg(loginToken).toUtf8());
                qDebug() << "urlContent " << urlSubmit.toString();
                requestSubmit.setHeader(QNetworkRequest::ContentTypeHeader, QVariant(QStringLiteral("application/json")));
                replyPatch = netAccessManager->sendCustomRequest(requestSubmit, "PATCH", itemJsonData);
                setRunningState(true);
                connect(replyPatch, SIGNAL(finished()), this, SLOT(readyPatch()));
                connect(replyPatch, SIGNAL(downloadProgress(qint64, qint64)), this, SLOT(progressUpdate(qint64, qint64)));
                setProgressValue(0);
            }
        } else {
            requestSubmit.setUrl(urlSubmit);
            requestSubmit.setRawHeader("Authorization", QString::fromLatin1("Bearer %1").arg(loginToken).toUtf8());

            requestSubmit.setRawHeader("Accept", QString(QString::fromLatin1("application/vnd.pgrst.object+json")).toUtf8());
            requestSubmit.setRawHeader("Prefer", QString(QString::fromLatin1("return=representation")).toUtf8());

            qDebug() << "urlContent " << urlSubmit.toString();
            requestSubmit.setHeader(QNetworkRequest::ContentTypeHeader, QVariant(QStringLiteral("application/json")));
            replyPost = netAccessManager->post(requestSubmit, itemJsonData);

            setRunningState(true);
            connect(replyPost, SIGNAL(finished()), this, SLOT(readyPost()));
            connect(replyPost, SIGNAL(downloadProgress(qint64, qint64)), this, SLOT(progressUpdate(qint64, qint64)));
            setProgressValue(0);

            // soluzione alternativa per il tracciamento dei post è quello di creare sul momento l'oggetto
            // replyPost come new e usare un mapper per identificarlo (soluzione adottata nel service riga 400 ca)
        }
        return true;
    }
    return false;
}

void InterfaceData::setIndexesKey(QString indexesKey)
{
    if (m_indexesKey == indexesKey)
        return;

    m_indexesKey = indexesKey;
    Q_EMIT indexesKeyChanged(m_indexesKey);
}

void InterfaceData::setContentPath(QString contentPath)
{
    if (m_contentPath == contentPath)
        return;

    m_contentPath = contentPath;
    Q_EMIT contentPathChanged(m_contentPath);
}

void InterfaceData::setSection(QString section)
{
    if (m_section == section)
        return;

    m_section = section;
    Q_EMIT sectionChanged(m_section);
}

void InterfaceData::setContentQuery(QString contentQuery)
{
    if (m_contentQuery == contentQuery)
        return;

    m_contentQuery = contentQuery;
    Q_EMIT contentQueryChanged(m_contentQuery);
}

// metodo che impota il contenuto di tutta una query come dati
void InterfaceData::setContent(QByteArray content)
{
    if (m_content == content)
        return;

    m_content = content;
    Q_EMIT contentChanged(m_content);
}

void InterfaceData::setSchemaPath(QString schemaPath)
{
    QFileInfo schemaFileInfo = QFileInfo(schemaPath);
    if (schemaFileInfo.isFile()) {
        QFile file(schemaPath);
        qDebug() << "LocalFile schema" << schemaPath;
        if (!file.open(QIODevice::ReadOnly)) {
            Q_EMIT errorHappened(tr("Error on Schema url") + schemaPath);
            return;
        }
        QByteArray schemaData = file.readAll();

        QJsonDocument jsonTestsSchemaDoc = QJsonDocument::fromJson(schemaData);

        schemaPath = schemaFileInfo.baseName();
        QVariant testsSchemaValue = jsonTestsSchemaDoc.toVariant();
        settingsConfiguration->beginGroup(schemaPath);
        if (!m_schemaKey.isEmpty())
            settingsConfiguration->setValue(m_schemaKey, testsSchemaValue);
        settingsConfiguration->endGroup();
    }

    if (m_schemaPath == schemaPath)
        return;

    m_schemaPath = schemaPath;
    Q_EMIT schemaPathChanged(m_schemaPath);
}

void InterfaceData::setIndexesPath(QString indexesPath)
{
    if (m_indexesPath == indexesPath)
        return;

    m_indexesPath = indexesPath;
    Q_EMIT indexesPathChanged(m_indexesPath);
}

bool InterfaceData::runningState() const
{
    return m_runningState;
}

// probabilmente funzione che si rivela inutile , il formato dei campi viene interrogato direttamente
// tramite il metodo parsejsonObj(...)
/*
void InterfaceData::elaborateSchema(QByteArray data)
{
    qDebug()<<"Schema Data"<<data;
    QJsonDocument jsonDoc=QJsonDocument::fromJson(data);
    qDebug()<<"jsonDoc is Empty"<<jsonDoc.isEmpty()<<"is null"<<jsonDoc.isNull();
    QJsonObject jsonObj=jsonDoc.object();
    if(!jsonObj.isEmpty())
    {
        QJsonValue jsonDataValue=jsonObj.value("data");
        if(jsonDataValue.isObject())
        {
            QJsonObject jsonDataObj=jsonDataValue.toObject();
            QJsonValue jsonTypeValue=jsonDataObj.value("type");
            QString dataType=jsonTypeValue.toString();
            if(dataType=="array")
            {
                QJsonValue jsonItemsValue=jsonDataObj.value("items");
                QJsonObject jsonItemsObj=jsonItemsValue.toObject();
                QJsonValue jsonPropertiesValue=jsonItemsObj.value("properties");
                QJsonObject jsonPropertiesObj=jsonPropertiesValue.toObject();
                qDebug()<<"jsonPropertiesObj"<<jsonPropertiesObj;

            }

        }

    }
}
*/

QJsonValue InterfaceData::parseJsonObject(QJsonObject jsonObj, QString key)
{
    QJsonObject::iterator jsonIterator = jsonObj.begin();
    while (jsonIterator != jsonObj.end()) {
        QJsonValue jsonValue = jsonIterator.value();
        if (key == jsonIterator.key()) {
            return jsonValue;

        } else {
            //            qDebug()<<"value"<<jsonValue<<"isObject"<<jsonValue.isObject();
            /***QJsonValue  subObjValue=parseJsonObject(jsonValue.toObject(),key);
             */
            QJsonValue subObjValue = support->parseJsonObject(jsonValue.toObject(), key);
            if (!subObjValue.isNull()) {
                return subObjValue;
            }
        }
        jsonIterator++;
    }
    return QJsonValue();
}

// restituisce la lista dei ruoli a partire da Qt::UserRole
QList<int> InterfaceData::itemsRoleList()
{
    QList<int> itemsRoleList;
    for (int i = 0; i < m_itemsCount; i++) {
        itemsRoleList.append(Qt::UserRole + i);
    }
    qDebug() << "itemRoleList" << itemsRoleList << "m_itemsCount" << m_itemsCount;
    return itemsRoleList;
}

QString InterfaceData::itemName(int role)
{
    QHash<int, QByteArray> itemRoleNames = roleNames();

    QString nameItem = QString::fromUtf8(itemRoleNames.value(role));
    return nameItem;
}

// funzione che discrimina il tipo di uscita
// nel caso delle relazioni analizza gli schemi per individuare il tipo esatto
QString InterfaceData::itemType(int role)
{
    QString typeItem = roleTypes.value(role);
    return typeItem;
    /*####
    QString roleRelationTable=itemRelationTable(role);

    if(roleRelationTable.isEmpty())
    {
        QString typeItem=roleTypes.value(role);
        return typeItem;
    }
    else
    {
        if(!m_schemaPath.isEmpty())
        {
            settingsConfiguration->beginGroup(m_schemaPath);
        }
        QVariant relSchemaValue=settingsConfiguration->value(roleRelationTable);

        if(!m_schemaPath.isEmpty())
        {
            settingsConfiguration->endGroup();
        }
        QJsonDocument jsonRelSchemaDocument=QJsonDocument::fromVariant(relSchemaValue);

        QJsonValue relPropertiesValue=parseJsonObject(jsonRelSchemaDocument.object(),"properties");

        if(relPropertiesValue.isObject())
        {
            QJsonObject relPropertiesObj=relPropertiesValue.toObject();
            QString displayRole=itemRelationDisplay(role);
            QJsonValue relDisplayPropertiesValue=relPropertiesObj.value(displayRole);
            if(relDisplayPropertiesValue.isObject())
            {
                QJsonObject relDisplayPropertiesObj=relDisplayPropertiesValue.toObject();

                QString relDisplayType=relDisplayPropertiesObj.value("type").toString();
//                qDebug()<<"role"<<role<<" on "<<itemName(role)<<"relTable"<<roleRelationTable<<"displayRole"<<displayRole<<"relDisplayType"<<relDisplayType;
                return relDisplayType;
            }


            else
            {
                return "string";
            }
        }
        else
        {
            return "string";
        }
    }

*/
}

QString InterfaceData::itemFormat(int role)
{
    QString formatItem = roleFormats.value(role);
    return formatItem;
}

int InterfaceData::itemLength(int role)
{
    int lengthItem = roleLengths.value(role);
    return lengthItem;
}

QString InterfaceData::itemDescription(int role)
{
    QString descriptionItem = roleDescriptions.value(role);
    return descriptionItem;
}

QString InterfaceData::itemRelationTable(int role)
{
    QString relationTableItem = roleRelationTables.value(role);
    return relationTableItem;
}

QString InterfaceData::itemRelationIndex(int role)
{
    QString relationIndexItem = roleRelationIndexes.value(role);
    return relationIndexItem;
}

QString InterfaceData::itemRelationDisplay(int role)
{
    QString relationDisplayItem = roleRelationDisplays.value(role);
    return relationDisplayItem;
}

QVariantMap InterfaceData::itemConstraint(int role)
{
    QVariantMap constraintItem = roleConstraints.value(role);
    return constraintItem;
}

bool InterfaceData::itemEnabled(int role)
{
    bool enabledItem = roleEnabled.value(role, true);
    return enabledItem;
}

QString InterfaceData::itemKey(int role)
{
    QString keyItem = roleKeys.value(role);
    return keyItem;
}

QVariantList InterfaceData::itemEnum(int role)
{
    QVariantList enumItem = roleEnums.value(role);
    return enumItem;
}

void InterfaceData::saveIndexes()
{
    QStringList idList;
    int idRole = Qt::UserRole - 1;
    for (int i = 0; i < rowCount(); i++) {
        QStandardItem *stdItem = item(i);
        QVariant idValue = stdItem->data(idRole);
        idList.append(idValue.toString());
    }
    settingsConfiguration->setValue(m_indexesKey, idList);
}

QString InterfaceData::getSubTableName(QJsonValue subTableValue)
{
    QString subTableName = subTableValue.toString();
    subTableName.truncate(subTableName.indexOf(QStringLiteral("_")));
    return subTableName;
}

QString InterfaceData::getSubTableFKey(QJsonValue subTableValue)
{
    QString subTableFKey = subTableValue.toString();
    subTableFKey.remove(0, subTableFKey.indexOf(QStringLiteral("_")) + 1);
    subTableFKey.truncate(subTableFKey.lastIndexOf(QStringLiteral("_")));
    return subTableFKey;
}
// method used to load settings and data for tests page
void InterfaceData::readyContent()
{
    QByteArray replyContentReceived = replyContent->readAll();
    QJsonDocument jsonContentDocument = QJsonDocument::fromJson(replyContentReceived);

    qDebug() << "readyContent: reply on " << replyContent->url(); //<<replyContentReceived;
    if (replyContent->error()) {
        QJsonObject jsonContentObj = jsonContentDocument.object();
        QJsonValue contentMessageValue = jsonContentObj.value(QStringLiteral("message"));
        QString contentMessage = contentMessageValue.toString();
        qDebug() << replyContent->error() << " text " << replyContent->errorString() << "message " << contentMessage;
        QString errorMessage = QString::fromLatin1("%1\n%2").arg(replyContent->errorString()).arg(contentMessage);
        setRunningState(false);
        Q_EMIT errorHappened(errorMessage);

        Q_EMIT loadCompleted();
        return;
    }
    // azione utile a quando si lancia last e il riempimento deve ripartire da zero con una lista vuota
    // oppure quando non sono definite finestre e pertanto al caricamento la lista parte sempre caricarsi da vuota
    if (!m_numScreenItems || !offsetFactor) {
        clear();
    }
    //    clear();
    // realizzare discorso section con un while -> ok utilizzato valore unico nel parser

    QByteArray contentRangeData = replyContent->rawHeader("Content-Range");
    qDebug() << "readyContent: contentRange" << contentRangeData;
    QByteArray rangeItems = contentRangeData.split('/').value(0);
    QByteArray downRange = rangeItems.split('-').value(0);
    if (downRange.toInt() < minRange) {
        minRange = downRange.toInt();
    }
    QByteArray upRange = rangeItems.split('-').value(1);

    maxRange = upRange.toInt();

    QByteArray numItemsData = contentRangeData.split('/').value(1);
    numItems = numItemsData.toInt();
    QString contentRange = QString::fromLatin1("%1-%2/%3").arg(minRange).arg(maxRange).arg(numItems);
    setContentRange(contentRange);

    if (!m_section.isEmpty()) {
        /***sectionValue=parseJsonObject(jsonContentDocument.object(),m_section);
         */
        QJsonValue sectionValue = support->parseJsonObject(jsonContentDocument.object(), m_section);

        jsonContentDocument = QJsonDocument::fromVariant(sectionValue.toVariant());
    }

    // setContent has effect on signal contentChanged used by drawer model that will load through acquireSchema bringing with itself data received here.
    setContent(replyContentReceived);
    // after it will continue to run and in case of drawer it will have an object with keys filled by the name of fields
    QList<int> roleList = itemsRoleList();
    // Case of object is usually used for loading schemaData
    if (jsonContentDocument.isObject()) {
        QJsonObject contentObject = jsonContentDocument.object();
        QStringList contentKeys = contentObject.keys();
        qDebug() << "readyContent: --- content keys " << contentKeys << roleList;

        for (int j = 0; j < contentKeys.count(); j++) {
            QString contentKey = contentKeys.value(j);
            //            if(contentKey=="anagrafica" || contentKey=="anagraficaview")
            //            {
            QJsonValue itemValue = contentObject.value(contentKey);
            bool visibility = true;
            if (itemValue.isObject()) {
                QJsonObject itemObj = itemValue.toObject();
                QString descriptionItem = itemObj.value(QStringLiteral("description")).toString();

                QJsonDocument descriptionDocument = QJsonDocument::fromJson(descriptionItem.toUtf8());

                                qDebug()<<"descriptionDocument"<<descriptionDocument;
                if (descriptionDocument.isObject()) {
                    QJsonObject descriptionObj = descriptionDocument.object();
                    QJsonValue visibilityValue = descriptionObj.value(QStringLiteral("visibility"));
                    //                    qDebug()<<"visibilityValue"<<visibilityValue;
                    visibility = visibilityValue.toBool(true);
                    QJsonValue subTableValue = descriptionObj.value(QStringLiteral("subtable"));
                    QJsonValue referencesTableValue = descriptionObj.value(QStringLiteral("references"));
                    if (!subTableValue.isUndefined()) {
                        if (subTableValue.isString()) {
                            QString subTableName = getSubTableName(subTableValue);
                            QString subTableFKey = getSubTableFKey(subTableValue);
                            QString referenceTable = referencesTableValue.toString();
                            qDebug() << "readyContent: subtableValue" << subTableValue << "subTableName" << subTableName << "subTableFKey" << subTableFKey
                                     << "referenceTable" << referenceTable;
                            Q_EMIT subTableRequest(subTableName, subTableFKey, referenceTable);
//                            Q_EMIT subTableRequest(subTableName, subTableFKey, referenceTable, referenceIndex);
                        } else if (subTableValue.isArray()) {
                            QJsonArray subTableJsonArray = subTableValue.toArray();
                            QJsonArray::iterator subTableJsonIterator = subTableJsonArray.begin();
                            QJsonArray referencesTableJsonArray = referencesTableValue.toArray();
                            QJsonArray::iterator referencesTableJsonIterator = referencesTableJsonArray.begin();
                            while (subTableJsonIterator != subTableJsonArray.end()) {
                                QJsonValue subTableValue = *subTableJsonIterator;
                                QString subTableName = getSubTableName(subTableValue);
                                QString subTableFKey = getSubTableFKey(subTableValue);
                                QJsonValue referenceTableValue = *referencesTableJsonIterator;
                                QString referenceTable = referenceTableValue.toString();
                                qDebug() << "readyContent: subtableValue" << subTableValue << "subTableName" << subTableName << "subTableFKey" << subTableFKey
                                         << "referenceTable" << referenceTable;
                                Q_EMIT subTableRequest(subTableName, subTableFKey, referenceTable);
                                subTableJsonIterator++;
                                referencesTableJsonIterator++;
                            }
                        }
                    }
                }
            }
            qDebug() << "readyContent: contentkey" << contentKey << "visibility" << visibility;
            // prende elementi di cui dare rappresentazione nel drawer solo se visibili.
            if (visibility) {
                QStandardItem *item = new QStandardItem();
                for (int i = 0; i < roleList.count(); i++) {
                    int role = roleList.value(i);
                    QString roleName = itemName(role);
                    QString roleKey = itemKey(role);
                    //                    QString roleType=itemType(role);
                    qDebug() << "----role" << role << " roleName " << roleName << " rolekey " << roleKey;
                    // put a value in the model in position of parameter role "type" the key of fields
                    QVariant roleValue = QVariant(contentKey);
                    item->setData(roleValue, role);
                }
                appendRow(item);
            }
        }
    }
    // case to load data to present to test page through model
    else if (jsonContentDocument.isArray()) {
        qDebug() << "InterfaceData: readyContent: replycontenturl" << replyContent->url().query();
        QUrl replyContentUrl = replyContent->url();
        QUrlQuery replyContentQuery = QUrlQuery(replyContentUrl.query());
        QString replyContentOrder = replyContentQuery.queryItemValue(QStringLiteral("order"));
        QJsonArray contentArray = jsonContentDocument.array();
        for (int i = 0; i < contentArray.count(); i++) {
            QJsonValue contentValue = contentArray.at(i);
            if (contentValue.isObject()) {
                QJsonObject contentObject = contentValue.toObject();

                QStandardItem *item = new QStandardItem();
                for (int j = 0; j < roleList.count(); j++) {
                    int role = roleList.value(j);
                    QString roleName = itemName(role);
                    QString roleKey = itemKey(role);
                    QString roleType = itemType(role);
                    QJsonValue roleJsonValue = contentObject.value(roleKey);
                    QString relationTable = itemRelationTable(role);
                    QVariant roleValue = roleJsonValue.toVariant();
                    //                    qDebug()<<"setData
                    //                    role"<<role<<"roleName"<<roleName<<"roleType"<<roleType<<"roleKey"<<roleKey<<"contentObject"<<contentObject;
                    //                    qDebug()<<"InterfaceData: readyContent - roleName"<<roleName<<"roleType"<<roleType<<"contentObject"<<contentObject;
                    item->setData(roleValue, role);
                    if (relationTable.isEmpty()) {
                        QVariant roleValue = roleJsonValue.toVariant();
                        item->setData(roleValue, role);
                    } else {
                        QString relationDisplay = itemRelationDisplay(role);
                        QString relationIndex = itemRelationIndex(role);
                        QJsonValue relTableJsonValue = contentObject.value(relationTable);
                        if (relTableJsonValue.isObject()) {
                            QJsonObject relTableObj = relTableJsonValue.toObject();

                            QVariantMap relValuesMap = relTableObj.toVariantMap();
                            item->setData(relValuesMap, role);
                        }
                    }
                }
                //                Gli elementi vengono messi sempre in cima per rendere disponibile lo swipe dall'alto che vale indipendentemente dall'ordine.
                insertRow(0, item);
            }
        }
    }
    setRunningState(false);
    Q_EMIT loadCompleted();

    rowInserting = -1;
}

void InterfaceData::readyPatch()
{
    qDebug() << "ready Patch savedRowData ";
    qDebug() << savedRowData;
    qDebug() << "ready Patch savedRowData 1";
    QByteArray replyPatchReceived = replyPatch->readAll();
    qDebug() << "replyPatchReceived" << replyPatchReceived;
    QJsonDocument jsonPatchDocument = QJsonDocument::fromJson(replyPatchReceived);
    if (replyPatch->error()) {
        QJsonObject jsonPatchObj = jsonPatchDocument.object();
        QJsonValue patchMessageValue = jsonPatchObj.value(QStringLiteral("message"));
        QString patchMessage = patchMessageValue.toString();
        qDebug() << replyPatch->error() << " text " << replyPatch->errorString() << "message " << patchMessage;
        QString errorMessage = QString::fromLatin1("%1\n%2").arg(patchMessage).arg(replyPatch->errorString());
        setRunningState(false);
        Q_EMIT errorHappened(errorMessage);
        return;
    }
    setRunningState(false);
    savedRowData.clear();
    rowChanged = -1;
    Q_EMIT patchCompleted();
}

void InterfaceData::readyPost()
{
    QByteArray replyPostReceived = replyPost->readAll();
    qDebug() << "replyPostReceived" << replyPostReceived;
    QJsonDocument jsonPostDocument = QJsonDocument::fromJson(replyPostReceived);

    if (replyPost->error()) {
        QJsonObject jsonPostObj = jsonPostDocument.object();
        QJsonValue postMessageValue = jsonPostObj.value(QStringLiteral("message"));
        QString postMessage = postMessageValue.toString();
        qDebug() << replyPost->error() << " text " << replyPost->errorString() << "message " << postMessage;
        QString errorMessage = QString::fromLatin1("%1\n%2").arg(postMessage).arg(replyPost->errorString());
        setRunningState(false);
        Q_EMIT errorHappened(errorMessage);

        rowInserting = -1;
        return;
    }
    if (jsonPostDocument.isObject()) {
        QString primaryKey = itemName(m_rolePrimaryKey);

        QJsonObject postObject = jsonPostDocument.object();
        // passaggio che serve ad impostare il valore della primary key ricevuto indietro in un oggetto completo
        // all'interno del corrente modello
        QJsonValue primaryKeyJsonValue = postObject.value(primaryKey);
        QVariant primaryKeyValue = primaryKeyJsonValue.toVariant();
        //        qDebug()<<"rolePrimaryKey"<<rolePrimaryKey<<"primaryKey"<<primaryKey<<"primaryKeyJsonValue"<<primaryKeyJsonValue<<"rowInserting"<<rowInserting;
        //        Nuova sintassi sperimentale che richiama l'elemento della stessa classe corrente (al pari di this)
        QModelIndex indexPrimaryKey = QStandardItemModel::index(rowInserting, 0);
        qDebug() << "indexPrimaryKey" << indexPrimaryKey;
        setData(indexPrimaryKey, primaryKeyValue, m_rolePrimaryKey);
    }
    savedRowData.clear();
    rowInserting = -1;
    rowChanged = -1;
    setRunningState(false);
    Q_EMIT postCompleted();
}

void InterfaceData::readyDelete()
{
    QByteArray replyDeleteReceived = replyDelete->readAll();

    qDebug() << "replyDeleteReceived" << replyDeleteReceived;
    QJsonDocument jsonDeleteDocument = QJsonDocument::fromJson(replyDeleteReceived);

    if (replyDelete->error()) {
        QJsonObject jsonPostObj = jsonDeleteDocument.object();
        QJsonValue deleteMessageValue = jsonPostObj.value(QStringLiteral("message"));
        QString deleteMessage = deleteMessageValue.toString();
        qDebug() << replyDelete->error() << " text " << replyDelete->errorString() << "message " << deleteMessage;
        QString errorMessage = QString::fromLatin1("%1\n%2").arg(deleteMessage).arg(replyDelete->errorString());
        setRunningState(false);
        Q_EMIT errorHappened(errorMessage);

        return;
    }
    removeRow(rowDeleting);
    setRunningState(false);
    rowDeleting = -1;
    qDebug() << "Delete url " << replyDelete->url();
}

void InterfaceData::progressUpdate(qint64 value, qint64 maximum)
{
    if (maximum == -1) {
        setProgressValue(1);
        return;
    }
    if (maximum) {
        float progressValue = (float)value / maximum;
        setProgressValue((float)value / maximum);
        qDebug() << "value" << value << "maximum" << maximum << "progressValue" << progressValue;
    }
}

bool InterfaceData::goNext()
{
    qDebug() << "replyContent is running" << replyContent->isRunning();
    if (replyContent->isRunning()) {
        return false;
    }
    if (!m_numScreenItems) {
        return false;
    }
    if (numItems > 0 && m_numScreenItems * (offsetFactor + 1) >= numItems) {
        return false;
    }
    offsetFactor++;
    return true;
}

void InterfaceData::goLast()
{
    // resetta anche la query di una possibile precedente ricerca
    //    m_contentQuery.clear();
    numItems = 0;
    offsetFactor = 0;
}

void InterfaceData::setNumScreenItems(int numScreenItems)
{
    if (m_numScreenItems == numScreenItems)
        return;

    m_numScreenItems = numScreenItems;
    Q_EMIT numScreenItemsChanged(m_numScreenItems);
}

void InterfaceData::searchItems(QVariantMap valueMap, QVariantMap secondaryValueMap, QVariantMap operatorMap)
{
    qDebug() << "valueMap" << valueMap << "\noperatorMap" << operatorMap;
    //    m_contentQuery="order=id.desc&città=fts.caserta";

    QMapIterator<QString, QVariant> valueIterator(valueMap);
    QUrlQuery urlQuery;
    while (valueIterator.hasNext()) {
        valueIterator.next();
        QString searchKey = valueIterator.key();
        QVariant searchValue = valueIterator.value();
        QString searchText = searchValue.toString();
        QVariant searchOperatorValue = operatorMap.value(searchKey);
        QString searchOperator = searchOperatorValue.toString();

        // verifica serchText per quando il campo non è stato riempito, searchOperator per quando il campo non è abilitato
        //        if(!searchText.isEmpty() && !searchOperator.isEmpty())
        if (!searchOperator.isEmpty()) {
            if (searchOperator == QStringLiteral("=")) {
                urlQuery.addQueryItem(searchKey, QString::fromLatin1("eq. %1").arg(searchText));
            } else if (searchOperator == QStringLiteral("~")) {
                urlQuery.addQueryItem(searchKey, QString::fromLatin1("ilike.*%1*").arg(searchText));
            } else if (searchOperator == QStringLiteral("≥")) {
                urlQuery.addQueryItem(searchKey, QString::fromLatin1("gte.%1").arg(searchText));
            } else if (searchOperator == QStringLiteral("≤")) {
                urlQuery.addQueryItem(searchKey, QString::fromLatin1("lte.%1").arg(searchText));
            } else if (searchOperator == QStringLiteral("÷")) {
                QVariant searchSecondaryValue = secondaryValueMap.value(searchKey);
                QString searchSecondaryText = searchSecondaryValue.toString();
                urlQuery.addQueryItem(searchKey, QString::fromLatin1("gte.%1").arg(searchText));
                urlQuery.addQueryItem(searchKey, QString::fromLatin1("lte.%1").arg(searchSecondaryText));

            } else if (searchOperator == QStringLiteral("Ø")) {
                urlQuery.addQueryItem(searchKey, QString::fromLatin1("is.null"));
            } else {
                urlQuery.addQueryItem(searchKey, QString::fromLatin1("eq.%1").arg(searchText));
            }
        }
    }

    goLast();
    //    il settings della quey va fatto dopo altrimenti verrebbe resettata da goLast()
    //    m_contentQuery=urlQuery.toString();
    setContentQuery(urlQuery.toString());
    // bisogna usare setContentQuery e non direttamente assegnazione per fare in modo che sia emesso segnale cche sia di notifica
    // per fare aggiornare la voce nel pageMain
    qDebug() << "m_contentQuery " << m_contentQuery;
    if (!urlQuery.isEmpty()) {
        loadContents();
    }
}

void InterfaceData::setContentOrder(QString contentOrder)
{
    if (m_contentOrder == contentOrder)
        return;

    m_contentOrder = contentOrder;
    Q_EMIT contentOrderChanged(m_contentOrder);
}

QVariantMap InterfaceData::indexValues(const QModelIndex &index) const
{
    QVariantMap valueMap;
    QHash<int, QByteArray> itemRoleNames = roleNames();
    QHashIterator<int, QByteArray> roleNamesIterator(itemRoleNames);
    while (roleNamesIterator.hasNext()) {
        roleNamesIterator.next();
        int role = roleNamesIterator.key();
        QString roleName = QString::fromLatin1(roleNamesIterator.value());
        QVariant value = index.data(role);
        valueMap.insert(roleName, value);
    }
    return valueMap;
}

void InterfaceData::revert()
{
    qDebug() << "revert with savedRowData" << savedRowData;
    // verifica effettuata anche su runningState, in quando quando sta operando ancora nn può effettuare il revert
    // ma viene preservato il saveRowData quindi lanciabile ad un revert successivo
    if (savedRowData.isEmpty() || runningState()) {
        return;
    }
    for (int j = 0; j < savedRowData.count(); j++) {
        QMap<int, QVariant> savedItemData = savedRowData.value(j);
        QMapIterator<int, QVariant> savedItemIterator(savedItemData);
        QModelIndex modelIndex = index(currentRow, j);
        while (savedItemIterator.hasNext()) {
            savedItemIterator.next();
            int role = savedItemIterator.key();
            QVariant savedValue = savedItemIterator.value();
            setData(modelIndex, savedValue, role);
        }
    }
    savedRowData.clear();
}

void InterfaceData::setRunningState(bool runningState)
{
    qDebug() << "InterfaceData: runningState" << runningState;
    if (m_runningState == runningState)
        return;

    m_runningState = runningState;
    Q_EMIT runningStateChanged(m_runningState);
}

void InterfaceData::setProgressValue(float progressValue)
{
    qWarning("Floating point comparison needs context sanity check");
    if (qFuzzyCompare(m_progressValue, progressValue))
        return;

    m_progressValue = progressValue;
    Q_EMIT progressValueChanged(m_progressValue);
}

void InterfaceData::setContentRange(QString contentRange)
{
    qDebug() << "InterfaceData: setContentRange" << contentRange << "- m_contentRange" << m_contentRange;
    if (m_contentRange == contentRange)
        return;

    m_contentRange = contentRange;
    Q_EMIT contentRangeChanged(m_contentRange);
}

void InterfaceData::abortContent()
{
    if (replyContent) {
        qDebug() << "abortContent" << replyContent;
        replyContent->abort();
    }
}

void InterfaceData::clean()
{
    clear();
}

void InterfaceData::onRowsInserted(const QModelIndex &parent, int first, int last)
{
    qDebug() << "row inserted first" << first << "last" << last << "parent" << parent;
    rowInserting = first;
}

void InterfaceData::onDataChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight, const QVector<int> &roles)
{
    qDebug() << "data changed row topLeft " << topLeft.row() << "bottomRight" << bottomRight.row() << "roles" << roles;
    qDebug() << "data changed column topLeft" << topLeft.column() << "bottomRight" << bottomRight.column() << "roles" << roles;
    rowChanged = topLeft.row();
}

void InterfaceData::add()
{
    int rowModel = rowCount();
    if (roleTypes.isEmpty())
        return;
    QStandardItem *item = new QStandardItem();

    QTime time = QTime::currentTime();
    srand((uint)time.msec());

    QString id = QString::number(rand());
    int idRole = Qt::UserRole - 1;
    item->setData(id, idRole);

    QHashIterator<int, QString> roleTypesIterator(roleTypes);
    while (roleTypesIterator.hasNext()) {
        roleTypesIterator.next();
        int role = roleTypesIterator.key();
        QString roleType = roleTypesIterator.value();
        qDebug() << "add role" << role << "roletype" << roleType << "relationTable" << itemRelationTable(role) << "itemdata" << item->data(role);
    }

    qDebug() << "rowModel" << rowModel;
    appendRow(item);
    saveIndexes();
}

void InterfaceData::remove(int index)
{
    QStandardItem *rowItem = item(index);
    qDebug() << "InterfaceData: remove - m_rolePrimaryKey" << m_rolePrimaryKey << "rowItem" << rowItem << "rowCount" << rowCount() << "index" << index;
    if (rowItem) {
        if (m_rolePrimaryKey == -1) {
            return;
            /******
             *
             * Aggiungere messaggio di errore
             */
        }
        QModelIndex rowIndex = rowItem->index();
        QVariant primaryKeyValue = rowIndex.data(m_rolePrimaryKey);
        QString primaryKeyText = primaryKeyValue.toString();
        QString serverHost = settingsConfiguration->value(QStringLiteral("server")).toString();
        QString loginToken = settingsConfiguration->value(QStringLiteral("token")).toString();
        QUrl urlSubmit(serverHost);
        urlSubmit.setPath(m_contentPath);
        QNetworkRequest requestSubmit;
        QUrlQuery urlQuery;
        QString primaryKey = itemName(m_rolePrimaryKey);
        //        urlQuery.addQueryItem(m_primaryKey,"eq."+primaryKeyText);
        urlQuery.addQueryItem(primaryKey, QString::fromLatin1("eq.%1").arg(primaryKeyText));
        urlSubmit.setQuery(urlQuery);
        qDebug() << "urlsubmit" << urlSubmit.toString();

        requestSubmit.setUrl(urlSubmit);
        requestSubmit.setRawHeader("Authorization", QString::fromLatin1("Bearer %1").arg(loginToken).toUtf8());
        rowDeleting = index;
        replyDelete = netAccessManager->sendCustomRequest(requestSubmit, "DELETE", QByteArray());
        setRunningState(true);
        connect(replyDelete, SIGNAL(finished()), this, SLOT(readyDelete()));
        connect(replyDelete, SIGNAL(downloadProgress(qint64, qint64)), this, SLOT(progressUpdate(qint64, qint64)));
        setProgressValue(0);
    }
}

QString InterfaceData::primaryKey()
{
    return itemName(m_rolePrimaryKey);
}

QVariantMap InterfaceData::catchValueMap(QVariant primaryValue)
{
    if (primaryValue.isNull()) {
        qDebug() << "primaryValue is null";
        return QVariantMap();
    }
    for (int i = 0; i < rowCount(); i++) {
        QModelIndex modelIndex = index(i, 0);
        QVariant primaryData = data(modelIndex, m_rolePrimaryKey);
        if (primaryData == primaryValue) {
            QVariantMap valueMap;
            QList<int> roleList = itemsRoleList();
            for (int j = 0; j < roleList.count(); j++) {
                int role = roleList.value(j);
                QString roleName = itemName(role);
                QVariant valueData = data(modelIndex, role);
                valueMap.insert(roleName, valueData);
            }
            return valueMap;
        }
    }
    return QVariantMap();
}

QVariant InterfaceData::dataRole(QModelIndex index, int role)
{
    qDebug() << "dataRole" << roleNames() << " rowCount " << rowCount() << "columnCount" << columnCount() << data(this->index(0, 0));
    return data(index, role);
}

bool InterfaceData::matchContainsFilter(int index)
{
    //    qDebug()<<m_matchContainsMap;
    return m_matchContainsMap.value(index);
}

QString InterfaceData::contentKey() const
{
    return m_contentKey;
}

QString InterfaceData::indexesKey() const
{
    return m_indexesKey;
}

QString InterfaceData::contentPath() const
{
    return m_contentPath;
}

QString InterfaceData::section() const
{
    return m_section;
}

QString InterfaceData::contentQuery() const
{
    return m_contentQuery;
}

QByteArray InterfaceData::content() const
{
    return m_content;
}

QString InterfaceData::schemaPath() const
{
    return m_schemaPath;
}

QString InterfaceData::indexesPath() const
{
    return m_indexesPath;
}

int InterfaceData::numScreenItems() const
{
    return m_numScreenItems;
}

QString InterfaceData::contentOrder() const
{
    return m_contentOrder;
}

float InterfaceData::progressValue() const
{
    return m_progressValue;
}

QString InterfaceData::contentRange() const
{
    return m_contentRange;
}

void InterfaceData::loadContentData()
{
    if (m_contentKey.isEmpty())
        return;

    QStringList indexesId = settingsConfiguration->value(m_indexesKey).toStringList();

    settingsConfiguration->beginGroup(m_contentKey);

    if ((m_contentKey != settingsConfiguration->group()))
        return;

    clear();
    QStringList contentsKeys = settingsConfiguration->childKeys();
    int idRole = Qt::UserRole - 1;
    for (int i = 0; i < contentsKeys.count(); i++) {
        QString id = indexesId.value(i);
        if (id.isNull()) {
            id = contentsKeys.value(i);
        }
        QVariant contentData = settingsConfiguration->value(id);

        if (contentData.userType() == QMetaType::QVariantMap) {
            QVariantMap itemMap = contentData.toMap();
            QStandardItem *item = new QStandardItem();

            QList<int> roleList = itemsRoleList();
            qDebug() << "roleList" << roleList;
            for (int i = 0; i < roleList.count(); i++) {
                int role = roleList.value(i);
                QVariant roleValue;
                QString roleName = itemName(role);
                QString roleKey = itemKey(role);
                if (roleKey.isNull()) {
                    roleValue = itemMap.value(roleName);
                } else {
                    roleValue = itemMap.value(roleKey);
                }

                item->setData(roleValue, role);
            }
            // Accorgimento per memorizzare ID
            item->setData(id, idRole);
            // importante farlo dopo altrimenti inserisce prima un item nullo ee poi nel compilarlo
            // nn si accorge che è dovuto un data changed

            insertRow(i, item);
        }
    }

    for (int j = 0; j < rowCount(); j++) {
        QModelIndex modelIndex = index(j, 0);
    }
    settingsConfiguration->endGroup();
}

void InterfaceData::loadContents()
{
    qDebug() << "loadContents: m_contentPath" << m_contentPath; //<<"schemaDescription"<<schemaDescription;

    QString serverHost = settingsConfiguration->value(QStringLiteral("server")).toString();
    QString loginToken = settingsConfiguration->value(QStringLiteral("token")).toString();
    QUrl urlContent(serverHost);
    urlContent.setPath(m_contentPath);

    QList<int> roleList = itemsRoleList();

    QStringList selectionList;

    for (int i = 0; i < roleList.count(); i++) {
        int role = roleList.value(i);
        QString roleName = itemName(role);
        QString roleRelationTable = itemRelationTable(role);
        QString roleRelationIndex = itemRelationIndex(role);
        if (!roleRelationTable.isEmpty()) {
            //            case for avoid select repeated on the same table
            if (!selectionList.contains(roleRelationTable + QString::fromLatin1("(*)"))) {
                selectionList.append(roleRelationTable + QString::fromLatin1("(*)"));
            }
        }
    }

    QString selectionQuery = selectionList.join(QChar::fromLatin1(','));
    QUrlQuery urlQuery(m_contentQuery);

    if (!selectionList.isEmpty()) {
        urlQuery.addQueryItem(QString::fromLatin1("select"), QString::fromLatin1("*,%1").arg(selectionQuery));
    }

    if (!m_contentOrder.isEmpty()) {
        urlQuery.addQueryItem(QString::fromLatin1("order"), m_contentOrder);
    }
    qDebug() << "loadContents: urlQuery " << urlQuery.toString();
    urlContent.setQuery(urlQuery);

    QNetworkRequest requestContent;
    requestContent.setUrl(urlContent);
    if (!loginToken.isEmpty()) {
        requestContent.setRawHeader("Authorization", QString::fromLatin1("Bearer %1").arg(loginToken).toUtf8());
    }
    requestContent.setRawHeader("Prefer", QString::fromLatin1("count=exact").toUtf8());
    if (m_numScreenItems > 0) {
        int minIndex = m_numScreenItems * offsetFactor;
        int maxIndex = m_numScreenItems * (offsetFactor + 1) - 1;
        QString range = QString::fromUtf8("%1-%2").arg(minIndex).arg(maxIndex);
        qDebug() << "loadContents: range" << range << "on numItems" << numItems;
        requestContent.setRawHeader("Range", range.toUtf8());
    }
    qDebug() << "loadContents: urlContent " << urlContent.toString();
    // On every get call it is instantiated a new object QNetworkReply
    replyContent = netAccessManager->get(requestContent);
    setRunningState(true);

    connect(replyContent, SIGNAL(finished()), this, SLOT(readyContent()));
    connect(replyContent, SIGNAL(downloadProgress(qint64, qint64)), this, SLOT(progressUpdate(qint64, qint64)));
    //    valore per dare l'azzeramento disponibile che lanci il progressbar anche se nn è stato fatto alcun download
    setProgressValue(0);
}

QString InterfaceData::schemaKey() const
{
    return m_schemaKey;
}

void InterfaceData::setFilter(QString filter)
{
    if (m_filter == filter)
        return;
    if (filter.length()) {
        QHash<int, QByteArray> roleNamesHash = roleNames();
        int role = roleNamesHash.key(m_filterRole.toUtf8(), -1);
        //        qDebug()<<"setFilter "+filter<<"role"<<role;
        if (role >= 0) {
            for (int i = 0; i < rowCount(); i++) {
                QStandardItem *standardItem = item(i, m_filterColumn);
                QVariant standardValue = standardItem->data(role);
                QString standardText = standardValue.toString();
                if (standardText.contains(filter, Qt::CaseInsensitive)) {
                    m_matchContainsMap.insert(standardItem->row(), true);
                } else {
                    m_matchContainsMap.insert(standardItem->row(), false);
                }
            }
        }
    } else {
        m_matchContainsMap.clear();
    }
    m_filter = filter;
    Q_EMIT filterChanged(m_filter);
}

void InterfaceData::setFilterColumn(int filterColumn)
{
    if (m_filterColumn == filterColumn)
        return;

    m_filterColumn = filterColumn;
    Q_EMIT filterColumnChanged(m_filterColumn);
}

void InterfaceData::setFilterRole(QString filterRole)
{
    if (m_filterRole == filterRole)
        return;

    m_filterRole = filterRole;
    Q_EMIT filterRoleChanged(m_filterRole);
}

void InterfaceData::setOrderRole(QString sortRole)
{
    if (m_orderRole == sortRole)
        return;

    m_orderRole = sortRole;
    Q_EMIT orderRoleChanged(m_orderRole);
}

QString InterfaceData::filter() const
{
    return m_filter;
}

int InterfaceData::filterColumn() const
{
    return m_filterColumn;
}

QString InterfaceData::filterRole() const
{
    return m_filterRole;
}

QString InterfaceData::orderRole() const
{
    return m_orderRole;
}

void InterfaceData::sortItems(int column, Qt::SortOrder order)
{
    //    qDebug()<<"sortItems"<<column<<"order"<<order;
    if (!m_orderRole.isEmpty()) {
        QHash<int, QByteArray> roleNamesHash = roleNames();
        int role = roleNamesHash.key(m_orderRole.toUtf8(), -1);
        //        qDebug()<<"setSort "<<"role"<<role;
        if (role >= 0) {
            setSortRole(role);
        }
    }
    sort(column, order);
}
