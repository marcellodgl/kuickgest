#include "support.h"

Support::Support()
{
}

QJsonValue Support::parseJsonObject(QJsonObject jsonObj, QString key)
{
    QJsonObject::iterator jsonIterator = jsonObj.begin();
    while (jsonIterator != jsonObj.end()) {
        QJsonValue jsonValue = jsonIterator.value();
        if (key == jsonIterator.key()) {
            return jsonValue;
        } else {
            //            qDebug()<<"value"<<jsonValue<<"isObject"<<jsonValue.isObject();
            QJsonValue subObjValue = parseJsonObject(jsonValue.toObject(), key);
            if (!subObjValue.isNull()) {
                return subObjValue;
            }
        }
        jsonIterator++;
    }
    return QJsonValue();
}

QStringList Support::keysJsonList(QByteArray dataObj)
{
    QJsonDocument jsonDocObj = QJsonDocument::fromJson(dataObj);
    QMap<int, QString> keysPositionMap;
    //    qDebug()<<"dataObj"<<dataObj;
    if (!jsonDocObj.isObject()) {
        qDebug() << "dataObj not object";
        return QStringList();
    }
    QJsonObject jsonObj = jsonDocObj.object();
    QStringList keysList;
    QStringList sortedKeysList = jsonObj.keys();
    for (int i = 0; i < sortedKeysList.count(); i++) {
        QString sortedKey = sortedKeysList.value(i);
        int indexKey = QString::fromUtf8(dataObj).indexOf(QString::fromUtf8("\"%1\":").arg(sortedKey));
        keysPositionMap.insert(indexKey, sortedKey);
    }

    //    Technique to extrapolate keys in the order present in dataObj
    QList<int> keysPositions = keysPositionMap.keys();
    qDebug() << "keysPositions" << keysPositions;
    for (int i = 0; i < keysPositions.count(); i++) {
        int keyPosition = keysPositions.value(i);
        QString key = keysPositionMap.value(keyPosition);
        keysList.append(key);
    }
    qDebug() << "keysList" << keysList;
    return keysList;
}
