#include "interfacesettings.h"

InterfaceSettings::InterfaceSettings(QQuickItem *parent) : QQuickItem(parent)
{
    settings=new QSettings();
}

QVariant InterfaceSettings::value(QString key)
{
    QVariant settingsValue=settings->value(key);
    return settingsValue;
}

void InterfaceSettings::setValue(QString key,QVariant value)
{
    qDebug() << "value type" << value.userType();
    settings->setValue(key,value);
}

