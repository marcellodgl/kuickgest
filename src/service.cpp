#include "service.h"

Service::Service(QObject *parent)
    : QObject(parent)
{
    setRunningState(false);
    netAccessManager = new QNetworkAccessManager(this);

    settingsConfiguration = new QSettings(QCoreApplication::organizationName(), QCoreApplication::applicationName());
    m_progressValue = 1;
    support = new Support();
}

// metodo attivato dal tasto acquire per prelevare dati da passare ai settings
bool Service::updateData()
{
    qDebug() << "service update data";

    QString serverLogin = settingsConfiguration->value(QStringLiteral("login")).toString();
    QString serverPassword = settingsConfiguration->value(QStringLiteral("password")).toString();
    QString serverHost = settingsConfiguration->value(QStringLiteral("server")).toString();
    // verifica correttezza dell'url del server
    if (serverHost.isEmpty()) {
        Q_EMIT errorHappened(tr("Invalid host Url."));

        return false;
    }
    if (serverLogin.isEmpty()) {
        Q_EMIT parametersAcquired();
        return true;
    }
    // Running mode on for progress indicator graphic element
    setRunningState(true);
    QUrlQuery urlQuery;
    urlQuery.addQueryItem(QStringLiteral("username"), serverLogin);
    urlQuery.addQueryItem(QStringLiteral("password"), serverPassword);

    QUrl urlToken(serverHost);
    urlToken.setPath(m_loginTokenPath);
    QUrlQuery urlTokenQuery;
    urlTokenQuery.addQueryItem(QStringLiteral("email"), serverLogin);
    urlTokenQuery.addQueryItem(QStringLiteral("pass"), serverPassword);
    urlToken.setQuery(urlTokenQuery);
    QNetworkRequest requestToken;
    requestToken.setUrl(urlToken);
    qDebug() << "Service: urltoken " << urlToken.toString();
    replyToken = netAccessManager->get(requestToken);
    connect(replyToken, SIGNAL(finished()), this, SLOT(readyToken()));
    return true;
}

// Elabora a partire da dato grezzo letto precedentemente i contenuti json per avere la configurazione delle tabelle
// e la ripone nei settings con chiavi per ciascuna tabella

void Service::acquireSchema(QByteArray schemaData)
{
    //    qDebug()<<"Service acquireSchema:"<<schemaData;
    /*****QJsonDocument jsonSchemaDocument= QJsonDocument::fromBinaryData(schemaData);
     */

    QJsonDocument jsonSchemaDocument = QJsonDocument::fromJson(schemaData);
    // m_section is a parameters that inside openapi document localize definition table names, usually it is "definitions"
    // when this parameter is present the jsonDocument is parsed on its value
    if (!m_section.isEmpty()) {
        QJsonValue sectionValue = support->parseJsonObject(jsonSchemaDocument.object(), m_section);

        jsonSchemaDocument = QJsonDocument::fromVariant(sectionValue.toVariant());
        //        qDebug()<<"sectionValue"<<sectionValue;
    }

    //    qDebug()<<"in acquireSchema"<<jsonSchemaDocument;
    // Verify json document is not empty and is an object
    if (!jsonSchemaDocument.isNull() && jsonSchemaDocument.isObject()) {
        QJsonObject jsonSchemaObject = jsonSchemaDocument.object();
        QStringList schemaKeys = jsonSchemaObject.keys();
        qDebug() << "schemaKeys" << schemaKeys;

        for (int i = 0; i < schemaKeys.count(); i++) {
            QString schemaKey = schemaKeys.value(i);
            qDebug() << "schemaKey" << schemaKey;

            //            if(  schemaKey.contains("anagrafica") )
            //            {
            qDebug() << "schemadata\n";
            //                qDebug()<<QString(schemaData);
            qDebug() << "fine schemadata\n";
            // tecnica grezza adoperata per recuoerare il codice json intatto in formato byte array senza che venga perso
            // l'ordine delle chiavi (che fungono il ruolo di nome dei campi) come ricevute dal server
            //(passando per le chiamate QJson automaticamente le chiavi vengono disposte in ordine alfabetico)

            QString leftKey = QString::fromUtf8("\"") + schemaKey + QString::fromUtf8("\":{\"properties\":");
            // Is correct to use string function directly on QByteArray and not to QString
            //                int propertyIndex=QString(schemaData).indexOf(leftKey);
            int propertyIndex = schemaData.indexOf(leftKey.toUtf8());
            int leftIndex = propertyIndex + leftKey.length();
            qDebug() << "leftIndex" << leftIndex << "leftKey" << leftKey << "leftKeyLength" << leftKey.length();
            // caso particolare in cui nello schema della tabella compare anche la relativa descrizione
            // va cercato l'item + description e poi quanto è sfalzato successivamente la voce "properties"
            // per ottenere infine un oggetto valido
            if (propertyIndex == -1) {
                QString leftKeyDescription = QString::fromUtf8("\"") + schemaKey + QString::fromUtf8("\":{\"description\":");
                int leftIndexDescription = schemaData.indexOf(leftKeyDescription.toUtf8());
                qDebug() << "leftIndexDescription" << leftIndexDescription;
                QString propertyKey = QStringLiteral("\"properties\":");
                QByteArray leftDescriptionData = schemaData.mid(leftIndexDescription, -1);
                //                qDebug() << "preleft leftDescriptionData"<<leftDescriptionData;

                //                    qDebug()<<"leftDescriptionData"<<leftDescriptionData;
                leftIndex = leftIndexDescription + leftDescriptionData.indexOf(propertyKey.toUtf8()) + propertyKey.length();
                qDebug() << "leftIndex" << leftIndex;
            }
            //                QByteArray leftSchemaData=QString(schemaData).mid(leftIndex,-1).toUtf8();
            QByteArray leftSchemaData = schemaData.mid(leftIndex, -1);
            int rightIndex = leftSchemaData.indexOf(",\"type");
            //                if(schemaKey=="fatture") qDebug()<<"leftSchemaData"<<leftSchemaData;
            QByteArray extractSchemaData = leftSchemaData.left(rightIndex);

            qDebug() << "extractSchemaData on " << schemaKey << extractSchemaData;
            // obtained list of keys (fields) from schema for considered table
            QStringList keysList = support->keysJsonList(extractSchemaData);
            // Inserted all object for considered table in settings
            QJsonValue jsonSchemaValue = jsonSchemaObject.value(schemaKey);
            settingsConfiguration->beginGroup(QStringLiteral("schemes"));
            settingsConfiguration->setValue(schemaKey, jsonSchemaValue.toVariant());
            settingsConfiguration->endGroup();
            // Inserted list of keys (fields) from schema for considered table in settings
            settingsConfiguration->beginGroup(QStringLiteral("keys"));
            settingsConfiguration->setValue(schemaKey, keysList);
            settingsConfiguration->endGroup();
        }
    }
}

void Service::setProgressValue(float progressValue)
{
    qWarning("Floating point comparison needs context sanity check");
    if (qFuzzyCompare(m_progressValue, progressValue))
        return;

    m_progressValue = progressValue;
    Q_EMIT progressValueChanged(m_progressValue);
}

void Service::setRunningState(bool runningState)
{
    qDebug() << "Service: runningState " << runningState;
    if (m_runningState == runningState)
        return;

    m_runningState = runningState;
    Q_EMIT runningStateChanged(m_runningState);
}

void Service::cleanRunningState()
{
    setRunningState(false);
}
float Service::progressValue() const
{
    return m_progressValue;
}

bool Service::runningState() const
{
    return m_runningState;
}

void Service::onComunicationError(QNetworkReply::NetworkError code)
{
    qDebug() << "error code " << code;
}

void Service::testsStart()
{
    qDebug() << "test start";
}
QString Service::descriptionKey() const
{
    return m_descriptionKey;
}

QString Service::loginTokenPath() const
{
    return m_loginTokenPath;
}

QString Service::section() const
{
    return m_section;
}

void Service::setDescriptionKey(QString descriptionKey)
{
    if (m_descriptionKey == descriptionKey)
        return;

    m_descriptionKey = descriptionKey;
    Q_EMIT descriptionKeyChanged(m_descriptionKey);
}

void Service::setLoginTokenPath(QString loginTokenPath)
{
    if (m_loginTokenPath == loginTokenPath)
        return;

    m_loginTokenPath = loginTokenPath;
    Q_EMIT loginTokenPathChanged(m_loginTokenPath);
}

void Service::setSection(QString section)
{
    if (m_section == section)
        return;

    m_section = section;
    Q_EMIT sectionChanged(m_section);
}

void Service::readyToken()
{
    QByteArray replyTokenReceived = replyToken->readAll();
    QJsonDocument jsonLoginDocument = QJsonDocument::fromJson(replyTokenReceived);

    if (replyToken->error()) {
        QJsonObject jsonLoginObj = jsonLoginDocument.object();
        QJsonValue loginMessageValue = jsonLoginObj.value(QStringLiteral("message"));
        QString loginMessage = loginMessageValue.toString();
        qDebug() << "Service: Error " << replyToken->error() << " text " << replyToken->errorString() << "message " << loginMessage;
        Q_EMIT errorHappened(replyToken->errorString() + QString::fromUtf8("\n") + loginMessage);
        setRunningState(false);
        return;
    }
    if (jsonLoginDocument.isArray()) {
        QJsonArray jsonLoginArray = jsonLoginDocument.array();
        QJsonValue jsonLoginValue = jsonLoginArray.at(0);
        QJsonObject jsonLoginObj = jsonLoginValue.toObject();
        QJsonValue jsonTokenValue = jsonLoginObj.value(QStringLiteral("token"));
        QString loginToken = jsonTokenValue.toString();
        qDebug() << "token received " << loginToken;
        settingsConfiguration->setValue(QStringLiteral("token"), loginToken);
        setRunningState(false);
        Q_EMIT tokenAcquired();
    }
}
