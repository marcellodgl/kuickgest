Kuickgest: Data managment application interfaced through REST API resource
==========
Kuickgest is an application of consulting and editing data that are acquired and managed by REST API resource.

The interface based on KDE Kirigami/QML is organized on pages convergent for mobile and desktop aspect with data in model view representation.

## Look and operations of application:

#### Settings
The action on drawer *settings* access to the network parameters to the HTTP resource: Url, Login, Password.

![Drawer](images/kuickgest-screenshot.png)

> **Drawer** for access application actions (Mobile).

#### Table functions
In the Table page are present main actions: *last* that catch last inserted records on data resource, *find* that switch to Sample page for search function, *add* that pass to Sample page to insert new row fields to put in the data resource.

On mobile, selecting on Table page a row, swiping the page to the right interface switchs to the Sample page with row values, instead swiping on a single row actions became available: *delete*, *edit* on rows.
On desktop, double clicking on a row, Sample page became visible with row items values, changing selected row values are updated, the same of mobile for *delete* and *edit* actions.
The *delete* action require a message dialog confirmation.

![TablePage](images/kuickgest-screenshot1.png)

> **Table page** showing data records obtained from resource (Mobile).

![SingleInterface](images/kuickgest-screenshot3.png)

> Sample and table page organized on a **Single interface** (Desktop).

#### Row operations
On context menu there are other actions: *all* for query full table records, *order* call an appropriate dialog to sort the the query of records, *update* refresh records on table, *previous* request preceding records, *swipe* is a checkable action for scroll the row to render visibility for all items. On mobile previous records could be called also swiping up or down rows.

#### Items operations
In Sample page fields are shown for visualization, insert and modify. Updated values are acquired using buttons *save*, Button *undo* revert items initial values, *close* returns to Table page.

Fields format represented are:

- Text field: a text line edit
- Script field: a large text area for multiline contents
- Related field: a Combobox select an item related to another table by digit initial key of value, through the *Details* button a dialog appear to scroll all the items of related table, eventually filtering values by a text filter and select an item.
- Date field: a date text edit with buttons through move forward and backwards, through the *Calendar* button a dialog appear to scroll calendar and select a date.
- Integer field: a digit field to insert integer values
- Number field: a point number field to insert number values

![SamplePage](images/kuickgest-screenshot2.png)

> **Sample page** in ordinary mode for showing, edit fields values (Mobile).

#### Search method
Selecting *find* action appear the Sample page with empty fields to be filled for extract data from resource. Each fields is equipped of an operator selector on which is based the query comparison: ~,=,>,<,÷,IN,Ø depending on type.

Over *search* action the query to the data resource start, the answer corrispond to the Table page rows gathering. Using *reset* action all fields are cleared. The *close* action restore ordinary mode and return to Table page.

![SearchPage](images/kuickgest-screenshot4.png)

> **Sample page** in search mode for specify items to search (Mobile).

#### Cards info
In the Main page there is a grid of cards reporting for each table an header name label and status info:

- selected row on table
- search query on resource
- range of data catched from resource
- fields order for query on resource

![MainPage](images/kuickgest-screenshot5.png)

> **Main page** with cards grid reporting tables info (Mobile).

